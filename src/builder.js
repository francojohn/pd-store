import React, {Component} from 'react';
import {change_status_color} from "./utils";



export function create_table_row_status(object) {
    let value = object.get("status");
    let span_color = change_status_color[value];
    let color = span_color ? span_color : "badge-dark";
    return (
        <span className={`badge badge-pill font-weight-light text-white ${color}`}
              style={{letterSpacing: '1px'}}> {value} </span>
    );
}