import {getItemStatusClass} from "./OrderPresenter";

describe('test status class', function () {
    test("test status ON PROCESS from ON PROCESS", () => {
        let status = "ON PROCESS";
        let from_status = "ON PROCESS";
        let result = getItemStatusClass(status, from_status);
        let expectedResult = "active";
        expect(result).toBe(expectedResult);
    });
    test("test status ON PROCESS from IN TRANSIT", () => {
        let status = "ON PROCESS";
        let from_status = "IN TRANSIT";
        let result = getItemStatusClass(status, from_status);
        let expectedResult = "";
        expect(result).toBe(expectedResult);
    });

    test("test status ON PROCESS from DELIVERED", () => {
        let status = "ON PROCESS";
        let from_status = "DELIVERED";
        let result = getItemStatusClass(status, from_status);
        let expectedResult = "";
        expect(result).toBe(expectedResult);
    });


    test("test status IN TRANSIT from ON PROCESS", () => {
        let status = "IN TRANSIT";
        let from_status = "ON PROCESS";
        let result = getItemStatusClass(status, from_status);
        let expectedResult = "active";
        expect(result).toBe(expectedResult);
    });
    test("test status IN TRANSIT from DELIVERED", () => {
        let status = "IN TRANSIT";
        let from_status = "DELIVERED";
        let result = getItemStatusClass(status, from_status);
        let expectedResult = "";
        expect(result).toBe(expectedResult);
    });


    test("test status DELIVERED from ON PROCESS", () => {
        let status = "DELIVERED";
        let from_status = "ON PROCESS";
        let result = getItemStatusClass(status, from_status);
        let expectedResult = "active";
        expect(result).toBe(expectedResult);
    });

    test("test status DELIVERED from IN TRANSIT", () => {
        let status = "DELIVERED";
        let from_status = "IN TRANSIT";
        let result = getItemStatusClass(status, from_status);
        let expectedResult = "active";
        expect(result).toBe(expectedResult);
    });
    test("test status DELIVERED from DELIVERED", () => {
        let status = "DELIVERED";
        let from_status = "DELIVERED";
        let result = getItemStatusClass(status, from_status);
        let expectedResult = "active";
        expect(result).toBe(expectedResult);
    });





    // test("test status IN TRANSIT", () => {
    //
    //     let status = "IN TRANSIT";
    //     let from_status = "IN TRANSIT";
    //
    //     let result = getItemStatusClass(status, from_status);
    //     let expectedResult = "active";
    //
    //     expect(result).toBe(expectedResult);
    // });


});
