import Parse from "parse";

export function getItemStatusClass(status,from) {
    if(status === "DELIVERED"){
        return "active";
    }
    if(status === from){
        return "active";
    }
    if(status === "IN TRANSIT" && from === "ON PROCESS"){
        return "active";
    }
    return "";
}
class OrderPresenter {
    constructor(view){
        this.view = view;
    }
    componentDidMount(){
        this.fetchOrder();
    }
    async fetchOrder(){
       let user = Parse.User.current();
       let orders = await this.getOrderByUser(user);
       this.view.setOrders(orders);
    }
    getOrderByUser(user){
        let query = new Parse.Query("Order");
        query.equalTo("buyer",user);
        query.ascending("-createdAt");
        query.includeAll();
        return query.find();
    }
}

export default OrderPresenter