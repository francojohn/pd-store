import React, {Component, Fragment} from "react";
import './style.css';
import OrderPresenter, {getItemStatusClass} from "./OrderPresenter";
import {change_status_color} from "../../utils";
import ItemProduct from "../../website/ItemProduct";
import {Link} from 'react-router-dom';
import TrackStatus from "../track-status/TrackStatus";
import {create_table_row_status} from "../../builder";

class OrderItem extends Component {
    constructor() {
        super();
        this.state = {items: []};
    }

    componentDidMount() {
        this.fetchProducts();
    }

    async fetchProducts() {
        let order = this.props.children;
        let relation = order.get("items");
        let query = relation.query();
        query.include("product.thumbnail");
        let items = await query.find();
        this.setState({items: items});
    }

    render() {
        let {items} = this.state;
        let order = this.props.children;
        let id = order.id;
        let status = order.get("status");
        let span_color = change_status_color[status];
        let span_class = ['font-size-xs text-white rounded p-1 px-2'];
        span_class.push(span_color ? span_color : "bg-dark");

        return (
            <div className="table-responsive">
                <table className="table" style={{width: "700px"}}>
                    <thead>
                    <tr>
                        <th scope="col" className="border-0"></th>
                        <th scope="col" className="border-0">ITEM(S)</th>
                        <th scope="col" className="border-0">QUANTITY</th>
                        <th scope="col" className="border-0">STATUS</th>
                        <th scope="col" className="border-0">ACTION</th>
                    </tr>
                    </thead>
                    <tbody>
                    {
                        items.map((item) => {
                            let id = item.id;
                            let name = item.get("name");
                            let quantity = item.get("quantity");
                            let product = item.get("product");
                            let variant = item.get("variant");
                            variant = variant ? variant : {};
                            return (
                                <tr key={id} className="border-0">
                                    <td scope="row" className="border-0" style={{width: '280px'}}>
                                        <ItemProduct no_footer={true}>{product}</ItemProduct>
                                    </td>
                                    <td scope="row"
                                        className="border-0 pt-4 text-primary"
                                        style={{width: '350px'}}>
                                        <h6 className="m-0">{name}</h6>
                                        {
                                            Object.keys(variant).map((key) => {
                                                let value = variant[key];
                                                return (
                                                    <label key={key} className="text-muted font-size-xs">{`${key} ${value}`}</label>
                                                )
                                            })
                                        }
                                    </td>
                                    <td className="border-0 pt-4" style={{width: '220px'}}>
                                        <label>{quantity}</label>
                                    </td>
                                    <td className="border-0 pt-4" style={{width: '220px'}}>
                                        {
                                            create_table_row_status(order)
                                        }
                                    </td>
                                    <td className="border-0 pt-4" style={{width: '220px'}}>
                                        {
                                            status === "ON PROCESS" &&
                                            <Link to={`/account/cancel/${id}`} className="font-size-sm">Cancel</Link>
                                        }
                                    </td>
                                </tr>
                            )
                        })
                    }
                    </tbody>
                </table>
            </div>
        );
        return items.map((item) => {
            let name = item.get("name");
            let quantity = item.get("quantity");
            let product = item.get("product");
            let variant = item.get("variant");
            variant = variant ? variant : {};
            return (
                <div className="account-content">
                    <div className="card-body">
                        <div className="row p-3">
                            <div className="col-sm-2 border-right">
                                <ItemProduct no_footer={true}>{product}</ItemProduct>
                            </div>
                            <div className="col-sm-6">
                                <h5 className="font-size-xs mb-1 text-muted mb-2">PRODUCT</h5>
                                <h6 className="text-primary">{name}</h6>
                                {
                                    Object.keys(variant).map((key) => {
                                        let value = variant[key];
                                        return (
                                            <Fragment>
                                                <div
                                                    className="text-muted font-size-xs font-weight-bold m-0 text-uppercase d-block">{key}</div>
                                                <div className="text-dark font-size-xs ml-1 p-0">{value}</div>
                                            </Fragment>
                                        )
                                    })
                                }
                            </div>
                            <div className="col-sm-2 text-center">
                                <h5 className="font-size-xs mb-1 text-muted mb-2">QUANTITY</h5>
                                <h6>{quantity}</h6>
                            </div>
                            <div className="col-sm-2 text-center">
                                <h5 className="font-size-xs mb-1 text-muted mb-2">STATUS</h5>
                                <label className={span_class.join(" ")} style={{"font-size": "10px"}}> {status} </label><br/>

                                {
                                    status === "ON PROCESS" &&
                                    <Link to={`/account/cancel/${id}`} className="font-size-sm">Cancel</Link>
                                }
                            </div>
                        </div>
                    </div>
                </div>
            )
        })
    }
}

class Myorder extends Component {
    constructor() {
        super();
        this.presenter = new OrderPresenter(this);
        this.state = {orders: []};
    }

    componentDidMount() {
        this.presenter.componentDidMount();
    }

    setOrders(orders) {
        this.setState({orders: orders});
    }

    getOrder() {
        let {orders} = this.state;
        return orders;
    }

    renderCollapse(order) {
        let id = order.id;
        let ship_to = order.get("ship_to");
        let status = order.get("status");
        let shipping_total = order.get("shipping_total");
        let order_total = order.get("order_total");
        let payment = order.get("payment");
        return (
            <div className="collapse" id={`order${id}`}>
                <div className="card-body">
                    <div className="col-sm-12">
                        <div className="container">
                            <ul className="progressbar-manage-order">
                                <li className={getItemStatusClass(status, "ON PROCESS")}>ON PROCESS</li>
                                <li className={getItemStatusClass(status, "IN TRANSIT")}>IN TRANSIT</li>
                                <li className={getItemStatusClass(status, "DELIVERED")}>DELIVERED</li>
                                <div className="clearfix"></div>
                            </ul>
                        </div>
                    </div>
                    <div className="card mb-3">
                        <div className="card-body">
                            <div className="row">
                                <div className="col-sm-6">
                                    <h5 className="font-size-sm bg-light text-dark p-3">Delivery
                                        Address
                                        & Billing Address</h5>
                                    <div className="pl-3">
                                        <label
                                            className="font-size-xs d-block text-uppercase text-muted">Ship to:</label>
                                        <p style={{whiteSpace: "pre-line"}}>{ship_to}</p>
                                    </div>
                                </div>
                                <div className="col-sm-6">
                                    <h5 className="font-size-sm bg-light text-dark p-3">Item
                                        Info</h5>
                                    <div className="pl-3">
                                        <label className="font-size-xs d-block text-uppercase text-muted">Order
                                            ID:</label>
                                        <p>{id}</p>
                                        <label
                                            className="font-size-xs d-block text-uppercase text-muted">Status:</label>
                                        {
                                            create_table_row_status(order)
                                        }
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-sm-6 p-0">
                            <div className="card border-0">
                                <div className="card-body">
                                    <h5 className="font-size-sm bg-light text-dark p-3">Shipping Status</h5>
                                    <TrackStatus order={order}/>
                                </div>
                            </div>
                        </div>
                        <div className="col-sm-6">
                            <div className="card mb-3">
                                <div className="card-body">
                                    <h5 className="font-size-sm bg-light text-dark p-3">Summary</h5>
                                    <div className="pl-3">
                                        <label className="text-uppercase text-muted float-left mb-2 font-size-sm">Shipping
                                            Fee</label>
                                        <p className="float-right mb-2">₱ {shipping_total}</p>
                                        <div className="clearfix"></div>
                                        <hr/>
                                        <label
                                            className="text-uppercase text-muted float-left mb-2 font-size-sm">Total</label>
                                        <p className="float-right mb-2"><b>₱ {order_total}</b>
                                        </p>
                                        <div className="clearfix"></div>
                                        <label
                                            className="text-uppercase text-muted float-left mb-2 font-size-sm">Paid
                                            By</label>
                                        <p className="float-right mb-2"><b>{payment}</b></p>
                                        <div className="clearfix"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }


    render() {
        let {orders} = this.state;

        return (
            <div>
                <div style={{height: "55px"}} className="border-bottom account-title">
                    <Link to="/account">
                        <i className="fa fa-arrow-left float-left ml-3 text-muted"
                           style={{paddingTop: "18px", fontSize: "20px"}}></i>
                        <label className="float-left ml-3 text-muted" style={{paddingTop: "15px"}}>My Orders</label>
                        <div className="clearfix"></div>
                    </Link>
                </div>

                <div className="accordion">
                    {
                        orders.map((order) => {
                            let id = order.id;
                            let status = order.get("status");
                            let items = order.get("items");
                            let span_color = change_status_color[status];
                            let span_class = ['font-size-xs text-white rounded p-1 px-2'];
                            span_class.push(span_color ? span_color : "bg-dark");
                            return (
                                <div key={id} className="card border-0 mb-3">
                                    <div className="card-header bg-white border-0">
                                        <div className="row">
                                            <div className="col-sm-4">
                                                <small>Order:&nbsp;&nbsp;<label
                                                    className="font-size-sm bg-light text-dark p-1 px-2">{id}</label>
                                                </small>
                                            </div>
                                            <div className="col-sm-4 offset-4 text-right">
                                                <button type="button"
                                                        className="btn btn-primary p-1 px-2 font-size-xs text-uppercase text-white"
                                                        data-toggle="collapse" data-target={`#order${id}`}>
                                                    <i className="fas fa-cog"></i>&nbsp;&nbsp;<b>Manage</b>
                                                </button>
                                            </div>

                                        </div>
                                    </div>
                                    <OrderItem>{order}</OrderItem>
                                    {
                                        this.renderCollapse(order)
                                    }
                                </div>
                            )
                        })
                    }
                </div>
            </div>
        );
    }
}

export default Myorder;