import React, {Component} from "react";
import "./style.css";
import TrackStatusPresenter from "./TrackStatusPresenter";

const StatusItem = ({active, date, status, message}) => {
    let day = date.toDateString();
    let hour = date.toLocaleTimeString();
    return (
        <tr>
            <td>
                <div className="status mt-3">
                    <div className={`status-con rounded-circle ${active ? "active" : ""}`}></div>
                </div>
            </td>
            <td>
                <small>Tuesday<br/>{day}</small>
                <br/>
                <label className="font-size-xs">{hour}</label>
            </td>
            <td>
                <small className="text-uppercase"><b>{status}</b></small>
                <br/>
                <label className="font-size-xs">{message}</label>
            </td>
        </tr>
    );
}

class TrackStatus extends Component {
    constructor() {
        super();
        this.presenter = new TrackStatusPresenter(this);
        this.state = {statuses: []}
    }

    componentDidMount() {
        this.presenter.componentDidMount();
    }

    getOrder() {
        let {order} = this.props;
        return order;
    }
    setStatuses(statuses){
        this.setState({statuses:statuses});
    }
    render() {
        let {statuses} = this.state;
        let {order} = this.props;
        let order_id = order.id;
        return (
            <table className="table text-center">
                <thead>
                <tr>
                    <th scope="col"
                        className="border-primary border-top-0">STATUS
                    </th>
                    <th scope="col"
                        className="border-primary border-top-0">DATE
                    </th>
                    <th scope="col"
                        className="border-primary border-top-0">STATUS
                    </th>
                </tr>
                </thead>
                <tbody>
                {
                    statuses.map((s, index) => {
                        let id = s.id ? s.id : index;
                        let status = s.get("status");
                        let message = s.get("message");
                        let createdAt = s.get("createdAt");
                        createdAt = createdAt ? createdAt : s.get("date");
                        let isActive = statuses.length === index + 1;
                        return (
                            <StatusItem key={`${order_id+id}`} date={createdAt} status={status} message={message} active={isActive}/>
                        )
                    })
                }
                </tbody>
            </table>
        );
    }
}

export default TrackStatus;