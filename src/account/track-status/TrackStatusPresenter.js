import Parse from "parse";

class TrackStatusPresenter {
    constructor(view) {
        this.view = view;
    }

    componentDidMount() {
        this.fetchOrderStatus();
    }

    async fetchOrderStatus() {
        let order = this.view.getOrder();
        let statutes = [];
        let orderStatus = new Parse.Object("OrderStatus");
        orderStatus.set("date", order.get("createdAt"));
        orderStatus.set("status", "ON PROCESS");
        orderStatus.set("message", "order created");
        //
        statutes.push(orderStatus);
        let bookStatus = await this.getOrderStatusByOrder(order);
        statutes.push(...bookStatus);
        this.view.setStatuses(statutes);
    }

    getOrderStatusByOrder(order) {
        let query = new Parse.Query("OrderStatus");
        query.equalTo("order", order);
        return query.find();
    }
}

export default TrackStatusPresenter