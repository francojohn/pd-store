import React, {Component} from "react";
import {Route, Link, Redirect} from 'react-router-dom';
import SideBarMenu, {menu} from "../components/sidebar-menu";
import './style.css';
//parse
import Parse from 'parse';
import Myorder from "./Myorder/myorder";
import Myaddress from "./Myaddress/myaddress";
import Cancelorder from "./Cancelorder/cancelorder";
import ProfileSettings from "./profile-settings/ProfileSettings";
import Construction from "../website/construction/construction";

let historys = [
    new menu('/profile/depositehistory', 'fa fa-history', 'Deposit History'),
    new menu('/profile/withdrawhistory', 'fa fa-history', 'Withdraw History'),
]

let sectionTwo = [
    new menu('/account/profile', 'fa fa-user', 'My Account'),
];


let sectionOne = [
    new menu('/account/order', 'fas fa-box', 'My Orders'),
    new menu('/account/item', 'fa fa-calendar-check', 'My Item'),
    new menu('/account/address', 'fas fa-map-marked-alt', 'My Address'),
];


let menus = [
    sectionOne,
    sectionTwo,
];

class Account extends Component {
    constructor() {
        super();
    }

    onRoute = title => {
        this.title = title;
    }

    render() {
        let user = Parse.User.current();
        if (!user) {
            return (<Redirect push={true} to="/signin"/>);
        }
        let name = user.get("name");
        let picture = user.get('picture');
        let pictureUrl = picture ? picture._url : "/anonymous_user.png";
        return (
            <div className="account">
                <div className="container">
                    <div className="row p-4">
                        <div className="col-sm-2">
                            <div className="bg-light text-center p-4">
                                <img alt="header profile" style={{'height': '100px', 'width': '100px'}}
                                     className="img-fluid img-thumbnail rounded-circle text-center"
                                     src={pictureUrl}/>
                            </div>
                            <br/>
                            <h2 className="text-muted text-center text-capitalize"
                                style={{fontSize: "18px"}}>{name}</h2>
                            <SideBarMenu text={"text-muted"} {...this.props} menus={menus} onRoute={this.onRoute}/>
                        </div>
                        <div className="col-sm-10 mobileAccount">
                            <div className="">
                                <div className="card" style={{"background": "#f9f9f9"}}>
                                    <Route path="/account/order" component={Myorder}/>
                                    <Route path="/construction" component={Construction}/>
                                    <Route path="/account/address" component={Myaddress}/>
                                    <Route path="/account/profile" component={ProfileSettings}/>
                                    <Route path="/account/cancel/:id" component={Cancelorder}/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Account;