import React, {Component} from "react";
import './style.css';
import ProfileSettingsPresenter from "./ProfileSettingsPresenter";
import {Link} from 'react-router-dom';
class ProfileSettings extends Component {
    constructor() {
        super();
        this.presenter = new ProfileSettingsPresenter(this);

    }
    componentDidMount() {
        this.presenter.componentDidMount();
    }

    //setters

    setName(name) {
        document.getElementById("profile_settings_label_name").innerHTML = name;
    }

    setFirstName(first_name) {
        document.getElementById("profile_settings_input_first_name").value = first_name;
    }

    setLastName(last_name) {
        document.getElementById("profile_settings_input_last_name").value = last_name;
    }

    setAddress(address) {
        document.getElementById("profile_settings_input_address").value = address;
    }

    setEmail(email) {
        document.getElementById("profile_settings_input_email").value = email;
    }

    setMobilePhone(mobile_phone) {
        document.getElementById("profile_settings_input_mobile_phone").value = mobile_phone;
    }

    setBirthDay(birthday) {
        return document.getElementById("profile_settings_input_birthday").value = birthday;
    }

    setPicture(pictureUrl) {
        return document.getElementById("profile_settings_picture").src = pictureUrl;
    }

    showProgress() {
        let button = document.getElementById("profile_settings_btn_save");
        button.disabled = true;
        button.classList.add('bg-progress');
        button.innerHTML = "Please Wait..";
    }

    hideProgress() {
        let button = document.getElementById("profile_settings_btn_save");
        button.disabled = false;
        button.classList.remove('bg-progress');
        button.innerHTML = "SAVE";
    }

    //getters
    getFirstName() {
        return document.getElementById("profile_settings_input_first_name").value;
    }

    getLastName() {
        return document.getElementById("profile_settings_input_last_name").value;
    }

    getAddress() {
        return document.getElementById("profile_settings_input_address").value;
    }

    getMobilePhone() {
        return document.getElementById("profile_settings_input_mobile_phone").value;
    }

    getBirthDay() {
        return document.getElementById("profile_settings_input_birthday").value;
    }


    //events
    formSubmit = (e) => {
        e.preventDefault();
        this.presenter.formSubmit();
    }

    render() {
        return (
            <div>
                <div style={{height:"55px"}} className="border-bottom account-title">
                    <Link to="/account">
                        <i className="fa fa-arrow-left float-left ml-3 text-muted" style={{paddingTop: "18px", fontSize: "20px"}}></i>
                        <label className="float-left ml-3 text-muted" style={{paddingTop: "15px"}}>Profile Settings</label>
                        <div className="clearfix"></div>
                    </Link>
                </div>
                <div className="row my-4">
                    <div className="col-12 mt-2">
                        <div className="card border-0">
                            <div className="card-body">
                                <h5 className="font-size-sm bg-light text-dark p-3">Profile Information</h5>
                                <div className="row">
                                    <div className="col-4 border-right">
                                        <div className="mt-4">
                                            <div className="text-center position-relative">
                                                <img className="rounded-circle img-fluid"
                                                     style={{width: '150px', height: '150px'}}
                                                     src="/anonymous_user.png"
                                                     alt="image-user" id="profile_settings_picture"/>
                                                <button type="file"
                                                        className="btn btn-sm btn-outline-light position-absolute"
                                                        style={{'left': '44%', 'bottom': '5%',}}><i
                                                    className="fas fa-camera"></i></button>

                                            </div>
                                            <div className="text-center mt-2">
                                                <h5 className="text-muted mt-3 m-0"
                                                    id="profile_settings_label_name"></h5>
                                                <label className="font-size-sm bg-secondary text-white py-2 px-3">Account
                                                    No. 12369906</label>
                                            </div>
                                        </div>

                                        <div className="dropdown-divider my-4"></div>

                                        <h6 className="text-primary font-size-sm font-weight-bold">Account Setting</h6>

                                        <div className="container">
                                            <div className="row mt-4 justify-content-between">
                                                <h6 className="font-size-sm">Auto Email</h6>

                                                <label className="switch">
                                                    <input type="checkbox"/>
                                                    <span className="slider round"></span>
                                                </label>
                                            </div>

                                            <div className="row mt-4 justify-content-between">
                                                <h6 className="font-size-sm">Pop-up Notification</h6>
                                                <label className="switch">
                                                    <input type="checkbox"/>
                                                    <span className="slider round"></span>
                                                </label>
                                            </div>

                                            <div className="row mt-4 justify-content-between">
                                                <h6 className="font-size-sm">SMS Messages</h6>
                                                <label className="switch">
                                                    <input type="checkbox"/>
                                                    <span className="slider round"></span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>

                                    <div className="col-8">
                                        <div className="mt-4">
                                            <form onSubmit={this.formSubmit}>
                                                <div className="form-row">
                                                    <div className="form-group col-md-6">
                                                        <label className="font-size-sm">First Name</label>
                                                        <input type="text" className="form-control"
                                                               id="profile_settings_input_first_name"/>
                                                    </div>

                                                    <div className="form-group col-md-6">
                                                        <label className="font-size-sm">Last Name</label>
                                                        <input type="text" className="form-control"
                                                               id="profile_settings_input_last_name"/>
                                                    </div>
                                                </div>

                                                <div className="form-group">
                                                    <label className="font-size-sm">Email Address</label>
                                                    <input type="email" className="form-control"
                                                           id="profile_settings_input_email" disabled/>
                                                </div>
                                                <div className="form-group" data-toggle="collapse"
                                                     data-target="#collapse_change_password">
                                                    <label className="font-size-sm">Password</label>
                                                    <input type="password" className="form-control"
                                                           id="profile_settings_input_password" value="*******"/>
                                                </div>

                                                <div className="my-3">
                                                    <div className="collapse multi-collapse"
                                                         id="collapse_change_password">
                                                        <div className="form-group">
                                                            <label className="font-size-sm">New Password</label>
                                                            <input type="password" className="form-control"
                                                                   placeholder="New Password"/>
                                                        </div>

                                                        <div className="form-group">
                                                            <label className="font-size-sm">Confirm Password</label>
                                                            <input type="password" className="form-control"
                                                                   placeholder="Confirm Password"/>
                                                        </div>
                                                        <button className="btn btn-outline-dark mr-2" type="button"
                                                                data-toggle="collapse"
                                                                data-target="#collapse_change_password">Cancel
                                                        </button>
                                                        <button className="btn btn-primary">Confirm</button>
                                                    </div>
                                                </div>

                                                <div className="form-group">
                                                    <label className="font-size-sm">Mobile Phone</label>
                                                    <input type="text" className="form-control"
                                                           id="profile_settings_input_mobile_phone"/>
                                                </div>

                                                <div className="form-row">
                                                    <div className="form-group col-md-6">
                                                        <label className="font-size-sm">Birthday</label>
                                                        <input type="date" className="form-control"
                                                               id="profile_settings_input_birthday"/>
                                                    </div>
                                                </div>

                                                <button type="submit" className="btn btn-sm text-white btn-primary px-5 mt-3"
                                                        id="profile_settings_btn_save">SAVE
                                                </button>
                                            </form>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        );
    }
}

export default ProfileSettings;