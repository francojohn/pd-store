import Parse from "parse";

class ProfileSettingsPresenter {
    constructor(view) {
        this.view = view;
    }

    componentDidMount() {
        this.setData();
    }

    setData() {
        //get data
        let user = Parse.User.current();
        let picture = user.get('picture');
        let pictureUrl = picture ? picture._url : "/anonymous_user.png";

        let first_name = user.get("first_name");
        let last_name = user.get("last_name");
        let name = user.get("name");
        let birthday = user.get("birthday");
        let address = user.get("address");
        let email = user.get("email");
        let mobile_phone = user.get("mobile_phone");
        //set data
        this.view.setName(name ? name : '');
        this.view.setFirstName(first_name ? first_name : '');
        this.view.setLastName(last_name ? last_name : '');
        this.view.setEmail(email ? email : '');
        this.view.setMobilePhone(mobile_phone ? mobile_phone : '');
        this.view.setBirthDay(birthday ? birthday.toISOString().substr(0, 10):'');
        this.view.setPicture(pictureUrl);
    }

    async formSubmit() {
        this.view.showProgress();
        let first_name = this.view.getFirstName();
        let last_name = this.view.getLastName();
        let name = `${first_name} ${last_name}`;
        let mobile_phone = this.view.getMobilePhone();
        let birthday = this.view.getBirthDay();
        //set data
        let user = Parse.User.current();
        user.set("first_name", first_name);
        user.set("last_name", last_name);
        user.set("name", name);
        user.set("mobile_phone", mobile_phone);
        user.set("birthday", new Date(birthday));
        try {
            await user.save();

        } catch (e) {
            alert(e);
        }
        this.view.hideProgress();
    }
}

export default ProfileSettingsPresenter