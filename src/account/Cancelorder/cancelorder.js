import React, {Component, Fragment} from 'react';
import OrderCancelPresenter from "./OrderCancelPresenter";
import ItemProduct from "../../website/ItemProduct";

class Cancelorder extends Component {
    constructor() {
        super();
        this.presenter = new OrderCancelPresenter(this);
        this.state = {order: null, items: []}
    }
    componentDidMount() {
        this.presenter.componentDidMount();
    }
    setOrder(order) {
        this.setState({order: order});
    }
    getOrder() {
        let {order} = this.state;
        return order;
    }
    setItems(items) {
        this.setState({items: items});
    }

    getOrderId() {
        return this.props.match.params.id;
    }

    submitClick = () => {
        this.presenter.submitClick();
    }
    gotoOrderCancelSuccess(){

    }
    showProgress() {
        let button = document.getElementById("order_btn_submit");
        button.disabled = true;
        button.classList.add('bg-progress');
        button.innerHTML = "Please Wait..";
    }

    hideProgress() {
        let button = document.getElementById("order_btn_submit");
        button.disabled = false;
        button.classList.remove('bg-progress');
        button.innerHTML = "SUBMIT";
    }
    render() {
        let {order, items} = this.state;
        if (!order) {
            return (
                <h1>loading</h1>
            )
        }
        let id = order.id;
        return (
            <div className="cancel">
                {
                    items.map((item) => {
                        let name = item.get("name");
                        let quantity = item.get("quantity");
                        let product = item.get("product");
                        let variant = item.get("variant");
                        variant = variant ? variant : {};

                        return (
                            <div className="card border-0 mb-3">
                                <div className="card-header bg-white border-0">
                                    <div className="row">
                                        <div className="col-sm-6">
                                            <smal>Order:&nbsp;&nbsp;<label
                                                className="font-size-sm bg-light text-dark p-1 px-2">{id}</label>
                                            </smal>
                                        </div>
                                    </div>
                                </div>
                                <div className="card-body">
                                    <div className="row">
                                        <div className="col-sm-2 border-right">
                                            <ItemProduct no_footer={true}>{product}</ItemProduct>
                                        </div>
                                        <div className="col-sm-4">
                                            <h5 className="font-size-xs mb-1 text-muted mb-2">PRODUCT</h5>
                                            <h6 className="text-primary">{name}</h6>
                                            {
                                                Object.keys(variant).map((key) => {
                                                    let value = variant[key];
                                                    return (
                                                        <Fragment>
                                                            <div
                                                                className="text-muted font-size-xs font-weight-bold m-0 text-uppercase d-block">{key}</div>
                                                            <div
                                                                className="text-dark font-size-xs ml-1 p-0">{value}</div>
                                                        </Fragment>
                                                    )
                                                })
                                            }
                                        </div>
                                        <div className="col-sm-2 text-center">
                                            <h5 className="font-size-xs mb-1 text-muted mb-2">QUANTITY</h5>
                                            <h6>{quantity}</h6>
                                        </div>
                                        <div className="col-sm-4 text-right">
                                            <form>
                                                <div className="form-group">
                                                    <select className="form-control" id="sel1">
                                                        <option value="" disabled selected>Select Reason</option>
                                                        <option>Change of mind</option>
                                                    </select>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        )
                    })
                }


                <div className="card border-0 mb-3">
                    <div className="card-body">
                        <label className="font-size-xs mb-1 text-muted mb-2">Additional Information (optional)</label>
                        <textarea className="form-control" placeholder=""></textarea>
                    </div>
                </div>
                <div className="card border-0 mb-3">
                    <div className="card-body">
                        <h5 className="font-size-sm mb-3">Cancellation Policy</h5>
                        <div className="p-3 bg-light">
                            <h2 className="font-size-sm mt-2 mb-3">Before cancelling the order, kindly read thoroughly
                                our following terms & conditions:</h2>
                            <ol className="pl-4">
                                <li className="font-size-sm mb-2">Far far away, behind the word mountains, far from the
                                    countries Vokalia and Consonantia, there live the blind texts. Separated they live
                                    in Bookmarksgrove right at the coast of the Semantics, a large language ocean.
                                </li>
                                <li className="font-size-sm mb-2">Far far away, behind the word mountains, far from the
                                    countries Vokalia and Consonantia, there live the blind texts. Separated they live
                                    in Bookmarksgrove right at the coast of the Semantics, a large language ocean.
                                </li>
                                <li className="font-size-sm mb-2">Far far away, behind the word mountains, far from the
                                    countries Vokalia and Consonantia, there live the blind texts. Separated they live
                                    in Bookmarksgrove right at the coast of the Semantics, a large language ocean.
                                </li>
                                <li className="font-size-sm mb-2">Far far away, behind the word mountains, far from the
                                    countries Vokalia and Consonantia, there live the blind texts. Separated they live
                                    in Bookmarksgrove right at the coast of the Semantics, a large language ocean.
                                </li>
                            </ol>
                        </div>
                        <div className="mt-3">
                            <input className="form-check form-check-inline" type="checkbox"/>
                            <span className="ml-3">I have read and accepted the Cancellation Policy of PD Store.</span>
                        </div>
                    </div>
                </div>
                <div className="text-right">
                    <button onClick={this.submitClick} className="btn btn-sm btn-primary text-white" id="order_btn_submit">SUBMIT</button>
                </div>
            </div>

        );
    }
}

export default Cancelorder;