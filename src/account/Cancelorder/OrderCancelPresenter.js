import Parse from "parse";

class OrderCancelPresenter {
    constructor(view) {
        this.view = view;
    }

    componentDidMount() {
        this.fetchOrder();
    }

    async fetchOrder() {
        let orderId = this.view.getOrderId();
        let order = await this.getOrderById(orderId);
        this.view.setOrder(order);

        let items = await this.getItemsByOrder(order);
        this.view.setItems(items);
    }

    getOrderById(id) {
        let query = new Parse.Query("Order");
        query.equalTo("objectId", id);
        return query.first();
    }

    getItemsByOrder(order) {
        let relation = order.get("items");
        let query = relation.query();
        query.include("product.thumbnail");
        return query.find();
    }
    async submitClick(){
        this.view.showProgress();
        try{
            let order = this.view.getOrder();
            order.set("status","CANCELLED");
            await order.save();
            this.view.hideProgress();
            this.view.gotoOrderCancelSuccess();
        }catch (e) {
            alert(e);
            this.view.hideProgress();
        }

    }
}

export default OrderCancelPresenter