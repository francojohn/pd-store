import Myorder from "../Myaddress/myaddress"
import React, {Component} from "react";
import AddressPresenter from "./AddressPresenter";
import {Link} from 'react-router-dom';

class Myaddress extends Component {
    constructor() {
        super();
        this.presenter = new AddressPresenter(this);
        this.state = {address: []};
    }

    componentDidMount() {
        this.presenter.componentDidMount();
    }

    setAddress(address) {
        this.setState({address: address});
    }

    render() {
        let {address} = this.state;
        return (
            <div className="address">
                <div style={{height:"55px"}} className="account-title border-bottom">
                    <Link to="/account">
                        <i className="fa fa-arrow-left float-left ml-3 text-muted" style={{paddingTop: "18px", fontSize: "20px"}}></i>
                        <label className="float-left ml-3 text-muted" style={{paddingTop: "15px"}}>My Address</label>
                        <div className="clearfix"></div>
                    </Link>
                </div>
                <div className="row">
                    {
                        address.map((add) => {
                            let full_name = add.get("full_name");
                            let mobile_number = add.get("mobile_number");
                            let complete_address = add.get("complete_address");
                            let address_name = add.get("address_name");
                            return (
                                <div className="col-sm-6">
                                    <div className="card p-4 border-0 my-address">
                                        <div className="card-body">
                                            <small className="text-muted mb-3 d-inline float-left font-size-sm">Shipping
                                                Address
                                            </small>
                                            <div className="float-right font-size-xs">
                                                <a href="#"><i
                                                    className="fas fa-pencil-alt"></i></a>&nbsp;&nbsp;|&nbsp;&nbsp;<a
                                                href="#"><i className="fas fa-trash-alt"></i></a>
                                            </div>
                                            <div className="clearfix"></div>
                                            <p className="text-capitalize mb-1 font-size-sm"><b>{full_name}</b></p>
                                            <p className="m-0 font-size-sm">{complete_address}</p>
                                            <p className="m-0 font-size-sm">{mobile_number}</p>
                                        </div>
                                    </div>
                                </div>
                            )
                        })
                    }

                </div>
            </div>
        );
    }
}

export default Myaddress;