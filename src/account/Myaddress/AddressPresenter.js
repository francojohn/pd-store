import Parse from "parse";

class AddressPresenter {
    constructor(view){
        this.view = view;
    }
    async componentDidMount(){
        let user = Parse.User.current();
        let address = await this.fetchAddress(user);
        this.view.setAddress(address);
    }
    fetchAddress(user) {
        let query = new Parse.Query('Address');
        query.equalTo("user", user);
        return query.find();
    }

}

export default AddressPresenter