import AddProductPresenter from "./AddProductPresenter";

describe("test format variant", () => {
    let presenter = new AddProductPresenter(this);
    test("single variant", () => {
        let variant = {"1": {"key": "Color", "variants": ["Black", "White"]}};
        let expectedResult = {"Color": ["Black", "White"]};
        let result = presenter.formatVariant(variant);
        expect(result).toEqual(expectedResult);
    });
    test("double variant", () => {
        let variant = {
            "1": {"key": "Color", "variants": ["Black", "White"]},
            "2": {"key": "Size", "variants": ["Small", "Medium"]}
        };
        let expectedResult = {"Color": ["Black", "White"], "Size": ["Small", "Medium"]};
        let result = presenter.formatVariant(variant);
        expect(result).toEqual(expectedResult);
    });
    test("multiple variant", () => {
        let variant = {
            "1": {"key": "Color", "variants": ["Black", "White"]},
            "2": {"key": "Size", "variants": ["Small", "Medium"]},
            "3": {"key": "Gender", "variants": ["Male", "Female"]},
        };
        let expectedResult = {"Color": ["Black", "White"], "Size": ["Small", "Medium"], "Gender": ["Male", "Female"]};
        let result = presenter.formatVariant(variant);
        expect(result).toEqual(expectedResult);
    });
});

describe("test generate variant", () => {
    let presenter = new AddProductPresenter(this);
    test("single variant", () => {
        let variant = {"Color": ["Black", "White"]};
        let result = presenter.generateVariant(Object.keys(variant), variant);
        let expectedResult = [{"Color": "Black"}, {"Color": "White"}];
        expect(result).toEqual(expectedResult);
    });
    test("double variant", () => {
        let variant = {"Color": ["Black", "White"], "Size": ["Small", "Medium"]};
        let result = presenter.generateVariant(Object.keys(variant), variant);
        let expectedResult = [{"Color": "Black", "Size": "Small"}, {
            "Color": "Black",
            "Size": "Medium"
        }, {"Color": "White", "Size": "Small"}, {"Color": "White", "Size": "Medium"}];
        expect(result).toEqual(expectedResult);
    });
    test("multiple variant", () => {
        let variant = {"Color": ["Black", "White"], "Size": ["Small", "Medium"], "Gender": ["Male", "Female"]};
        let result = presenter.generateVariant(Object.keys(variant), variant);
        let expectedResult = [{"Color": "Black", "Gender": "Male", "Size": "Small"}, {
            "Color": "Black",
            "Gender": "Female",
            "Size": "Small"
        }, {"Color": "Black", "Gender": "Male", "Size": "Medium"}, {
            "Color": "Black",
            "Gender": "Female",
            "Size": "Medium"
        }, {"Color": "White", "Gender": "Male", "Size": "Small"}, {
            "Color": "White",
            "Gender": "Female",
            "Size": "Small"
        }, {"Color": "White", "Gender": "Male", "Size": "Medium"}, {
            "Color": "White",
            "Gender": "Female",
            "Size": "Medium"
        }];
        expect(result).toEqual(expectedResult);
    });
});


