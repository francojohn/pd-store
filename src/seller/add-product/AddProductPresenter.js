import ProductModel from "../../website/product/ProductModel";
import {productToParse} from "../../website/product/ProductMapper";
import Parse from "parse";
import ProductImageModel from "../../website/product/ProductImageModel";

class AddProductPresenter {
    constructor(view) {
        this.view = view;
    }

    componentDidMount() {

    }

    addImageChange(files) {
        let images = [];
        for (let i = 0; i < files.length; i++) {
            let file = files[i];
            let image = new ProductImageModel();
            image.file = file;
            image.isThumbnail = (i == 0);
            images.push(image);
        }
        this.view.setImages(images);
    }

    imageDeleteClick(key) {
        console.log("imageDeleteClick");
        // let images = {... this.view.getImages()};
        // delete images[key];
        // this.view.setImageFiles(images);
    }

    formSubmit() {
        let productVariants = this.view.getProductVariants();
        let variant = this.formatVariant(this.view.getRawVariant());

        let images = this.view.getImages();
        if (images.length < 1) {
            this.view.showErrorMessage("please upload one or more image");
            return;
        }
        //get value
        let name = this.view.getProductName();
        let description = this.view.getProductName();
        let price = this.view.getProductPrice();
        let stock = this.view.getProductPrice();
        //assign value
        let product = new ProductModel();
        product.name = name;
        product.description = description;
        product.price = price;
        product.stock = stock;
        product.productImages = images;
        product.variant = variant;

        this.saveProduct(product);
    }

    addVariantClick() {
        let variant = this.view.getRawVariant();
        let position = Object.keys(variant).length + 1;
        variant[position] = {"key": "", "variants": [0]};
        this.view.setVariant(variant);
    }

    variantChange(position, key) {
        let variant = this.view.getRawVariant();
        variant[position]["key"] = key;
        this.view.setVariant(variant);
        this.createVariant(variant);
    }

    variantOptionChange(position, value, newValue) {
        let variant = this.view.getRawVariant();
        let variants = variant[position]["variants"];
        let index = variants.indexOf(value);
        variants[index] = newValue;
        this.view.setVariant(variant);
        this.createVariant(variant);
    }

    addVariantOptionClick(position) {
        let variant = this.view.getRawVariant();
        variant[position]["variants"].push(0);
        this.view.setVariant(variant);
    }

    removeVariantClick(position) {
        let variant = this.view.getRawVariant();
        delete variant[position];
        this.view.setVariant(variant);
        this.createVariant(variant);
    }

    removeVariantOptionClick(position, v) {
        let variant = this.view.getRawVariant();
        let variants = variant[position]["variants"];
        let index = variants.indexOf(v);
        variants.splice(index, 1);
        variant[position]["variants"] = variants;
        this.view.setVariant(variant);
        this.createVariant(variant);
    }

    createVariant(rawVariant) {
        let variant = this.formatVariant(rawVariant);
        let variants = this.generateVariant(Object.keys(variant), variant);
        let productVariants = [];
        for (let v of variants) {
            let product = new ProductModel();
            product.variant = v;
            productVariants.push(product);
        }
        this.view.setProductVariants(productVariants);
    }

    generateVariant(keys, variantList, startIndex = 0, generatedList = [], combinedVariants = {}) {
        const key = keys[startIndex], variants = variantList[key];
        for (combinedVariants[key] of variants) {
            (startIndex + 1 === keys.length) ? generatedList.push({...combinedVariants})
                : this.generateVariant(keys, variantList, startIndex + 1, generatedList, {...combinedVariants});
        }
        return generatedList;
    };

    formatVariant(rawVariant) {
        let variant = {}
        for (let index in rawVariant) {
            let key = rawVariant[index].key;
            variant[key] = rawVariant[index].variants;
        }
        return variant;
    }

    imageThumbnailClick(image) {
        let images = this.view.getImages();
        for (let img of images) {
            if (img.isThumbnail) {
                img.isThumbnail = false;
            }
            if (image == img) {
                image.isThumbnail = true;
            }
        }
        this.view.setImages(images);
    }

    async saveProduct(product) {
        let productVariants = this.view.getProductVariants();
        let variants = [];
        for (let p of productVariants) {
            let product = new Parse.Object("Product");
            product.set("stock", p.stock);
            product.set("price", p.price);
            product.set("variant", p.variant);
            variants.push(product);
        }
        variants = await Parse.Object.saveAll(variants);
        try {
            //map product
            let parseProduct = productToParse(product);
            parseProduct.relation("variations").add(variants);
            await parseProduct.save();

            let parseFiles = [];
            let productImages = [];
            for (let image of product.productImages) {
                let file = image.file;
                let parseFile = new Parse.File(file.name, file);
                await parseFile.save();
                let productImage = new Parse.Object("ProductImage");
                productImage.set("file", parseFile);
                productImage.set("product", parseProduct);
                productImages.push(productImage);

            }
            await Parse.Object.saveAll(productImages);
            this.view.successSave();
        } catch (err) {
            this.view.showErrorMessage(err);
        }

    }
}

export default AddProductPresenter
