import React, {Component, Fragment} from "react";
import './style.css';
import AddProductPresenter from "./AddProductPresenter";


class ImageLoader extends Component {
    constructor() {
        super();
        this.state = {url: null};
    }

    componentDidMount() {
        let {image} = this.props; //get file data
        //create file reader
        let file = image.file;
        let reader = new FileReader();
        reader.onload = ((e) => {
            let url = e.target.result;
            this.setState({url: url});
        });
        reader.readAsDataURL(file);
    }

    render() {
        let {url} = this.state;
        let {image} = this.props;
        return (
            <li className="mx-1">
                <div className="img-item text-center">
                    <div className="overlay"></div>
                    <img className="img-fluid" src={url}/>
                    <div className="content">
                        <a href="#" onClick={this.props.deleteClick} className="ml-2 dlt"
                           style={{width: "28px", height: "28px"}}>
                            <i className="fas fa-trash-alt"></i>
                        </a>
                        <a href="#" className="ml-2 crp" style={{width: "28px", height: "28px"}}>
                            <i className="fas fa-crop-alt"></i>
                        </a>
                        <button type="button"
                                onClick={this.props.thumbnailClick}
                                className={`btn btn-secondary btn-sm rounded-0 py-2 text-white btn-block font-size-sm cover ${image.isThumbnail ? 'd-block' : ''}`}>
                            Thumbnail Photo
                        </button>
                    </div>
                </div>
            </li>
        );
    }
}


class AddProduct extends Component {
    constructor() {
        super();
        this.presenter = new AddProductPresenter(this);
        this.state = {images: [], variant: {}, productVariants: []};
    }

    componentDidMount() {
        this.presenter.componentDidMount();

    }

    showErrorMessage(message) {
        alert(message);
    }

    setImages(images) {
        this.setState({images: images});
    }

    imageThumbnailClick = (image) => {
        this.presenter.imageThumbnailClick(image);
    }

    getImages() {
        return this.state.images;
    }

    setVariant(variant) {
        this.setState({variant: variant});
    }

    setProductVariants(productVariants) {
        this.setState({productVariants: productVariants});
    }

    getProductVariants() {
        return this.state.productVariants;
    }

    getRawVariant() {
        return this.state.variant;

    }

    successSave() {
        alert("successSave");
    }

    addImageChange = (e) => {
        let files = e.target.files;
        this.presenter.addImageChange(files);
    }
    formSubmit = (e) => {
        e.preventDefault();
        this.presenter.formSubmit();
    }
    imageDeleteClick = (image) => {
        this.presenter.imageDeleteClick(image);
    }

    addVariantClick = () => {
        this.presenter.addVariantClick();
    }

    addVariantOptionClick(key) {
        this.presenter.addVariantOptionClick(key);
    }

    removeVariantClick(key) {
        this.presenter.removeVariantClick(key);
    }

    removeVariantOptionClick(key, v) {
        this.presenter.removeVariantOptionClick(key, v);
    }

    getProductName() {
        return document.getElementById("product_name").value;
    }

    getProductDescription() {
        return document.getElementById("product_description").value;
    }

    getProductPrice() {
        let price = document.getElementById("product_price");
        if (price) {
            return parseInt(price.value);
        }
        return 0;
    }

    getProductStock() {
        let stock = document.getElementById("product_stock");
        if (stock) {
            return parseInt(stock.value);
        }
        return 0;
    }

    variantChange(newKey, key) {
        this.presenter.variantChange(newKey, key);
    }

    variantOptionChange(key, value, newValue) {
        this.presenter.variantOptionChange(key, value, newValue);
    }

    renderImageSection() {
        let {images} = this.state;
        return (
            <div className="card border-0 rounded-0 my-3">
                <div className="card-body">
                    <div className="p-4">
                        <div className="header">
                            <h5 className="mb-0 text-muted">Product Images</h5>
                            <p className="text-muted m-0 font-size-xs mb-4 float-left">Drag to reorder your images.
                                Every product can have up to 9 images.</p>
                            <span
                                className="text-muted m-0 font-size-xs mb-4 float-right"><b>5</b> / 9 images</span>
                            <div className="clearfix"></div>
                        </div>
                        <div className="img-upload">
                            <ul className="img-list">
                                <li className="mx-1">
                                    <div className="img-item text-center">
                                        <div className="form-group btn-upload-img">
                                            <label htmlFor="file" className="input-label font-size-xs text-primary">
                                                <i className="fas fa-images font-size-md"></i><br/>UPLOAD
                                                IMAGE</label>
                                            <input type="file" id="file" onChange={this.addImageChange} multiple/>
                                        </div>
                                    </div>
                                </li>
                                {
                                    images.map((image, index) => {
                                        return (
                                            <ImageLoader key={`image_loader_` + index}
                                                         thumbnailClick={() => this.imageThumbnailClick(image)}
                                                         deleteClick={() => this.imageDeleteClick(image)}
                                                         image={image}/>)
                                    })
                                }
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        )
    }

    renderBatchEdit() {
        return (<div className="col-sm-12">
            <div className="batchedit">
                <label className="font-size-sm mt-4 text-muted">Batch Edit</label>
                <div className="row">
                    <div className="col-sm-3 px-1">
                        <input type="text"
                               className="form-control font-size-xs mb-2 rounded-0"
                               placeholder="Price"/>
                    </div>
                    <div className="col-sm-3 px-1">
                        <input type="text"
                               className="form-control font-size-xs mb-2 rounded-0"
                               placeholder="Stock"/>
                    </div>
                    <div className="col-sm-3 px-1">
                        <input type="text"
                               className="form-control font-size-xs mb-2 rounded-0"
                               placeholder="SKU"/>
                    </div>
                    <div className="col-sm-3 px-1">
                        <button
                            className="btn-outline-custom font-size-xs d-block py-2 w-100">Apply
                            to all
                        </button>
                    </div>
                </div>
            </div>
            <table className="w-100 mt-4">
                <tr>
                    <th className="font-size-xs text-muted">Size</th>
                    <th className="font-size-xs text-muted">Color</th>
                    <th className="font-size-xs text-muted">Price</th>
                    <th className="font-size-xs text-muted">Stock</th>
                    <th className="font-size-xs text-muted">SKU</th>
                </tr>
                <tr>
                    <td><input type="text"
                               className="form-control font-size-xs mb-2 border-0"/></td>
                    <td><input type="text"
                               className="form-control font-size-xs mb-2 border-0"/></td>
                    <td><input type="text"
                               className="form-control font-size-xs mb-2 border-0"/></td>
                    <td><input type="text"
                               className="form-control font-size-xs mb-2 border-0"/></td>
                    <td><input type="text"
                               className="form-control font-size-xs mb-2 border-0"/></td>
                </tr>
                <tr>
                    <td className="font-size-xs">Medium</td>
                    <td className="font-size-xs">Red</td>
                    <td className="font-size-xs">₱ 200.00</td>
                    <td className="font-size-xs">24</td>
                    <td className="font-size-xs">123123asdasd</td>
                </tr>
            </table>
        </div>);
    }

    renderProductInfo() {
        return (
            <div className="card border-0 rounded-0 mb-3">
                <div className="card-body">
                    <div className="p-4">
                        <div className="header">
                            <div className="row">
                                <div className="col-sm-5">
                                    <h5 className="mb-0 text-muted mb-4">Product Information</h5>
                                    <div className="form-group">
                                        <div className="row">
                                            <div className="col-sm-12">
                                                <label className="font-size-xs text-muted float-left">Product
                                                    Name</label>

                                                <input type="text" className="form-control font-size-xs mb-2"
                                                       id="product_name" required/>

                                                <label className="font-size-xs text-muted float-left">Product
                                                    Description</label>
                                                <textarea
                                                    className="d-block w-100 form-control font-size-xs mb-2"
                                                    id="product_description" required></textarea>


                                                {/*<label*/}
                                                {/*className="font-size-xs text-muted float-left">Category</label>*/}
                                                {/*<select className="form-control font-size-xs mb-2">*/}
                                                {/*<option value="volvo">Volvo</option>*/}
                                                {/*<option value="saab">Saab</option>*/}
                                                {/*<option value="mercedes">Mercedes</option>*/}
                                                {/*<option value="audi">Audi</option>*/}
                                                {/*</select>*/}


                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }

    renderProductInventory() {
        let {variant} = this.state;
        return (
            <div className="card border-0 rounded-0 mb-3">
                <div className="card-body">
                    <div className="row">
                        <div className="col-sm-5">
                            <h5 className="mb-0 text-muted mb-4">Price and Inventory</h5>
                            {
                                Object.keys(variant).length == 0 &&
                                <Fragment>
                                    <label className="font-size-xs text-muted float-left">Price</label>
                                    <input type="number" className="form-control font-size-xs mb-2"
                                           id="product_price" required/>
                                    <label className="font-size-xs text-muted float-left">Stock</label>
                                    <input type="number" className="form-control font-size-xs mb-2"
                                           id="product_stock"/>
                                </Fragment>
                            }

                        </div>
                    </div>
                    <div className="col-sm-7">
                        {
                            this.renderProductVariant()
                        }

                        <div className="text-center">
                            <button type="button" onClick={this.addVariantClick}
                                    className="btn-outline-custom font-size-xs d-block py-2 w-100"><i
                                className="fas fa-plus"></i>&nbsp;&nbsp;Add Variation
                            </button>
                        </div>
                    </div>

                    {
                        Object.keys(variant).length > 0 &&
                        this.renderVariantList()
                    }

                </div>

            </div>

        )
    }

    renderShipping() {
        return (
            <div className="card border-0 rounded-0 mb-3">
                <div className="card-body">
                    <div className="myshop p-4">
                        <div className="header">
                            <div className="row">
                                <div className="col-sm-5">
                                    <h5 className="mb-0 text-muted mb-2">Shipping</h5>
                                    <p className="text-muted m-0 font-size-xs mb-4">Lorem ipsum dolor sit amet,
                                        consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean
                                        massa.</p>
                                    <div className="form-group">
                                        <div className="row m-0">
                                            <div className="col-sm-12 px-1">
                                                <label className="font-size-xs text-muted">Product Name</label>
                                                <input type="text"
                                                       className="form-control font-size-xs mb-2 float-left"
                                                       style={{width: "90%"}}/>
                                                <span
                                                    className="float-right font-size-xs text-muted pt-2 text-center"
                                                    style={{width: "10%"}}>KG</span>
                                                <div className="clearfix"></div>
                                            </div>
                                            <div className="col-sm-4 px-1">
                                                <input type="text" placeholder="W"
                                                       className="form-control font-size-xs mb-2"/>
                                            </div>
                                            <div className="col-sm-4 px-1">
                                                <input type="text" placeholder="L"
                                                       className="form-control font-size-xs mb-2"/>
                                            </div>
                                            <div className="col-sm-4 px-1">
                                                <input type="text" placeholder="H"
                                                       className="form-control font-size-xs mb-2"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-sm-7">
                                    <div className="p-4" style={{background: "#F1F1F1"}}>
                                        <h1 className="text-secondary font-size-sm">Shipping Included</h1>
                                        <p className="font-size-xs text-muted">Lorem ipsum dolor sit amet,
                                            consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean
                                            massa. Cum sociis natoque penatibus et magnis dis parturient montes,
                                            nascetur ridiculus mus. <a className="font-size-xs text-primary">Click
                                                Here</a></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

    renderOther() {
        return (
            <div className="card border-0 rounded-0">
                <div className="card-body mb-3">
                    <div className="myshop p-4">
                        <div className="header">
                            <div className="row">
                                <div className="col-sm-5">
                                    <h5 className="mb-0 text-muted mb-2">Others</h5>
                                    <p className="text-muted m-0 font-size-xs mb-4">Lorem ipsum dolor sit amet,
                                        consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean
                                        massa.</p>
                                    <div className="form-group">
                                        <div className="row">
                                            <div className="col-sm-12">
                                                <label className="font-size-xs text-muted">Condition</label>
                                                <select className="form-control font-size-xs mb-2">
                                                    <option value="volvo">Volvo</option>
                                                    <option value="saab">Saab</option>
                                                    <option value="mercedes">Mercedes</option>
                                                    <option value="audi">Audi</option>
                                                </select>

                                                <label className="font-size-xs text-muted">Parent SKU</label>
                                                <div className="clearfix"></div>
                                                <input type="text" className="form-control font-size-xs mb-2"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-sm-7">
                                    <div className="p-4" style={{background: "#F1F1F1"}}>
                                        <h1 className="text-secondary font-size-sm">Shipping Included</h1>
                                        <p className="font-size-xs text-muted">Lorem ipsum dolor sit amet,
                                            consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean
                                            massa. Cum sociis natoque penatibus et magnis dis parturient montes,
                                            nascetur ridiculus mus. <a className="font-size-xs text-primary">Click
                                                Here</a></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

    renderProductVariant() {
        let {variant} = this.state;

        return Object.keys(variant).map((position, index) => {
            let key = variant[position]["key"];
            let variants = variant[position]["variants"];
            return (
                <Fragment position={`variant_position` + index}>
                    <h5 className="font-size-sm text-secondary mt-3 my-2 text-center">Variation {index + 1}</h5>
                    <div className="p-4 mb-4" style={{background: "#F1F1F1"}}>
                        <div className="text-right">
                            <button onClick={() => this.removeVariantClick(position)} type="button"
                                    className="btn btn-link">
                                <i className="fas fa-times"></i>
                            </button>
                        </div>
                        <div className="col-sm-12">
                            <label className="font-size-xs text-muted">Variation Name</label>
                            <input onChange={(e) => this.variantChange(position, e.target.value)} type="text"
                                   className="form-control font-size-xs mb-2"
                                   placeholder="Enter Variation Name, eg: Color Size etc." defaultValue={key}
                                   required/>

                            <label className="font-size-xs text-muted float-left">Variation Options</label>
                            {
                                variants.map((v, index) => {
                                    return (
                                        <Fragment position={`variant_option_position` + index}>
                                            <input
                                                onChange={(e) => this.variantOptionChange(position, v, e.target.value)}
                                                type="text" className="form-control font-size-xs mb-2"
                                                placeholder="Enter Variation Options, eg: Red, Green, Small, Medium etc."
                                                required defaultValue={v.length > 1 ? v : ""}/>
                                            <div className="text-right">
                                                <button onClick={() => this.removeVariantOptionClick(position, v)}
                                                        type="button" className="btn btn-link">
                                                    <i className="fas fa-times"></i>
                                                </button>
                                            </div>
                                        </Fragment>
                                    )
                                })
                            }
                            <button type="button" onClick={() => this.addVariantOptionClick(position)}
                                    className="btn-outline-custom font-size-xs d-block py-2 w-100 mt-4">
                                <i className="fas fa-plus"></i>&nbsp;&nbsp;Add Variation Options
                            </button>
                        </div>
                    </div>
                </Fragment>
            )
        })

    }

    renderVariantList() {
        let {variant} = this.state;
        let {productVariants} = this.state;
        return (
            <Fragment>
                <h5 className="mb-0 text-muted mb-4">Variant List</h5>
                <table className="table">
                    <thead>
                    <tr>
                        {
                            Object.keys(variant).map((position) => {
                                let key = variant[position].key;
                                return <th className="font-size-xs text-muted">{key}</th>
                            })
                        }
                        <th className="font-size-xs text-muted">Price</th>
                        <th className="font-size-xs text-muted">Stock</th>
                    </tr>
                    </thead>
                    <tbody>
                    {
                        productVariants.map((product) => {
                            let variant = product.variant;
                            return (
                                <Fragment>
                                    <tr>
                                        {
                                            Object.keys(variant).map((key) => {
                                                return (<td>{variant[key]}</td>)
                                            })
                                        }
                                    </tr>
                                    <tr>
                                        <td>
                                            <input onChange={(e) => product.price = parseInt(e.target.value)}
                                                   type="number"
                                                   className="form-control font-size-xs mb-2 rounded-0"
                                                   required/>
                                        </td>
                                        <td>
                                            <input onChange={(e) => product.stock = parseInt(e.target.value)}
                                                   type="number"
                                                   className="form-control font-size-xs mb-2 rounded-0"
                                                   required/>
                                        </td>
                                    </tr>
                                </Fragment>

                            )
                        })
                    }
                    </tbody>
                </table>
            </Fragment>

        )
    }

    render() {
        return (
            <form onSubmit={this.formSubmit}>
                {
                    this.renderImageSection()
                }
                {
                    this.renderProductInfo()
                }
                {
                    this.renderProductInventory()
                }


                <button className="btn btn-dark font-size-xs text-white mt-4">Cancel</button>
                <button type="submit" className="btn btn-primary font-size-xs text-white mr-2 mt-4"><i
                    className="fas fa-save"></i>&nbsp;&nbsp;Save Product
                </button>
            </form>

        );
    }
}

export default AddProduct;
