import React, {Component} from "react";
import {Link} from 'react-router-dom';
import ShopProductsPresenter from "./ShopProductsPresenter";
import {change_status_color, getPrice, getProductThumbnail} from "../../utils";
import "./style.css";

class ShopProducts extends Component {
    constructor() {
        super();
        this.presenter = new ShopProductsPresenter(this);
        this.state = {
            products: [],
        };
    }

    componentDidMount() {
        this.presenter.componentDidMount();
    }

    setProducts(products) {
        this.setState({products: products});
    }

    render() {
        let {products} = this.state;
        return (
            <fragment>
                <div className="header-shop p-4 text-right">
                    <div className="d-inline-block ml-2">
                        <Link to="/seller/addproduct" className="btn btn-primary py-2 text-white font-size-sm"
                              style={{"margin-top": "-4px"}}>
                            <i className="fas fa-plus"></i>&nbsp;&nbsp;Add New Product
                        </Link>
                    </div>
                </div>
                <ul className="nav nav-tabs mt-3" role="tablist">
                    <li className="nav-item">
                        <a className="nav-link active p-3 px-4" id="all-tab" data-toggle="tab" href="#all" role="tab"
                           aria-controls="all" aria-selected="true"><i className="fas fa-list mr-2 font-size-sm"></i>All</a>
                    </li>
                    <li className="nav-item">
                        <a className="nav-link p-3 px-4" id="live-tab" data-toggle="tab" href="#live" role="tab"
                           aria-controls="live" aria-selected="false"><i
                            className="far fa-eye"></i>&nbsp;&nbsp;&nbsp;Live</a>
                    </li>
                    <li className="nav-item">
                        <a className="nav-link p-3 px-4" id="sold-tab" data-toggle="tab" href="#sold" role="tab"
                           aria-controls="sold" aria-selected="false"><i
                            className="fas fa-box-open"></i>&nbsp;&nbsp;&nbsp;Sold out</a>
                    </li>
                    <li className="nav-item">
                        <a className="nav-link p-3 px-4" id="suspend-tab" data-toggle="tab" href="#suspend" role="tab"
                           aria-controls="suspend" aria-selected="false"><i
                            className="fas fa-pause-circle"></i>&nbsp;&nbsp;&nbsp;Suspended</a>
                    </li>
                    <li className="nav-item">
                        <a className="nav-link p-3 px-4" id="unlist-tab" data-toggle="tab" href="#unlist" role="tab"
                           aria-controls="unlist" aria-selected="false"><i
                            className="far fa-eye-slash"></i>&nbsp;&nbsp;&nbsp;Unlisted</a>
                    </li>
                </ul>

                <div className="tab-content" id="myTabContent">
                    <div className="tab-pane fade show border-bottom border-left border-right p-3 mb-4 active bg-white"
                         id="all" role="tabpanel" aria-labelledby="all-tab">
                        <div className="table-responsive table-striped" style={{minHeight: '400px'}}>
                            <table className="table">
                                <thead>
                                <tr>
                                    <th scope="col" className="border-0">THUMBNAIL</th>
                                    <th scope="col" className="border-0">NAME</th>
                                    <th scope="col" className="border-0">PRICE</th>
                                    <th scope="col" className="border-0">STOCK</th>
                                </tr>
                                </thead>
                                <tbody>
                                {
                                    products.map((product) => {
                                        let createdAt = product.get("createdAt");
                                        let thumbnail = getProductThumbnail(product);
                                        let name = product.get("name");
                                        let price = product.get("price");
                                        let stock = product.get("stock");
                                        return (
                                            <tr className="border-0">
                                                <td scope="row" className="border-0 font-size-xs align-middle">
                                                    <img src={thumbnail} className="img-fluid"
                                                         className="img-fluid h-auto" style={{width: "80px"}}></img>
                                                </td>
                                                <td scope="row" className="border-0 font-size-xs align-middle">
                                                    <h5 className="font-size-sm mt-3"><b>{name}</b></h5>
                                                </td>
                                                <td className="border-0 font-size-xs align-middle">
                                                    <h5 className="font-size-sm mt-3"><b>{price}</b></h5>
                                                </td>
                                                <td className="border-0 font-size-xs align-middle">
                                                    <h5 className="font-size-xs mt-3 badge badge-pill font-weight-bold text-white badge-secondary"> {stock}</h5>
                                                </td>
                                            </tr>
                                        )
                                    })
                                }
                                </tbody>
                            </table>
                        </div>
                    </div>


                    <div className="tab-pane fade p-3 px-4 bg-white border-bottom border-left border-right" id="live"
                         role="tabpanel" aria-labelledby="live-tab">live
                    </div>
                    <div className="tab-pane fade p-3 px-4 bg-white border-bottom border-left border-right" id="sold"
                         role="tabpanel" aria-labelledby="sold-tab">sold
                    </div>
                    <div className="tab-pane fade p-3 px-4 bg-white border-bottom border-left border-right" id="suspend"
                         role="tabpanel" aria-labelledby="suspend-tab">suspend
                    </div>
                    <div className="tab-pane fade p-3 px-4 bg-white border-bottom border-left border-right" id="unlist"
                         role="tabpanel" aria-labelledby="unlist-tab">unlist
                    </div>

                </div>
            </fragment>
        );
    }
}

export default ShopProducts;
