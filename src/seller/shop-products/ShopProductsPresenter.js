import Parse from "parse";

class ShopProductsPresenter {
    constructor(view) {
        this.view = view;
    }

    componentDidMount() {
        this.getProduct()
    }

    async getProduct() {
        //get all have variantions product
        let variantQuery = new Parse.Query("Product");
        variantQuery.matchesQuery("variations", new Parse.Query("Product"));
        //get all product don't have variant
        let notVariantQuery = new Parse.Query("Product");
        notVariantQuery.doesNotExist("variant");

        let mainQuery = Parse.Query.or(variantQuery, notVariantQuery);
        mainQuery.includeAll();
        let products = await mainQuery.find();
        this.view.setProducts(products);
    }
}

export default ShopProductsPresenter
