import React, {Component, Fragment} from "react";
import {Link} from 'react-router-dom';
import {change_status_color} from "../../utils";
import './style.css';

class shopshipping extends Component {
    render() {
        return (
            <div className="card">
                <div className="card-body">
                <div className="myshop p-4">
                    <div className="header">
                        <h5 className="mb-0 text-muted">My Shipping</h5>
                        <p className="text-muted m-0 font-size-xs mb-4">Set your preferred shipping options and shipping fee</p>
                    </div>
                    <h4 className="font-size-sm mb-4">More Shipping Options</h4>
                    <div id="accordion">
                        <div className="card rounded-0 border-0">
                            <div className="card-header bg-white px-3 pt-3 pb-2" id="headingOne">
                                <h5 className="m-0 float-left font-size-sm pt-1" style={{width:"88%"}}>LBC Express</h5>
                                <div className="float-right p-0 ctrl-ship" style={{width:"12%"}}>
                                    <div className="float-left pr-1" style={{width:"50%"}}>
                                        <div className="row justify-content-between">
                                            <label className="switch">
                                                <input type="checkbox"/>
                                                <span className="slider round"></span>
                                            </label>
                                        </div>
                                    </div>
                                    <div className="float-right border-left pl-2" style={{width:"50%"}}>
                                        <a href="#" className="m-0" data-toggle="collapse" data-target="#collapseOne"
                                           aria-expanded="true" aria-controls="collapseOne">
                                            <i className="fas fa-angle-down"></i>
                                        </a>
                                    </div>
                                    <div className="clearfix"></div>
                                </div>
                                <div className="clearfix"></div>
                            </div>

                            <div id="collapseOne" className="collapse" aria-labelledby="headingOne"
                                 data-parent="#accordion">
                                <div className="card-body">
                                    <div className="row">
                                        <div className="col-sm-8 border-right">
                                            <p className="font-size-sm">
                                                Enable this option if you offer shipping via LBC Express.
                                            </p>
                                        </div>
                                        <div className="col-sm-4">
                                            <div className="row">
                                                <div className="col-9">
                                                    <p className="font-size-sm">Enable this channel</p>
                                                </div>
                                                <div className="col-3">
                                                    <div className="row justify-content-between float-right">
                                                        <label className="switch">
                                                            <input type="checkbox"/>
                                                            <span className="slider round"></span>
                                                        </label>
                                                    </div>
                                                    <div className="clearfix"></div>
                                                </div>
                                                <div className="col-9">
                                                    <p className="font-size-sm">Set as preferred shipping option
                                                    </p>
                                                </div>
                                                <div className="col-3">
                                                    <div className="row justify-content-between float-right">
                                                        <label className="switch">
                                                            <input type="checkbox"/>
                                                            <span className="slider round"></span>
                                                        </label>
                                                    </div>
                                                    <div className="clearfix"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div className="card rounded-0 border-0">
                            <div className="card-header bg-white px-3 pt-3 pb-2" id="headingTwo">
                                <h5 className="m-0 float-left font-size-sm pt-1" style={{width:"88%"}}>LBC Express</h5>
                                <div className="float-right p-0 ctrl-ship" style={{width:"12%"}}>
                                    <div className="float-left pr-1" style={{width:"50%"}}>
                                        <div className="row justify-content-between">
                                            <label className="switch">
                                                <input type="checkbox"/>
                                                <span className="slider round"></span>
                                            </label>
                                        </div>
                                    </div>
                                    <div className="float-right border-left pl-2" style={{width:"50%"}}>
                                        <a href="#" className="m-0" data-toggle="collapse" data-target="#collapseTwo"
                                           aria-expanded="true" aria-controls="collapseTwo">
                                            <i className="fas fa-angle-down"></i>
                                        </a>
                                    </div>
                                    <div className="clearfix"></div>
                                </div>
                                <div className="clearfix"></div>
                            </div>

                            <div id="collapseTwo" className="collapse" aria-labelledby="headingTwo"
                                 data-parent="#accordion">
                                <div className="card-body">
                                    <div className="row">
                                        <div className="col-sm-8 border-right">
                                            <p className="font-size-sm">
                                                Enable this option if you offer shipping via LBC Express.
                                            </p>
                                        </div>
                                        <div className="col-sm-4">
                                            <div className="row">
                                                <div className="col-9">
                                                    <p className="font-size-sm">Enable this channel</p>
                                                </div>
                                                <div className="col-3">
                                                    <div className="row justify-content-between float-right">
                                                        <label className="switch">
                                                            <input type="checkbox"/>
                                                            <span className="slider round"></span>
                                                        </label>
                                                    </div>
                                                    <div className="clearfix"></div>
                                                </div>
                                                <div className="col-9">
                                                    <p className="font-size-sm">Set as preferred shipping option
                                                    </p>
                                                </div>
                                                <div className="col-3">
                                                    <div className="row justify-content-between float-right">
                                                        <label className="switch">
                                                            <input type="checkbox"/>
                                                            <span className="slider round"></span>
                                                        </label>
                                                    </div>
                                                    <div className="clearfix"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        );
    }
}

export default shopshipping;