import React, {Component, Fragment} from "react";
import ItemProduct from "../../website/ItemProduct";
import {Link} from "react-router-dom";
import ShopOrderPresenter from "./ShopOrderPresenter";
import {change_status_color} from "../../utils";
import {create_table_row_status} from "../../builder";

import "./style.css";
import Pager from "../../components/Pager";
import ModalEnterTrackNumber from "./ModalEnterTrackNumber";
import ModalReceivedBy from "./ModalReceivedBy";

class OrderItem extends Component {
    constructor() {
        super();
        this.state = {items: []};
    }

    componentDidMount() {
        this.fetchProducts();
    }

    async fetchProducts() {
        let order = this.props.children;
        let relation = order.get("items");
        let query = relation.query();
        query.include("product.thumbnail");
        let items = await query.find();
        this.setState({items: items});
    }

    render() {

        let {items} = this.state;
        let order = this.props.children;
        let id = order.id;
        let status = order.get("status");
        let span_color = change_status_color[status];
        let span_class = ['font-size-xs text-white rounded p-1 px-2'];
        span_class.push(span_color ? span_color : "bg-dark");

        return (
            <Fragment>
                <div className="table-responsive">
                    <table className="table" style={{width: "700px"}}>
                        <thead>
                        <tr>
                            <th scope="col" className="border-0"></th>
                            <th scope="col" className="border-0">ITEM(S)</th>
                            <th scope="col" className="border-0">QUANTITY</th>
                            <th scope="col" className="border-0">STATUS</th>
                        </tr>
                        </thead>
                        <tbody>
                        {
                            items.map((item) => {
                                let id = item.id;
                                let name = item.get("name");
                                let quantity = item.get("quantity");
                                let product = item.get("product");
                                let variant = item.get("variant");
                                variant = variant ? variant : {};
                                return (
                                    <tr key={id} className="border-0">
                                        <td scope="row" className="border-0" style={{width: '280px'}}>
                                            <ItemProduct no_footer={true}>{product}</ItemProduct>
                                        </td>
                                        <td scope="row"
                                            className="border-0 pt-4 text-primary"
                                            style={{width: '350px'}}>
                                            <h6 className="m-0">{name}</h6>
                                            {
                                                Object.keys(variant).map((key) => {
                                                    let value = variant[key];
                                                    return (
                                                        <label key={key}
                                                               className="text-muted font-size-xs">{`${key} ${value}`}</label>
                                                    )
                                                })
                                            }
                                        </td>
                                        <td className="border-0 pt-4" style={{width: '220px'}}>
                                            <label>{quantity}</label>
                                        </td>
                                        <td className="border-0 pt-4 font-size-sm" style={{width: '220px'}}>
                                            {
                                                create_table_row_status(order)
                                            }
                                        </td>
                                    </tr>
                                )
                            })
                        }
                        </tbody>
                    </table>
                </div>
            </Fragment>
        );
    }
}

class ShopOrder extends Component {

    constructor() {
        super();
        this.presenter = new ShopOrderPresenter(this);
        this.state = {
            orders: [],
            pageCount: 0,
            perPage: 10,
            currentPage: 1,
            isShowModalTrackNumber: false,
            isShowModalReceivedBy: false,
        };
    }

    componentDidMount() {
        this.presenter.componentDidMount();
    }

    setOrders(orders) {
        this.setState({orders: orders});
    }

    showModalTrackNumber() {
        this.setState({isShowModalTrackNumber: true});
    }

    hideModalTrackNumber() {
        this.setState({isShowModalTrackNumber: false});
    }
    showModalReceivedBy() {
        this.setState({isShowModalReceivedBy: true});
    }
    hideModalReceivedBy() {
        this.setState({isShowModalReceivedBy: false});
    }

    onPageNavigate = (page) => {
        this.presenter.pageNavigate(page);
    }

    getPerpage() {
        let {perPage} = this.state;
        return perPage;
    }

    setCurrentPage(page) {
        this.setState({currentPage: page});
    }

    setPerpage(perPage, callback) {
        this.setState({perPage: perPage}, callback);
    }

    getCurrentPage() {
        let {currentPage} = this.state;
        return currentPage;
    }

    setPageCount(count) {
        this.setState({pageCount: count});
    }

    readyToShipClick(e, order) {
        e.preventDefault();
        this.presenter.readyToShipClick(order);
    }

    inTransitClick(e, order) {
        e.preventDefault();
        this.presenter.inTransitClick(order);
    }
    deliveredClick(e, order) {
        e.preventDefault();
        this.presenter.deliveredClick(order);
    }

    cancelClick(e, order) {
        e.preventDefault();
        this.presenter.cancelClick(order);
    }

    update() {
        this.forceUpdate();
    }

    trackNumberSubmit =(e)=>{
        e.preventDefault();
        let track_number = e.target["modal_enter_track_number"].value;
        this.presenter.trackNumberSubmit(track_number);
    }
    receivedBySubmit =(e)=>{
        e.preventDefault();
        let received_by = e.target["modal_received_by"].value;
        this.presenter.receivedBySubmit(received_by);
    }

    getActions(order) {
        let status = order.get("status");
        let actions = [];

        let readyToShip = (
            <a onClick={(e) => this.readyToShipClick(e, order)} href="#" className="dropdown-item">READY TO SHIP</a>
        );
        let inTransit = (
            <a onClick={(e) => this.inTransitClick(e, order)} href="#" className="dropdown-item">IN TRANSIT</a>
        );
        let cancel = (
            <a onClick={(e) => this.cancelClick(e, order)} href="#" className="dropdown-item">CANCEL</a>
        );
        let delivered = (
            <a onClick={(e) => this.deliveredClick(e, order)} href="#" className="dropdown-item">DELIVERED</a>
        );

        if (status === "ON PROCESS") {
            actions = [readyToShip, cancel];
        }

        if (status === "READY TO SHIP") {
            actions = [inTransit];
        }
        if (status === "IN TRANSIT") {
            actions = [delivered];
        }

        return (
            <div className="dropdown-menu font-size-xs">
                {
                    actions
                }
            </div>
        )
    }


    render() {
        let extra;
        let {orders} = this.state;
        let {perPage, currentPage, pageCount} = this.state;
        if (this.state.isShowModalTrackNumber) {
            extra = (<ModalEnterTrackNumber close={this.hideModalTrackNumber.bind(this)} onSubmit={this.trackNumberSubmit}/>);
        }
        if (this.state.isShowModalReceivedBy) {
            extra = (<ModalReceivedBy close={this.hideModalReceivedBy.bind(this)} onSubmit={this.receivedBySubmit}/>);
        }
        return (
            <Fragment>
                <div className="header-shop p-4">
                    <div className="row">
                        <div className="col-sm-6">
                            <label className="d-inline-block mr-2 font-size-sm">View Data</label>
                            <select className="form-control d-inline-block font-size-sm"  style={{width: "70px"}}>
                                <option>10</option>
                                <option>20</option>
                                <option>50</option>
                                <option>80</option>
                                <option>100</option>
                            </select>
                            <form action="../print.html" className="d-inline-block">
                                <button className="btn btn-primary py-2 text-white font-size-sm ml-2" style={{"margin-top":"-2px"}}>
                                    <i className="fas fa-print"></i>&nbsp;&nbsp;Print Reciept
                                </button>
                            </form>
                        </div>
                        <div className="col-sm-6 text-right">
                            <div className="d-inline-block ml-2">
                                <input type="text" className="form-control font-size-sm" placeholder="Search" />
                            </div>
                            <div className="d-inline-block ml-2">
                                <select className="form-control font-size-sm">
                                    <option value="" disabled selected>Sort By</option>
                                    <option>ORDER ID</option>
                                    <option>ORDER DATE</option>
                                    <option>STATUS</option>
                                    <option>TOTAL</option>
                                    <option>ORDER BY</option>
                                </select>
                            </div>
                            <div className="d-inline-block ml-2">
                                <button className="btn btn-primary py-2 text-white font-size-sm" style={{"margin-top":"-4px"}}>
                                    <i className="fas fa-search"></i>&nbsp;&nbsp;Search
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <ul className="nav nav-tabs mt-3" role="tablist">
                    <li className="nav-item">
                        <a className="nav-link active p-3 px-4" id="all-tab" data-toggle="tab" href="#all" role="tab"
                           aria-controls="all" aria-selected="true"><i className="fas fa-list mr-2 font-size-sm"></i>All</a>
                    </li>
                    <li className="nav-item">
                        <a className="nav-link p-3 px-4" id="process-tab" data-toggle="tab" href="#process" role="tab"
                           aria-controls="process" aria-selected="false"><i className="fas fa-spinner mr-2 font-size-sm"></i>&nbsp;&nbsp;&nbsp;On Progress</a>
                    </li>
                    <li className="nav-item">
                        <a className="nav-link p-3 px-4" id="ready-tab" data-toggle="tab" href="#ready" role="tab"
                           aria-controls="ready" aria-selected="false"><i className="fas fa-box mr-2 font-size-sm"></i>&nbsp;&nbsp;&nbsp;Ready to Ship</a>
                    </li>
                    <li className="nav-item">
                        <a className="nav-link p-3 px-4" id="ship-tab" data-toggle="tab" href="#ship" role="tab"
                           aria-controls="ship" aria-selected="false"><i className="fas fa-shipping-fast mr-2 font-size-sm"></i>&nbsp;&nbsp;&nbsp;Shipped</a>
                    </li>
                    <li className="nav-item">
                        <a className="nav-link p-3 px-4" id="deliver-tab" data-toggle="tab" href="#deliver" role="tab"
                           aria-controls="deliver" aria-selected="false"><i className="fas fa-check-circle mr-2 font-size-sm"></i>&nbsp;&nbsp;&nbsp;Delivered</a>
                    </li>
                    <li className="nav-item">
                        <a className="nav-link p-3 px-4" id="cancel-tab" data-toggle="tab" href="#cancel" role="tab"
                           aria-controls="cancel" aria-selected="false"><i className="fas fa-times-circle mr-2 font-size-sm"></i>Cancel</a>
                    </li>
                    {/*<li className="nav-item">*/}
                        {/*<a className="nav-link p-3 px-4" id="return-tab" data-toggle="tab" href="#return" role="tab"*/}
                           {/*aria-controls="return" aria-selected="false"><i className="fas fa-undo-alt mr-2 font-size-sm"></i>Return/Refund</a>*/}
                    {/*</li>*/}
                </ul>
                <div className="tab-content" id="myTabContent">
                    <div className="tab-pane fade show border-bottom border-left border-right p-3 mb-4 active bg-white" id="all" role="tabpanel"
                         aria-labelledby="all-tab">
                        <div className="table-responsive table-striped" style={{minHeight: '400px'}}>
                            <table className="table">
                                <thead>
                                <tr>
                                    <th className="border-0">
                                        <input className="form-check form-check-inline" type="checkbox"/>
                                    </th>
                                    <th scope="col" className="border-0"></th>
                                    <th scope="col" className="border-0 text-nowrap">ORDER ID</th>
                                    <th scope="col" className="border-0 text-nowrap">ORDER DATE</th>
                                    <th scope="col" className="border-0 text-nowrap">STATUS</th>
                                    <th scope="col" className="border-0 text-nowrap">TOTAL</th>
                                    <th scope="col" className="border-0 text-nowrap">PAYMENT</th>
                                    <th scope="col" className="border-0 text-nowrap">ORDER BY</th>
                                    <th scope="col" className="border-0 text-nowrap">ACTION</th>
                                </tr>
                                </thead>
                                <tbody>
                                {
                                    orders.map((order) => {
                                        let id = order.id;
                                        let buyer = order.get("buyer");
                                        let buyer_name = buyer.get("name");
                                        let createdAt = order.get("createdAt");
                                        let order_date = createdAt.toLocaleDateString();

                                        let status = order.get("status");
                                        let order_total = order.get("order_total");
                                        let payment = order.get("payment");
                                        let ship_to = order.get("ship_to");
                                        let notes = order.get("notes");
                                        let shipping_total = order.get("shipping_total");
                                        let item_total = order.get("item_total");
                                        let span_color = change_status_color[status];
                                        return (
                                            <Fragment key={id}>
                                                <tr className="border-0">
                                                    <td className="align-middle border-0"
                                                        style={{width: "20px"}}>{/*render checkbox*/}
                                                        <input className="form-check form-check-inline" type="checkbox"
                                                               readOnly/>
                                                    </td>
                                                    <td className="align-middle border-0" style={{width: "20px"}}>
                                                        <button className="btn btn-sm btn-link font-size-xs"
                                                                data-toggle="collapse" data-target={`#shop_order_${id}`}>
                                                            <i className="fas fa-plus"></i></button>
                                                    </td>
                                                    <td className="text-truncate font-size-xs align-middle border-0">
                                                        <div className="d-inline-block">{id}</div>
                                                    </td>
                                                    <td className="text-truncate font-size-xs align-middle border-0">
                                                        <div className="d-inline-block">{order_date}</div>
                                                    </td>
                                                    <td className="text-truncate font-size-xs align-middle border-0">
                                                        {
                                                            create_table_row_status(order)
                                                        }
                                                    </td>
                                                    <td className="text-truncate font-size-xs align-middle border-0">
                                                        {order_total}
                                                    </td>
                                                    <td className="text-truncate font-size-xs align-middle border-0">
                                                        {payment}
                                                    </td>
                                                    <td className="text-truncate font-size-xs align-middle border-0">
                                                        {buyer_name}
                                                    </td>
                                                    <td className="border-0 align-middle">
                                                        <div className="dropdown">
                                                            <button className="btn btn-link text-muted" type="button"
                                                                    data-toggle="dropdown">
                                                                <i className="fas fa-ellipsis-v"/>
                                                            </button>
                                                            {
                                                                this.getActions(order)
                                                            }
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td className="border-0 p-0" colSpan="9">
                                                        <div id={`shop_order_${id}`} className="collapse container-fluid">
                                                            <div className="mb-3 bg-white">
                                                                <div>
                                                                    <div className="row">
                                                                        <div className="col-sm-6">
                                                                            <h5 className="font-size-sm bg-light text-dark p-3">Delivery
                                                                                Address & Billing Address</h5>
                                                                            <div className="pl-3">
                                                                                <label
                                                                                    className="font-size-xs d-block text-uppercase text-muted">Ship
                                                                                    to:</label>
                                                                                <p style={{whiteSpace: "pre-line"}}>{ship_to}</p>
                                                                                <label
                                                                                    className="font-size-xs d-block text-uppercase text-muted">Notes:</label>
                                                                                <p style={{whiteSpace: "pre-line"}}>{notes}</p>
                                                                            </div>
                                                                        </div>
                                                                        <div className="col-sm-6">
                                                                            <h5 className="font-size-sm bg-light text-dark p-3">Summary</h5>
                                                                            <div className="pl-3">
                                                                                <label
                                                                                    className="text-uppercase text-muted float-left mb-2 font-size-sm">Shipping
                                                                                    Fee</label>
                                                                                <p className="float-right mb-2">₱ {shipping_total}</p>
                                                                                <div className="clearfix"></div>
                                                                                <label
                                                                                    className="text-uppercase text-muted float-left mb-2 font-size-sm">Item
                                                                                    Total</label>
                                                                                <p className="float-right mb-2">₱ {item_total}</p>
                                                                                <div className="clearfix"></div>
                                                                                <hr/>
                                                                                <label
                                                                                    className="text-uppercase text-muted float-left mb-2 font-size-sm">Total</label>
                                                                                <p className="float-right mb-2">
                                                                                    <b>₱ {order_total}</b>
                                                                                </p>
                                                                                <div className="clearfix"></div>
                                                                                <label
                                                                                    className="text-uppercase text-muted float-left mb-2 font-size-sm">Paid
                                                                                    By</label>
                                                                                <p className="float-right mb-2">
                                                                                    <b>{payment}</b></p>
                                                                                <div className="clearfix"></div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <OrderItem>{order}</OrderItem>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </Fragment>

                                        )
                                    })
                                }

                                </tbody>
                            </table>
                        </div>


                    </div>
                    <div className="tab-pane fade p-3 px-4 bg-white border-bottom border-left border-right" id="process" role="tabpanel" aria-labelledby="process-tab">process</div>
                    <div className="tab-pane fade p-3 px-4 bg-white border-bottom border-left border-right" id="ready" role="tabpanel" aria-labelledby="ready-tab">ready</div>
                    <div className="tab-pane fade p-3 px-4 bg-white border-bottom border-left border-right" id="ship" role="tabpanel" aria-labelledby="ship-tab">ship</div>
                    <div className="tab-pane fade p-3 px-4 bg-white border-bottom border-left border-right" id="deliver" role="tabpanel" aria-labelledby="deliver-tab">deliver</div>
                    <div className="tab-pane fade p-3 px-4 bg-white border-bottom border-left border-right" id="cancel" role="tabpanel" aria-labelledby="cancel-tab">cancel</div>
                    <div className="tab-pane fade p-3 px-4 bg-white border-bottom border-left border-right" id="return" role="tabpanel" aria-labelledby="return-tab">return</div>
                </div>
                {extra}

                <Pager className="mb-5" onPageNavigate={this.onPageNavigate}
                       perPage={perPage}
                       currentPage={currentPage}
                       pagerCount={pageCount}>
                </Pager>
            </Fragment>

        );
    }
}

export default ShopOrder;
