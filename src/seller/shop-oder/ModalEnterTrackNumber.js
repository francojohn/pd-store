import React, {Component} from "react";
import Modal from "../../components/Modal";

class ModalEnterTrackNumber extends Component {

    render() {
        return (
            <Modal>
                <div className="modal-body bg-danger text-white">
                    <button onClick={this.props.close} type="button" className="btn close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div className="modal-body text-center">
                    <form onSubmit={this.props.onSubmit}>
                        <div className="form-group">
                            <label htmlFor="exampleFormControlInput1">Track Number</label>
                            <input type="text" className="form-control" id="modal_enter_track_number"
                                   placeholder="enter track number" required/>
                        </div>
                        <button type="submit" className="btn btn-primary text-white">SUBMIT</button>
                    </form>
                </div>
            </Modal>
        );
    }
}

export default ModalEnterTrackNumber;