import Parse from "parse";

class ShopOrderPresenter {
    constructor(view) {
        this.view = view;
        this.lastOrder = null;
    }

    componentDidMount() {
        //first time call
        this.getOrderedData(new Parse.Query("Order"));
    }

    getOrderedData(query) {
        //sort by
        query.descending("createdAt");
        this.getData(query);
    }

    //first query
    async getData(query) {
        let perPage = this.view.getPerpage();
        let currentPage = this.view.getCurrentPage();
        //create query
        let final_query = this.fetchDataTargetPage(query, perPage, currentPage);
        final_query.includeAll();
        //async
        let objects = await final_query.find();
        //set pager
        this.fetchCount(final_query);
        this.lastQuery = final_query;
        //display
        this.view.setOrders(objects);
    }

    async fetchCount(query) {
        let count = await query.count();
        this.view.setPageCount(count);
    }

    fetchDataTargetPage(query, displayPerpage, targetPage) {
        let skip = (targetPage - 1) * displayPerpage;
        query.limit(displayPerpage);//limit of data
        query.skip(skip);// skip of data
        return query;
    }

    getOrderBySeller() {
        let query = new Parse.Query("Order");
        return query.find();
    }

    //pager click
    async pageNavigate(page) {
        this.view.setCurrentPage(page);
        let perPage = this.view.getPerpage();
        //create query
        let query = this.fetchDataTargetPage(this.lastQuery, perPage, page);
        //async
        let objects = await query.find();
        //display
        this.view.setOrders(objects);
    }

    async readyToShipClick(order) {
        let status = "READY TO SHIP";
        order.set("status", status);
        //create order status
        let orderStatus = new Parse.Object("OrderStatus");
        orderStatus.set("status", status);
        orderStatus.set("message", "your order is packed and ready to ship");
        orderStatus.set("order", order);
        //
        await Parse.Object.saveAll([order, orderStatus]);
        this.view.update();
    }

    async inTransitClick(order) {
        this.lastOrder = order;
        this.view.showModalTrackNumber();
    }

    async deliveredClick(order) {
        this.lastOrder = order;
        this.view.showModalReceivedBy();
    }

    async cancelClick(order) {
        let status = "CANCELLED";
        order.set("status", status);
        //create order status
        let orderStatus = new Parse.Object("OrderStatus");
        orderStatus.set("status", status);
        orderStatus.set("message", "your order cancelled please contact us");
        orderStatus.set("order", order);

        await Parse.Object.saveAll([order, orderStatus]);
        this.view.update();
    }

    async trackNumberSubmit(track_number) {
        let order = this.lastOrder;
        let status = "IN TRANSIT";
        if (order) {
            order.set("status", status);
            order.set("track_number", track_number);
            //create order status
            let orderStatus = new Parse.Object("OrderStatus");
            orderStatus.set("status", status);
            orderStatus.set("message", "your package is forwarded to LBC, with tracking number " + track_number);
            orderStatus.set("order", order);
            //
            await Parse.Object.saveAll([order, orderStatus]);
            this.view.update();
            this.view.hideModalTrackNumber();
        }
    }

    async receivedBySubmit(received_by) {
        let order = this.lastOrder;
        let receivebyLabel = received_by ? `received by ${received_by} thank you for shipping with us.` : "thank you for shipping with us.";
        if (order) {

            let status = "DELIVERED";
            order.set("status", status);
            //create order status
            let orderStatus = new Parse.Object("OrderStatus");
            orderStatus.set("status", status);
            orderStatus.set("message", "Your package has been delivered. " + receivebyLabel);
            orderStatus.set("order", order);
            //
            await Parse.Object.saveAll([order, orderStatus]);
            this.view.update();
            this.view.hideModalReceivedBy();
        }
    }

}

export default ShopOrderPresenter