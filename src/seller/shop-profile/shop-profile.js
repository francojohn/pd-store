import React, {Component, Fragment} from "react";
import {Link} from 'react-router-dom';
import {change_status_color} from "../../utils";
import './style.css';

class myshop extends Component {
    render() {
            return (
                <div className="myshop">
                    <div className="header">
                        <div className="coverprofile">
                            <img src="/bgprofile.jpg" className="w-100 img-fluid" />
                            <div className="content">
                                <div className="row m-0">
                                    <div className="col-sm-4 pl-3">
                                        <img src="/test-seller.jpg" className="w-100" style={{border: "4px solid #fff"}} />
                                        <div className="profile-rating p-3 bg-white">
                                            <h2 className="mt-4" style={{fontSize: "12px"}}>SHOP DESCRIPTION</h2>
                                            <p className="font-size-sm ">Ex consequat commodo adipisicing exercitation aute excepteur occaecat ullamco duis aliqua id magna ullamco eu.</p>
                                            <a href="#" className="font-size-xs font-weight-bold mb-4 d-block">READ MORE &nbsp;&nbsp;
                                                <i className="fas fa-long-arrow-alt-right"></i></a>
                                            <ul id="list-example" className="list-group">
                                                <li className="p-3 list-group-item rounded-0 w-100">
                                                    <a className="float-left font-size-xs text-muted"
                                                       href="#"><i className="fas fa-user"></i>&nbsp;&nbsp;FOLLOWERS: </a>
                                                    <b className="float-right font-size-xs text-muted">597</b>
                                                    <div className="clearfix"></div>
                                                </li>
                                                <li className="p-3 list-group-item w-100">
                                                    <a className="float-left font-size-xs text-muted"
                                                       href="#"><i className="fas fa-box"></i>&nbsp;&nbsp;PRODUCTS: </a>
                                                    <b className="float-right font-size-xs text-muted">597</b>
                                                    <div className="clearfix"></div>
                                                </li>
                                                <li className="p-3 list-group-item w-100">
                                                    <a className="float-left font-size-xs text-muted"
                                                       href="#"><i className="fas fa-star"></i>&nbsp;&nbsp;RATING: </a>
                                                    <b className="float-right font-size-xs" style={{color: "#ffd700"}}>597</b>
                                                    <div className="clearfix"></div>
                                                </li>
                                                <li className="p-3 list-group-item w-100 rounded-0">
                                                    <a className="float-left font-size-xs text-muted"
                                                       href="#"><i className="fas fa-times"></i>&nbsp;&nbsp;CANCELATION RATE: </a>
                                                    <b className="float-right font-size-xs text-danger">2%</b>
                                                    <div className="clearfix"></div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div className="col-sm-8">
                                        <div className="row">
                                            <div className="col-sm-8">
                                                <h2 className="font-size-md text-white mb-0">RCREATIVE PH</h2>
                                                <label className="font-size-xs text-white">SHOP NAME</label>
                                            </div>
                                            <div className="col-sm-4 text-right">
                                                <button className="btn-outline-primary font-size-sm text-white px-4 py-1 mr-1 d-inline-block" style={{border: "1px solid #fff"}}>FOLLOW</button>
                                                <button className="btn-outline-primary font-size-sm text-white py-1 d-inline-block" style={{border: "1px solid #fff"}}>
                                                    <i className="fas fa-cog"></i></button>
                                            </div>
                                        </div>
                                        <div className="font-size-xs text-muted float-left mt-3">Description images will be displayed in your shop profile page.</div>
                                        <div className="font-size-xs text-muted float-right mt-3">0 / 5 Images</div>
                                        <div className="clearfix"></div>
                                        <div className="card mt-4">
                                            <div className="card-body bg-white p-3">
                                                <div className="row">
                                                    <div className="col-sm-6 text-center">

                                                        <div className="form-group mt-4">
                                                            <label htmlFor="file" className="input-label font-size-xs text-primary"><i
                                                                className="fas fa-images font-size-md"></i><br/>UPLOAD IMAGE</label>
                                                            <input type="file" id="file"/>
                                                        </div>
                                                    </div>
                                                    <div className="col-sm-6 border-left text-center">

                                                        <div className="form-group mt-4">
                                                            <a href="#" data-toggle="modal" data-target="#myModal">
                                                                <i className="fab fa-youtube font-size-md"></i><br/>
                                                                <span className="font-size-xs">ADD YOUTUBE VIDEO</span>
                                                            </a>
                                                        </div>
                                                        <div className="modal" id="myModal">
                                                            <div className="modal-dialog">
                                                                <div className="modal-content">
                                                                    <div className="modal-body text-left">
                                                                        <label htmlFor="exampleFormControlInput1">Embed YouTube
                                                                            Video in your Shop Profile by pasting the URL below.
                                                                            Example: </label>
                                                                        <input type="email" className="form-control"
                                                                               placeholder="Youtube video URL"/>
                                                                        <br/>
                                                                        <button
                                                                            className="btn btn-primary text-white font-size-xs mr-1">
                                                                            SAVE
                                                                        </button>
                                                                        <button className="btn text-white font-size-xs"
                                                                                data-dismiss="modal">
                                                                            Cancel
                                                                        </button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            );
        }
    }

export default myshop;