import React, {Component} from "react";
import {Route, Link, Redirect} from 'react-router-dom';
import SideBarMenu, {menu} from "../components/sidebar-menu";
//parse
import myshop from "./shop-profile/shop-profile";
import shopshipping from "./shop-shipping/shop-shipping"
import ShopOrder from "./shop-oder/ShopOrder";
import addproduct from "./add-product/addproduct";
import Myorder from "../account/Myorder/myorder";
import Construction from "../website/construction/construction";
import Myaddress from "../account/Myaddress/myaddress";
import ProfileSettings from "../account/profile-settings/ProfileSettings";
import Cancelorder from "../account/Cancelorder/cancelorder";
import ShopProducts from "./shop-products/ShopProducts";
import dashboard from "./dashboard/Dashboard";


let sectionTwo = [
    new menu('/account/profile', 'fa fa-user', 'My Account'),
    new menu('/account/profile', 'fa fa-user', 'My Account'),
];


let sectionOne = [
    new menu('/seller/dashboard', 'fa fa-tachometer-alt', 'Dashboard'),
    new menu('/seller/order', 'fas fa-shopping-cart', 'Orders'),
    new menu('/seller/products', 'fas fa-gift', 'My Products'),
    new menu(sectionTwo, 'fas fa-gift', 'My Drowdown'),
];


let menus = [
    sectionOne,
    sectionTwo,
];

class Seller extends Component {
    onRoute = title => {
        this.title = title;
    }
    render() {
        return (
            <div className="container-fluid">
                <div className="row">
                    <div className="col-sm-2">
                        <label className="font-size-xs text-muted mb-1">SHOP</label>
                        <div className="bg-white">
                            <SideBarMenu text={"text-muted"} {...this.props} menus={menus} onRoute={this.onRoute}/>
                        </div>
                    </div>
                    <div className="col-sm-10 mobileAccount">
                        <div className="">
                            <div className="card border-0" style={{"background": "#f9f9f9"}}>
                                <Route path="/seller/order" component={ShopOrder}/>
                                <Route path="/seller/products" component={ShopProducts}/>
                                <Route path="/seller/addproduct" component={addproduct}/>
                                <Route path="/seller/dashboard" component={dashboard} />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
        return (
            <div className="seller">
                <div className="">
                    <div className="row m-3">
                        <div className="col-sm-2 p-2">
                            <label className="font-size-xs text-muted mb-1">SHOP</label>
                            <div className="menu bg-white p-3">
                                <aside>
                                    <ul className="p-0 m-0">
                                        <li className="d-block p-2">
                                            <a href="#" className="font-size-xs">
                                                <i className="fas fa-store text-secondary"></i>
                                                &nbsp;&nbsp;&nbsp;ORDERS
                                            </a>
                                        </li>
                                        <li className="d-block p-2">
                                            <a href="#" className="font-size-xs text-muted">
                                                <i className="fas fa-truck text-secondary"></i>
                                                &nbsp;&nbsp;&nbsp;MY SHIPPING
                                            </a>
                                        </li>
                                        <li className="d-block p-2">
                                            <a href="#" className="font-size-xs text-muted">
                                                <i className="fas fa-map-marked-alt text-secondary"></i>
                                                &nbsp;&nbsp;&nbsp;MY ADDRESSES
                                            </a>
                                        </li>
                                        <li className="d-block p-2">
                                            <a href="#" className="font-size-xs text-muted">
                                                <i className="fas fa-tachometer-alt text-secondary"></i>
                                                &nbsp;&nbsp;&nbsp;MY PERFORMANCE
                                            </a>
                                        </li>
                                        <li className="d-block p-2">
                                            <a href="#" className="font-size-xs text-muted">
                                                <i className="far fa-credit-card text-secondary"></i>
                                                &nbsp;&nbsp;&nbsp;BANK ACCOUNTS / CARD
                                            </a>
                                        </li>
                                    </ul>
                                </aside>
                            </div>
                            <label className="font-size-xs text-muted mb-1 mt-3">SETTINGS</label>
                            <div className="menu bg-white p-3">
                                <aside>
                                    <ul className="p-0 m-0">
                                        <li className="d-block p-2">
                                            <a href="#" className="font-size-xs text-muted">
                                                <i className="fas fa-comments text-secondary"></i>
                                                &nbsp;&nbsp;&nbsp;CHAT SETTINGS
                                            </a>
                                        </li>
                                        <li className="d-block p-2">
                                            <a href="#" className="font-size-xs text-muted">
                                                <i className="fas fa-bell text-secondary"></i>
                                                &nbsp;&nbsp;&nbsp;NOTIFICATION SETTINGS
                                            </a>
                                        </li>
                                    </ul>
                                </aside>
                            </div>
                            <label className="font-size-xs text-muted mb-1 mt-3">ACCOUNT</label>
                            <div className="menu bg-white p-3">
                                <aside>
                                    <ul className="p-0 m-0">
                                        <li className="d-block p-2">
                                            <a href="#" className="font-size-xs text-muted">
                                                <i className="fas fa-store text-secondary"></i>
                                                &nbsp;&nbsp;&nbsp;MY ACCOUNT
                                            </a>
                                        </li>
                                        <li className="d-block p-2">
                                            <a href="#" className="font-size-xs text-muted">
                                                <i className="fas fa-key text-secondary"></i>
                                                &nbsp;&nbsp;&nbsp;CHANGE PASSWORD
                                            </a>
                                        </li>
                                    </ul>
                                </aside>
                            </div>
                            <br/>
                        </div>

                        <div className="col-sm-9 p-2 mobileAccount">
                            <div className="card border-0" style={{"background": "#fff"}}>
                                <Route path="/seller/myshop" component={myshop}/>
                                <Route path="/seller/shopshipping" component={shopshipping}/>
                                <Route path="/seller/order" component={ShopOrder}/>
                                <Route path="/seller/addproduct" component={addproduct}/>
                                <Route path="/seller/dashboard" component={dashboard} />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Seller;
