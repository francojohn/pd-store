import React, {Component} from "react";
import './style.css';
import DashBoardPresenter from "./DashBoardPresenter";
import Parse from "parse";
import {getBgColor, getPosition} from "../../utils";

function DateFilter(name, date, decrement) {
    this.name = name;
    this.date = date;
}

function dateFactory(increment) {
    let date = new Date();
    date.setHours(0, 0, 0, 0);//set to start date midnight
    if (increment != null) date.setDate(date.getDate() + increment);
    return date;
}

let today = new DateFilter("Today", dateFactory());
let yesterday = new DateFilter("Yesterday", dateFactory(-1));
let last7days = new DateFilter("Last 7 Days", dateFactory(-7));
let last30days = new DateFilter("Last 30 Days", dateFactory(-30));


let dateFilters = [today, yesterday, last7days, last30days];

function Target(name, query, icon) {
    this.name = name;
    this.query = query;
    this.icon = icon;
}

let users = new Target("Registered Users", new Parse.Query("_User"), "fas fa-users");
let orders = new Target("Total Book", new Parse.Query("Book"), "fas fa-book");
let pickups = new Target("Total Pickup", new Parse.Query("BookStatus").equalTo("status", "PICKED UP"), "fas fa-motorcycle");
let dispatch = new Target("Total Dispatch", new Parse.Query("BookStatus").equalTo("status", "IN TRANSIT"), "fas fa-shipping-fast");

let targets = [users, orders, pickups, dispatch];


class Card extends Component {
    constructor() {
        super();
        this.state = {count: 0};
    }

    componentWillReceiveProps(nextProps) {
        this.getData(nextProps);
    }

    componentDidMount() {
        this.getData(this.props);
    }

    async fetchCount(query, date) {
        query.greaterThanOrEqualTo("createdAt", date);
        let count = await query.count();
        this.setState({count: count});
    }

    getData(props) {
        let {target, dateFilter} = props;
        this.fetchCount(target.query, dateFilter.date);
    }

    render() {
        let {target, bgColor, dateFilter} = this.props;
        let {count} = this.state;
        let {name, icon} = target;
        return (
            <div className="col-sm-3 p-2">
                <div className={`dash ${bgColor}`}>
                    <div className="content-top text-white">
                        <div className="float-left text-center" style={{width: "50%"}}>
                            <i className={icon}></i>
                        </div>
                        <div className="float-right text-right" style={{width: "50%"}}>
                            <div className="content">
                                <h1>{count}</h1>
                                <span className="font-size-sm">{name}</span>
                            </div>
                        </div>
                    </div>
                    <div className="clearfix"></div>
                    <div className="content-bottom">
                        <a href="#">
                            <span className="float-left">VIEW MORE</span>
                            <i className="fas fa-arrow-right float-right"></i>
                            <div className="clearfix"></div>
                        </a>
                    </div>
                </div>
            </div>
        )
    }
}


class Dashboard extends Component {
    constructor() {
        super();
        this.presenter = new DashBoardPresenter(this);
        this.state = {selectedFilter: dateFilters[0]};
    }

    componentDidMount() {
        this.presenter.componentDidMount();
    }

    filterClick(e, dateFilter) {
        e.preventDefault();
        this.presenter.filterClick(dateFilter);
    }

    setSelectedFilter(selectedFilter) {
        this.setState({selectedFilter: selectedFilter})
    }

    render() {
        let {selectedFilter} = this.state;
        return (
            <div className="container-fluid">
                <div className="row">
                    <div className="col-sm-12 p-2">
                        <button type="button" className="btn btn-light font-size-sm float-right" data-toggle="dropdown"
                                aria-haspopup="true" aria-expanded="false"
                                style={{"border": "1px solid #666"}}>
                            <i className="far fa-calendar-alt mr-2"></i>{selectedFilter.name}
                        </button>

                        <div className="clearfix"></div>
                        <hr/>
                        {
                            this.renderDateFilter()
                        }
                    </div>
                    <div className="row equal mx-0 mt-2 w-100">
                        {
                            targets.map((target, index) => {
                                let position = getPosition(index, targets.length);
                                let bgColor = getBgColor(position);
                                return (
                                    <Card target={target} dateFilter={selectedFilter} bgColor={bgColor}/>
                                )
                            })
                        }

                    </div>
                </div>
            </div>
        );
    }

    renderDateFilter() {
        return (
            <div className="dropdown-menu">
                <div className="row">
                    <div className="col-sm-7">
                        <h6 className="dropdown-header font-size-xs">DATE RANGE</h6>
                        <div className="dropdown-item">
                            <a className="float-left pr-2" style={{width: "50%"}}>
                                <label className="font-size-xs text-muted">Date From</label>
                                <input type="date" className="form-control form-control-sm font-size-xs"/>
                            </a>
                            <div className="float-left" style={{width: "50%"}}>
                                <label className="font-size-xs text-muted">Date To</label>
                                <input type="date" className="form-control form-control-sm font-size-xs"/>
                            </div>
                            <div className="clearfix"></div>
                        </div>
                        {/*<div className="dropdown-item">*/}
                        {/*<input type="checkbox" className="form-check d-inline-block"/>*/}
                        {/*<span className="d-inline-block ml-2 font-size-sm">Compare to other Dates</span>*/}
                        {/*</div>*/}
                    </div>
                    <div className="col-sm-5">
                        <h6 className="dropdown-header font-size-xs">QUICK DATES</h6>
                        {
                            dateFilters.map((dateFilter) => {
                                let {name, date} = dateFilter;
                                return (
                                    <a onClick={(e) => this.filterClick(e, dateFilter)}
                                       className="dropdown-item font-size-sm text-primary"
                                       href="#">{name}: {date.toLocaleDateString()}</a>
                                )
                            })
                        }
                    </div>
                </div>
            </div>
        )
    }
}

export default Dashboard;