import Parse from "parse";

class DashBoardPresenter {
    constructor(view) {
        this.view = view;
    }

    componentDidMount() {


    }
    filterClick(dateFilter){
        this.view.setSelectedFilter(dateFilter);
    }
}

export default DashBoardPresenter