export const change_status_color = {
    "ON PROCESS": "badge-warning",
    "READY TO SHIP": "badge-primary",
    "PICKED UP": "badge-secondary",
    "IN TRANSIT": "badge-info",
    "DELIVERED": "badge-success",
    "CANCELLED": "badge-danger",
}

export function getShippingFee(state, region, city) {
    // if (state.name === "National Capital Region") {
    //     return 40;
    // }
    let mindanao = ["Northern Mindanao", "Autonomous Region in Muslim Mindanao", "Davao", "Soccsksargen"];
    let visayas = ["Central Visayas", "Eastern Visayas", "Western Visayas", "Zamboanga Peninsula"];
    let hundred_eighty = [...mindanao, ...visayas];
    if (hundred_eighty.includes(state.name)) {
        return 180;
    }

    let rizal = ["San Mateo", "Rodriguez", "Cainta", "Antipolo", "Angono", "Taytay", "Binangonan", "Teresa"];
    let cavite = ["Bacoor", "Cavite City", "Imus", "Dasmariñas", "Gen Trias", "Tanza", "Noveleta", "Rosario", "Kawit", "Tanza"];
    let laguna = ["San Pedro", "Binan", "Sta. Rosa", "Cabuyao", "Calamba"];
    let bulacan = ["Meycauayan", "Marilao", "Bocaue", "Sta. Maria"];
    let ninety = [...rizal, ...cavite, ...laguna, ...bulacan];
    // if (ninety.includes(city.name)) {
    //     return 90;
    // }

    return 160;
}

export function getPrice(product, selectedVariant) {
    let price = product.get("price");
    if (selectedVariant) {
        let selectedPrice = selectedVariant.get("price");
        price = selectedPrice ? selectedPrice : price;
    }
    return price;
}

export function getCartTotal(carts) {
    let item_total = 0;
    carts.map((cart) => {
        let product = cart.get("product");
        let quantity = cart.get("quantity");
        let product_variant = cart.get("product_variant");
        let price = getPrice(product, product_variant);
        let total = price * quantity;
        item_total += total;
    });
    return item_total;
}

export function getProductThumbnail(product) {
    let thumbnail = product.get("thumbnail");
    let image_url;
    if (thumbnail) {
        let file = thumbnail.get("file");
        if (file)
            image_url = file._url.replace("http", "https");
    }
    return image_url;
}

export function getBgColor(position) {
    let bgs = ["bg-primary", "bg-secondary", "bg-success", "bg-info", "bg-warning", "bg-danger", "bg-light", "bg-dark"];
    return bgs[position];
}

export function getPosition(position, lenght) {
    if (position < lenght) {
        return position;
    } else {
        return getPosition(position - lenght, lenght);
    }
}