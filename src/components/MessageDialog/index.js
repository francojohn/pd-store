import React, {Component} from 'react';

import Modal from "../Modal";
import "./style.css";

class MessageDialog extends Component {

    render() {
        return (
            <Modal>
                <div className="modal-body bg-danger text-white">
                    <button onClick={this.props.close} type="button" className="btn close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div className="modal-body text-center">
                    <h4 classNameName="font-size-lg">{this.props.title}</h4>
                    <p>{this.props.message}</p>
                    <button onClick={this.props.close} className="btn btn-warning text-white px-3">Okay</button>
                </div>
            </Modal>
        );
    }
}

export default MessageDialog;