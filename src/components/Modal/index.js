import React, {Component} from 'react';


class Modal extends Component {
    render() {
        let classes = ["modal-dialog modal-dialog-centered"];
        let {size} = this.props;
        classes.push(size);
        return (
            <div>
                <div className='modal d-block anim-zoom' style={{overflowY: 'auto'}}>
                    <div className={classes.join(" ")}>
                        <div className="modal-content">
                            {
                                this.props.children
                            }
                        </div>
                    </div>
                </div>
                <div className="modal-backdrop show anim-fade-in"/>
            </div>
        );
    }
}

export default Modal;
