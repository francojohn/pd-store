import React, {Component} from 'react';
import './style.css';

class BadgeNotify extends Component {

    render() {
        const {
            value,
            icon,
            badge,
            text,
        } = this.props;

        let badgeClass = ['badge badge-pill position-absolute text-white'];

        if (badge) {
            badgeClass.push(badge);
        } else {
            badgeClass.push('badge-primary');
        }

        return (
            <div className="position-relative text-nowrap">
                <i className={icon}/>{text}
                <span className={badgeClass.join(' ')}
                      style={{fontSize: '9px', left: '8px', top: '-5px'}}>{value}</span>

                {/*<div className="notify"><span className="heartbit"></span> <span className="point"></span></div>*/}
            </div>
        );
    }

}

export default BadgeNotify;
