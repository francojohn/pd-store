import React, {Component} from 'react';


class Pager extends Component {

    //listener
    navigatePage(e, page) {
        e.preventDefault();
        let {onPageNavigate} = this.props;
        if (onPageNavigate) {
            onPageNavigate(page);
        }
    }

    dividePage(pagerCount,perPage) {
        return Math.ceil(pagerCount / perPage);
    }

    render() {
        let {pagerCount} = this.props;
        let {currentPage} = this.props;
        let {perPage} = this.props;

        //divide data count to per page count
        //ex data count is 20 page page is 5 the result is 4 page
        let count = this.dividePage(pagerCount,perPage);
        if (count <= 1) {//if no other page return blank
            return <div/>
        }
        //from current page they have left and right delta 2
        var delta = 2;
        var left = currentPage - delta;
        var right = currentPage + delta + 1;
        //
        var elapsed;//old value of loop
        let pages = [];
        //page start from number 1 to page count
        for (let i = 1; i <= count; i++) {
            if (i === 1 || i === count || (i >= left && i < right)) {
                let isDot = i - elapsed;
                if (elapsed && isDot !== 1) {
                    pages.push(
                        <li key={`elapsed${i}`} className="page-item disabled mr-2 border-0">
                            <a className="text-muted font-size-sm">....</a>
                        </li>
                    )
                }
                pages.push(
                    <li key={`page${i}`}
                        className={currentPage === i ? 'page-item active text-white mr-2 border-0' : 'page-item text-muted mr-2 border-0'}
                        onClick={(e) => this.navigatePage(e, i)}>
                        <a className="page-link font-size-sm">{i}</a>
                    </li>
                )
                elapsed = i;//assign value to elapsed
            }
        }

        return (
            <div className="row text-center">
                <div className="col-sm-3">
                    <small className="font-size-xs text-muted">
                        Page {currentPage} of {count}
                    </small>
                </div>
                <div className="col-sm-6">
                    <nav className={this.props.className}>
                        <ul className="pagination pagination-sm justify-content-center">
                            {
                                currentPage > 1 &&
                                <li className="page-item">
                                    <a className="page-link text-muted font-size-sm mr-2 border-0"
                                       onClick={(e) => this.navigatePage(e, currentPage - 1)}>
                                        <i className="fa fa-chevron-left"/>
                                    </a>
                                </li>
                            }
                            {//this is the page number
                                pages
                            }
                            {
                                currentPage !== count &&
                                <li className="page-item">
                                    <a className="page-link text-muted font-size-sm border-0"
                                       onClick={(e) => this.navigatePage(e, currentPage + 1)}>
                                        <i className="fa fa-chevron-right"/>
                                    </a>
                                </li>
                            }

                        </ul>
                    </nav>
                </div>
                <div className="col-sm-3">
                    <small className="font-size-xs text-muted">
                        Page size {perPage}
                    </small>
                </div>
            </div>

        );
    }

}

export default Pager;
