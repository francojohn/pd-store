import React, {Component, Fragment} from 'react';
import "./style.css";
//router
import {NavLink} from 'react-router-dom';

//menu object
export let menu = function (route, icon, title) {
    this.route = route;
    this.icon = icon;
    this.title = title;
}

const TextIcon = ({icon, text, textSize}) => {
    let classes = ["row", textSize ? textSize : "font-size-sm"];
    return (
        <div className={classes.join(" ")}>
            <div className="col-1">
                <i className={icon}/>
            </div>
            <div className="col-9 text-truncate text-nowrap">
                {text}
            </div>
        </div>
    );
}

class SideBarMenu extends Component {

    componentDidMount() {
        //navigate to current location
        let location = this.props.location.pathname;
        this.navigate(location);
    }

    navigate(location) {
        let link = document.getElementById(location);
        if (link) link.click();
    }


    renderLink(route, icon, title) {
        if (route instanceof Array) {
            return this.withSubMenu(route, icon, title);
        }
        let navClass = ['nav-link'];
        navClass.push(this.props.text);
        return (
            <li className="nav-item" ref={this.setLink}>
                <NavLink onClick={this.props.onRoute.bind(this, title)} to={route} className={navClass.join(" ")}
                         id={route}>
                    <TextIcon textSize={this.props.textSize} icon={icon} text={title}/>
                </NavLink>
            </li>
        );
    }

    withSubMenu(menus, icon, title) {
        let id = title.replace(/\s+/, "");
        let href = "#" + id;
        let navClass = ["nav-link collapsed accordion-toggle"];
        navClass.push(this.props.text);
        return (
            <Fragment>
                <li className="nav-item" style={{height: '30px'}}>
                    <a className={navClass.join(" ")} href={href}
                       data-toggle="collapse">
                        <TextIcon icon={icon} text={title}/>
                    </a>
                </li>
                <div className="collapse text-left" id={id}>
                    {
                        this.renderMenu(menus)
                    }
                </div>
            </Fragment>
        );
    }

    renderMenu(section) {
        return (
            <ul className="list-unstyled custom-sidebar navbar-nav pl-3 pr-2">
                {
                    section.map((menu) => {
                        return this.renderLink(menu.route, menu.icon, menu.title)
                    })
                }
            </ul>
        );
    }

    render() {
        let divider = 0;
        let {menus} = this.props;
        let menu = menus[0];
        if (menu instanceof Array) {
            return (
                menus.map((section) => {

                    divider++;
                    if (divider < menus.length && section.length > 0) {
                        return (
                            <Fragment>
                                {
                                    this.renderMenu(section)
                                }
                                <div className="dropdown-divider"/>
                            </Fragment>
                        )
                    }
                    return this.renderMenu(section);
                })
            );
        }
        return this.renderMenu(menus);
    }
}

export default SideBarMenu;
