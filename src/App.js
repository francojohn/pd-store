import React, {Component} from 'react';
import Parse from 'parse';
//router
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';

//components

import Home from './website/Home/Home';
import Header from "./website/header/";
import Footer from "./website/footer/Footer";
import Product from "./website/product/Product";
import Cart from "./website/cart/cart";
import Payment from "./website/payment/payment";
import Order from "./website/order/order";
import SignIn from "./website/signin/SignIn";
import Mobilemenu from "./website/Mobilemenu/mobilemenu";
import Signup from "./website/signup/signup";
import Construction from "./website/construction/construction"
import Account from "./account/Account";
import Seller from "./seller/seller";
import myproduct from "./website/myproduct/myproduct"
import AppProvider from "./AppProvider";
import CheckOut from "./website/checkout/CheckOut";
import faq from "./website/faq/faq";
import privacy from "./website/privacy/privacy"
import Chat from "./website/Chat";

const PARSE_APP_ID = 'e3af844bec24c1ad975700177b47993c';
Parse._initialize(PARSE_APP_ID);
Parse.serverURL = 'https://api1.programmersdevelopers.com/v1';
// Parse.serverURL = 'http://localhost/v1';


class App extends Component {

    generate(keys, variantList, startIndex = 0, generatedList = [], combinedVariants = {}) {
        const key = keys[startIndex], variants = variantList[key];
        for (combinedVariants[key] of variants) {
            (startIndex + 1 === keys.length) ? generatedList.push({...combinedVariants})
                : this.generate(keys, variantList, startIndex + 1, generatedList, {...combinedVariants});
        }
        return generatedList;
    };

    componentDidMount() {
        // let variant = {"Color": ["Black", "White"]};//test
        //
        // // let variant = {"Color": ["Black"], "Size": ["Small", "Medium"]};//test
        // // let variant = {"Color": ["Black", "White"], "Size": ["Small", "Medium"]};//test
        // // let variant = {"Color": ["Black", "White"], "Size": ["Small", "Medium", "Large"], "Gender": ["Male", "Female"]};//test
        //
        // let generatedList = this.generate(Object.keys(variant), variant);
        //
        // console.log(JSON.stringify(generatedList));

        this.facebookSDKInit();
    }

    facebookSDKInit() {
        window.fbAsyncInit = function () {
            Parse.FacebookUtils.init({// this line replaces FB.init({
                appId: '2044447989206002', // Facebook App ID
                status: true, // check Facebook Login status
                cookie: true, // enable cookies to allow Parse to access the session
                xfbml: true, // initialize Facebook social plugins on the page
                version: 'v3.0' // point to the latest Facebook Graph API version
            });
        };
        (function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) {
                return;
            }
            js = d.createElement(s);
            js.id = id;
            // js.src = "https://connect.facebook.net/en_US/sdk.js";
            js.src = "//connect.facebook.net/en_US/all.js";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    }

    render() {
        return (
            <Router>
                <Switch>
                    <AppProvider>
                        <Mobilemenu/>
                        <Header/>
                        <Route exact path="/" component={Home}/>
                        <Route path="/construction" component={Construction}/>
                        <Route path="/myproduct" component={myproduct}/>
                        <Route path="/product/:id" component={Product}/>
                        <Route path="/signin" component={SignIn}/>
                        <Route path="/account" component={Account}/>
                        <Route path="/seller" component={Seller}/>
                        <Route path="/cart" component={Cart}/>
                        <Route path="/checkout" component={CheckOut}/>
                        <Route path="/payment" component={Payment}/>
                        <Route path="/order/:id" component={Order}/>
                        <Route path="/faq" component={faq}/>
                        <Route path="/privacy" component={privacy}/>
                        <Footer/>
                        <Chat/>
                    </AppProvider>
                </Switch>
            </Router>
        );
    }
}

export default App;
