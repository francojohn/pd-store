import React,{Component, createContext} from 'react';

export const {Provider,Consumer} = createContext();


export const WithContext = (WrappedComponent) =>{
    return class ProductLoader extends Component{
        render(){
            return(
              <Consumer>
                  {(value) => (
                      <WrappedComponent {...value} {...this.props}/>
                  )}
              </Consumer>
            );
        }
    }
}