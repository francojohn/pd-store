import React, {Component} from 'react';
import {Provider} from './Context';
import Parse from "parse";


class AppProvider extends Component {
    state = {
        carts: [],
        order: null,
        address: null
    }

    componentDidMount() {
        this.fetchData();
        this.fetchAddress();
    }

    async fetchData() {
        let query = new Parse.Query('Cart');
        let user = Parse.User.current();
        query.equalTo("user", user);
        query.include("product");
        query.include("product_variant");
        query.include("product.thumbnail");
        try {
            let carts = await query.find();
            this.setCarts(carts);
        } catch (err) {

        }
    }

    async fetchAddress() {
        try {
            let user = Parse.User.current();
            let query = new Parse.Query("Address");
            query.equalTo("user", user);
            query.equalTo("default", true);
            let address = await query.first();
            this.setAddress(address);
        }catch (e) {
            console.log(e);
        }
    }
    /****************************************** State Management *************************************/
    setCarts = (carts) => {
        this.setState({carts: carts});
    }
    setAddress = (address) => {
        this.setState({address: address});
    }
    setOrder = (order) => {
        this.setState({order: order});
    }

    getValue = () => ({
        ...this.state,
        setCarts: this.setCarts,
        setOrder: this.setOrder,
        setAddress: this.setAddress,
    });

    render() {
        return (
            <Provider value={this.getValue()}>
                {this.props.children}
            </Provider>
        );
    }

}

export default AppProvider;
