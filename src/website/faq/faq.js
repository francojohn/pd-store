import React, {Component} from "react";
import {Link} from 'react-router-dom';
import './style.css';
import Parse from 'parse';

class faq extends Component {
    render() {
        return (
            <div className="agreement pb-5 bg-white" name="myAnchor" id="myAnchor">
                <div className="banner_content py-5">
                    <div className="container">
                        <div className="row">
                            <div className="col-sm-8 offset-sm-2 py-5">
                                <h2 className="text-center font-size-lg text-white mb-3"><b>Hi.</b> How can we help?
                                </h2>
                                <form className="content">
                                    <input type="text" className="form-control font-size-sm p-4"
                                           placeholder="Ask questions or input words" />
                                        <div className="btn-search border-0">
                                            <button className="text-secondary">
                                                <i className="fas fa-search"></i>
                                            </button>
                                        </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="container">
                    <div className="row">
                        <div className="col-sm-12">
                            <div className="help_content py-5">
                                <h4 className="font-size-sm border-bottom pb-4 text-secondary">Programmers Developers Store
                                    Help Center</h4>
                                <div className="row">
                                    <div className="col-sm-3">
                                        <h4 className="font-size-sm mt-3">Categories</h4>
                                        <div id="accordion">
                                            <div className="card border-0">
                                                <div className="card-header border-0 bg-transparent px-0 py-2"
                                                     id="headingOne">
                                                    <h5 className="mb-0">
                                                        <a data-toggle="collapse" data-target="#collapseOne"
                                                           aria-expanded="true" aria-controls="collapseOne">
                                                            <div className="btn btn-link py-0 font-size-sm float-left">
                                                                Order and Payments
                                                            </div>
                                                            <i className="fas fa-chevron-down font-size-xs float-right pr-3"
                                                               style={{"padding-top": "5px"}}></i>
                                                            <div className="clearfix"></div>
                                                        </a>
                                                    </h5>
                                                </div>

                                                <div id="collapseOne" className="collapse show" aria-labelledby="headingOne"
                                                     data-parent="#accordion">
                                                    <div className="card-body py-1 px-3">
                                                        <ul className="list-item">
                                                            <li className="item font-size-sm">
                                                                <a href="#" className="text-muted">
                                                                    PD Coins
                                                                </a>
                                                            </li>
                                                            <li className="item font-size-sm">
                                                                <a href="#" className="text-muted">
                                                                    Browse and Orders
                                                                </a>
                                                            </li>
                                                            <li className="item font-size-sm">
                                                                <a href="#" className="text-muted">
                                                                    Payments
                                                                </a>
                                                            </li>
                                                            <li className="item font-size-sm">
                                                                <a href="#" className="text-muted">
                                                                    Credit and Debit Card
                                                                </a>
                                                            </li>
                                                            <li className="item font-size-sm">
                                                                <a href="#" className="text-muted">
                                                                    PD Wallet
                                                                </a>
                                                            </li>
                                                            <li className="item font-size-sm">
                                                                <a href="#" className="text-muted">
                                                                    Voucher Code
                                                                </a>
                                                            </li>
                                                            <li className="item font-size-sm">
                                                                <a href="#" className="text-muted">
                                                                    Digital Products
                                                                </a>
                                                            </li>
                                                            <li className="item font-size-sm">
                                                                <a href="#" className="text-muted">
                                                                    Buyer Tips
                                                                </a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="card border-0">
                                                <div className="card-header border-0 bg-transparent px-0 py-2"
                                                     id="headingTwo">
                                                    <h5 className="mb-0">
                                                        <a className="collapsed" data-toggle="collapse" data-target="#collapseTwo"
                                                           aria-expanded="false" aria-controls="collapseTwo">
                                                        <div className="btn btn-link py-0 font-size-sm float-left collapsed">
                                                            Order and Payments
                                                        </div>
                                                        <i className="fas fa-chevron-down font-size-xs float-right pr-3"
                                                           style={{"padding-top:" :"5px"}}></i>
                                                        <div className="clearfix"></div>
                                                        </a>
                                                    </h5>
                                                </div>
                                                <div id="collapseTwo" className="collapse" aria-labelledby="headingTwo"
                                                     data-parent="#accordion">
                                                    <div className="card-body py-1 px-3">
                                                        <ul className="list-item">
                                                            <li className="item font-size-sm">
                                                                <a href="#" className="text-muted">
                                                                    PD Coins
                                                                </a>
                                                            </li>
                                                            <li className="item font-size-sm">
                                                                <a href="#" className="text-muted">
                                                                    Browse and Orders
                                                                </a>
                                                            </li>
                                                            <li className="item font-size-sm">
                                                                <a href="#" className="text-muted">
                                                                    Payments
                                                                </a>
                                                            </li>
                                                            <li className="item font-size-sm">
                                                                <a href="#" className="text-muted">
                                                                    Credit and Debit Card
                                                                </a>
                                                            </li>
                                                            <li className="item font-size-sm">
                                                                <a href="#" className="text-muted">
                                                                    PD Wallet
                                                                </a>
                                                            </li>
                                                            <li className="item font-size-sm">
                                                                <a href="#" className="text-muted">
                                                                    Voucher Code
                                                                </a>
                                                            </li>
                                                            <li className="item font-size-sm">
                                                                <a href="#" className="text-muted">
                                                                    Digital Products
                                                                </a>
                                                            </li>
                                                            <li className="item font-size-sm">
                                                                <a href="#" className="text-muted">
                                                                    Buyer Tips
                                                                </a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="card border-0">
                                                <div className="card-header border-0 bg-transparent px-0 py-2"
                                                     id="headingThree">
                                                    <h5 className="mb-0">
                                                        <a className="collapsed" data-toggle="collapse" data-target="#collapseThree"
                                                           aria-expanded="false" aria-controls="collapseThree">
                                                            <div className="btn btn-link py-0 font-size-sm float-left">
                                                                Order and Payments
                                                            </div>
                                                            <i className="fas fa-chevron-down font-size-xs float-right pr-3"
                                                               style={{"padding-top": "5px"}}></i>
                                                            <div className="clearfix"></div>
                                                        </a>
                                                    </h5>
                                                </div>
                                                <div id="collapseThree" className="collapse" aria-labelledby="headingThree"
                                                     data-parent="#accordion">
                                                    <div className="card-body py-1 px-3">
                                                        <ul className="list-item">
                                                            <li className="item font-size-sm">
                                                                <a href="#" className="text-muted">
                                                                    PD Coins
                                                                </a>
                                                            </li>
                                                            <li className="item font-size-sm">
                                                                <a href="#" className="text-muted">
                                                                    Browse and Orders
                                                                </a>
                                                            </li>
                                                            <li className="item font-size-sm">
                                                                <a href="#" className="text-muted">
                                                                    Payments
                                                                </a>
                                                            </li>
                                                            <li className="item font-size-sm">
                                                                <a href="#" className="text-muted">
                                                                    Credit and Debit Card
                                                                </a>
                                                            </li>
                                                            <li className="item font-size-sm">
                                                                <a href="#" className="text-muted">
                                                                    PD Wallet
                                                                </a>
                                                            </li>
                                                            <li className="item font-size-sm">
                                                                <a href="#" className="text-muted">
                                                                    Voucher Code
                                                                </a>
                                                            </li>
                                                            <li className="item font-size-sm">
                                                                <a href="#" className="text-muted">
                                                                    Digital Products
                                                                </a>
                                                            </li>
                                                            <li className="item font-size-sm">
                                                                <a href="#" className="text-muted">
                                                                    Buyer Tips
                                                                </a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-sm-9">
                                        <div className="main_content py-5">
                                            <h5 className="text-secondary font-size-md font-weight-bold">Hot Questions</h5>

                                            <div id="accordion">
                                                <div className="card border-0">
                                                    <div className="card-header border-0 bg-transparent px-0 py-2"
                                                         id="contentOne">
                                                        <h5 className="mb-0">
                                                            <a href="#" className="text-muted" data-toggle="collapse"
                                                               data-target="#collapseItemOne" aria-expanded="true"
                                                               aria-controls="collapseItemOne">
                                                                <div className="float-left">
                                                                    <i className="far fa-bookmark"></i>&nbsp;&nbsp;
                                                                    What is PD Wallet?
                                                                </div>
                                                                <div className="float-right">
                                                                    <i className="fas fa-long-arrow-alt-right text-muted"></i>
                                                                </div>
                                                                <div className="clearfix"></div>
                                                            </a>
                                                        </h5>
                                                    </div>

                                                    <div id="collapseItemOne" className="collapse show"
                                                         aria-labelledby="contentOne" data-parent="#accordion">
                                                        <div className="card-body py-1 px-3">
                                                            <p>
                                                                Lorem ipsum dolor sit amet, consectetuer adipiscing elit.
                                                                Aenean commodo ligula eget dolor. Aenean massa. Cum sociis
                                                                natoque penatibus et magnis dis parturient montes, nascetur
                                                                ridiculus mus. Donec quam felis, ultricies nec, pellentesque
                                                                eu, pretium quis, sem. Nulla consequat massa quis enim.
                                                                Donec pede justo, fringilla vel, aliquet nec, vulputate
                                                                eget, arcu. In enim justo, rhoncus ut, imperdiet a,
                                                                venenatis vitae, justo.
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="card border-0">
                                                    <div className="card-header border-0 bg-transparent px-0 py-2"
                                                         id="contentTwo">
                                                        <h5 className="mb-0">
                                                            <a href="#" className="text-muted collapsed"
                                                               data-toggle="collapse" data-target="#collapseItemTwo"
                                                               aria-expanded="false" aria-controls="collapseItemTwo">
                                                                <div className="float-left">
                                                                    <i className="far fa-bookmark"></i>&nbsp;&nbsp;
                                                                    What is PD Wallet?
                                                                </div>
                                                                <div className="float-right">
                                                                    <i className="fas fa-long-arrow-alt-right text-muted"></i>
                                                                </div>
                                                                <div className="clearfix"></div>
                                                            </a>
                                                        </h5>
                                                    </div>
                                                    <div id="collapseItemTwo" className="collapse"
                                                         aria-labelledby="contentTwo" data-parent="#accordion">
                                                        <div className="card-body py-1 px-3">
                                                            <p>
                                                                Lorem ipsum dolor sit amet, consectetuer adipiscing elit.
                                                                Aenean commodo ligula eget dolor. Aenean massa. Cum sociis
                                                                natoque penatibus et magnis dis parturient montes, nascetur
                                                                ridiculus mus. Donec quam felis, ultricies nec, pellentesque
                                                                eu, pretium quis, sem. Nulla consequat massa quis enim.
                                                                Donec pede justo, fringilla vel, aliquet nec, vulputate
                                                                eget, arcu. In enim justo, rhoncus ut, imperdiet a,
                                                                venenatis vitae, justo.
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="card border-0">
                                                    <div className="card-header border-0 bg-transparent px-0 py-2"
                                                         id="contentThree">
                                                        <h5 className="mb-0">
                                                            <a href="#" className="text-muted collapsed"
                                                               data-toggle="collapse" data-target="#collapseItemThree"
                                                               aria-expanded="false" aria-controls="collapseItemThree">
                                                                <div className="float-left">
                                                                    <i className="far fa-bookmark"></i>&nbsp;&nbsp;
                                                                    What is PD Wallet?
                                                                </div>
                                                                <div className="float-right">
                                                                    <i className="fas fa-long-arrow-alt-right text-muted"></i>
                                                                </div>
                                                                <div className="clearfix"></div>
                                                            </a>
                                                        </h5>
                                                    </div>
                                                    <div id="collapseItemThree" className="collapse"
                                                         aria-labelledby="contentThree" data-parent="#accordion">
                                                        <div className="card-body py-1 px-3">
                                                            <p>
                                                                Lorem ipsum dolor sit amet, consectetuer adipiscing elit.
                                                                Aenean commodo ligula eget dolor. Aenean massa. Cum sociis
                                                                natoque penatibus et magnis dis parturient montes, nascetur
                                                                ridiculus mus. Donec quam felis, ultricies nec, pellentesque
                                                                eu, pretium quis, sem. Nulla consequat massa quis enim.
                                                                Donec pede justo, fringilla vel, aliquet nec, vulputate
                                                                eget, arcu. In enim justo, rhoncus ut, imperdiet a,
                                                                venenatis vitae, justo.
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="card border-0">
                                                    <div className="card-header border-0 bg-transparent px-0 py-2"
                                                         id="contentFour">
                                                        <h5 className="mb-0">
                                                            <a href="#" className="text-muted collapsed"
                                                               data-toggle="collapse" data-target="#collapseItemFour"
                                                               aria-expanded="false" aria-controls="collapseItemFour">
                                                                <div className="float-left">
                                                                    <i className="far fa-bookmark"></i>&nbsp;&nbsp;
                                                                    What is PD Wallet?
                                                                </div>
                                                                <div className="float-right">
                                                                    <i className="fas fa-long-arrow-alt-right text-muted"></i>
                                                                </div>
                                                                <div className="clearfix"></div>
                                                            </a>
                                                        </h5>
                                                    </div>
                                                    <div id="collapseItemFour" className="collapse"
                                                         aria-labelledby="contentFour" data-parent="#accordion">
                                                        <div className="card-body py-1 px-3">
                                                            <p>
                                                                Lorem ipsum dolor sit amet, consectetuer adipiscing elit.
                                                                Aenean commodo ligula eget dolor. Aenean massa. Cum sociis
                                                                natoque penatibus et magnis dis parturient montes, nascetur
                                                                ridiculus mus. Donec quam felis, ultricies nec, pellentesque
                                                                eu, pretium quis, sem. Nulla consequat massa quis enim.
                                                                Donec pede justo, fringilla vel, aliquet nec, vulputate
                                                                eget, arcu. In enim justo, rhoncus ut, imperdiet a,
                                                                venenatis vitae, justo.
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="card border-0">
                                                    <div className="card-header border-0 bg-transparent px-0 py-2"
                                                         id="contentFive">
                                                        <h5 className="mb-0">
                                                            <a href="#" className="text-muted collapsed"
                                                               data-toggle="collapse" data-target="#collapseItemFive"
                                                               aria-expanded="false" aria-controls="collapseItemFive">
                                                                <div className="float-left">
                                                                    <i className="far fa-bookmark"></i>&nbsp;&nbsp;
                                                                    What is PD Wallet?
                                                                </div>
                                                                <div className="float-right">
                                                                    <i className="fas fa-long-arrow-alt-right text-muted"></i>
                                                                </div>
                                                                <div className="clearfix"></div>
                                                            </a>
                                                        </h5>
                                                    </div>
                                                    <div id="collapseItemFive" className="collapse"
                                                         aria-labelledby="contentFive" data-parent="#accordion">
                                                        <div className="card-body py-1 px-3">
                                                            <p>
                                                                Lorem ipsum dolor sit amet, consectetuer adipiscing elit.
                                                                Aenean commodo ligula eget dolor. Aenean massa. Cum sociis
                                                                natoque penatibus et magnis dis parturient montes, nascetur
                                                                ridiculus mus. Donec quam felis, ultricies nec, pellentesque
                                                                eu, pretium quis, sem. Nulla consequat massa quis enim.
                                                                Donec pede justo, fringilla vel, aliquet nec, vulputate
                                                                eget, arcu. In enim justo, rhoncus ut, imperdiet a,
                                                                venenatis vitae, justo.
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="card border-0">
                                                    <div className="card-header border-0 bg-transparent px-0 py-2"
                                                         id="contentSix">
                                                        <h5 className="mb-0">
                                                            <a href="#" className="text-muted collapsed"
                                                               data-toggle="collapse" data-target="#collapseItemSix"
                                                               aria-expanded="false" aria-controls="collapseItemSix">
                                                                <div className="float-left">
                                                                    <i className="far fa-bookmark"></i>&nbsp;&nbsp;
                                                                    What is PD Wallet?
                                                                </div>
                                                                <div className="float-right">
                                                                    <i className="fas fa-long-arrow-alt-right text-muted"></i>
                                                                </div>
                                                                <div className="clearfix"></div>
                                                            </a>
                                                        </h5>
                                                    </div>
                                                    <div id="collapseItemSix" className="collapse"
                                                         aria-labelledby="contentSix" data-parent="#accordion">
                                                        <div className="card-body py-1 px-3">
                                                            <p>
                                                                Lorem ipsum dolor sit amet, consectetuer adipiscing elit.
                                                                Aenean commodo ligula eget dolor. Aenean massa. Cum sociis
                                                                natoque penatibus et magnis dis parturient montes, nascetur
                                                                ridiculus mus. Donec quam felis, ultricies nec, pellentesque
                                                                eu, pretium quis, sem. Nulla consequat massa quis enim.
                                                                Donec pede justo, fringilla vel, aliquet nec, vulputate
                                                                eget, arcu. In enim justo, rhoncus ut, imperdiet a,
                                                                venenatis vitae, justo.
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default faq;