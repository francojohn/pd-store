const initialState = {data: [], isLoading: false, error: null};
export default function reducer(state = initialState, action) {
    switch (action.type) {
        case 'CATEGORY_FETCH_PENDING':
            state = {...state, isLoading: true};
            break;
        case 'CATEGORY_FETCH_FULFILLED':
            state = {...state, isLoading: false, data: action.payload};
            break;
        case 'CATEGORY_FETCH_REJECTED':
            state = {...state, isLoading: false, error: action.payload};
            break;
        default:
    }
    return state;
}
