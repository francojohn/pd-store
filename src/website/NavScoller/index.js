import React, {Component} from 'react';
import './style.css';

//router
import {Link} from 'react-router-dom';

//menu object
export let menu = function (route, title) {
    this.route = route;
    this.title = title;
}

class NavScoller extends Component {

    render() {
        let {menus} = this.props;
        if(!menus){
            menus = [];
        }
        return (
            <div className="bg-primary">
                <div className="nav-scroller">
                    <nav className="navbar navbar-expand-md container text-white p-0">
                        {
                            menus.map((menu) => {
                                let {title,route} = menu;
                                return (
                                    <Link key={title} to={route}
                                          className="nav-item nav-link font-size-xs font-weight-light text-white">{title}</Link>
                                )
                            })
                        }
                    </nav>
                </div>
            </div>
        );
    }
}


export default NavScoller;