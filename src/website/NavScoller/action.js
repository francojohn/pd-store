import Parse from 'parse';
import store from "../../redux/store";

export function fetch() {
    var query = new Parse.Query('Category');
    store.dispatch({type: 'CATEGORY_FETCH', payload: query.find()});
}