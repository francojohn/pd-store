import React, {Component} from "react";
import {Link} from 'react-router-dom';
import './style.css';
import {WithContext} from "../../Context";
import OrderSummary from "../cart/OrderSummary";
import PaymentPresenter from "./PaymentPresenter";

class Payment extends Component {
    constructor() {
        super();
        this.presenter = new PaymentPresenter(this);
    }

    componentDidMount() {
        this.presenter.componentDidMount();
    }

    confirmClick = () => {
        this.presenter.confirmClick();
    }

    getOrder() {
        let {order} = this.props;
        return order;
    }

    gotoCart() {
        this.props.history.push('/cart');
    }
    getPayment(){
        return "COD";
    }
    getCarts() {
        let {carts} = this.props;
        return carts;
    }
    setCarts(carts) {
        let {setCarts} = this.props;
        if(setCarts){
            setCarts(carts);
        }
    }
    setOrder(order){
        let {setOrder} = this.props;
        if(setOrder){
            setOrder(order);
        }
    }
    gotoOrderSuccess(orderId){
        this.props.history.push(`/order/${orderId}`);
    }
    showProgress() {
        let button = document.getElementById("payment_btn_confirm");
        button.disabled = true;
        button.classList.add('bg-progress');
        button.innerHTML = "Please Wait..";
    }

    hideProgress() {
        let button = document.getElementById("payment_btn_confirm");
        button.disabled = false;
        button.classList.remove('bg-progress');
        button.innerHTML = "CONFIRM";
    }
    render() {
        return (
            <div className="payment">
                <div className="container">
                    <div className="card rounded-0 py-5 px-2 my-5">
                        <div className="card-body">
                            <div className="mt-3">
                                <div className="container mt-3">
                                    <div className="row">
                                        <div className="col-sm-9 p-2">
                                            <Link to="/"
                                                  className="text-secondary d-block mb-5 font-weight-bold font-size-xs">
                                                <i className="fa fa-angle-left"></i>&nbsp;&nbsp;CONTINUE SHOPPING
                                            </Link>
                                            <div className="row">
                                                <div className="col-sm-12">
                                                    <div className="py-4">
                                                        <h5>Select Payment Method</h5>
                                                        <ul className="nav pay-nav nav-pills mt-5 mb-3" id="pills-tab"
                                                            role="tablist">
                                                            <li className="nav-item text-center">
                                                                <a className="nav-link p-4 active" id="pills-cod-tab"
                                                                   data-toggle="pill" href="#pills-cod" role="tab"
                                                                   aria-controls="pills-cod"
                                                                   aria-selected="true"><i
                                                                    className="fa fa-truck mb-2"></i><br/>Cash on
                                                                    Delivery</a>
                                                            </li>
                                                            <li className="nav-item text-center">
                                                                <a className="nav-link p-4" id="pills-gcash-tab"
                                                                   data-toggle="pill" href="#pills-gcash" role="tab"
                                                                   aria-controls="pills-gcash"
                                                                   aria-selected="false"><img className="mb-2"
                                                                                              src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAolBMVEUREkT///8AADkAADMAAD4AADycnKsAADQPEEMAAD8AADBdXXgAADYAADgLDUIAAC/R0dchIU3i4ufIyNAEBkAAACwAACh2dovq6u69vcfw8POiorDLy9N/gJC1tcApKlNSU2+Tk6NkZH0XGEmLi505OV3W1tt5eYxERGWrq7hfYHk6O15MTWyPj59sbYIKDEcAABIvMFgkJU8AABgAACIAAAoeyLMCAAAG0UlEQVR4nO2a2ZqqOBSFkQQDiEERVJzHciiL01Z3v/+rNUMSIop92uPhor/1X5USSRbZUzZlGAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA4CVCK4c3Pa8dOpQ6of1sDA9pyi8uLVzNc65NSrQpi5zjeXPu7Lu+9/jpcotFPy7pmFXfJ2b4+mTmtJVzpq/f4z/CGU3icUsQLEac0OpWcuYc4okc01vvXevpbj/BbBc36TSl0Pa2p1aFWRKZugCbfS8qQ4I5MV/T2LTCkLWr+jImO22PQjZ9OIS8ZKsNK6T94JHAbJPYv44Zf74SLJpVaB1r9LVaB0uMofu6IZOX7LRRhfrie+vOfnk9zhdFPBn7YvXht6ZpsB5tRmsZcjbW89s/pkmFnKrouOj7Jg05D9OcsIpbZSy3mYqy41GaJixqmWzYGWRyo5dmbVKhO5DmdnQ1j3JIf9HzxQeiAu1USxDU/fxofb+WshtUSM9i7YFTmYx7nrTRTykw8W+cjpJj+SNOrS4jhHjmXU1kO2ZxhXJxSVdo0+x3NTXGr+OLEDkJ66O+2uYdqS5d/ijsuvyym8az2WyxPrKbSsDueqt1fqWdLBnLC4lSoe3RZDrozU6HLfkdGp2VWHynPmBwGWZmdT7Ho+NUTybjuV8+L8drT8pLk0Xi2JrCrlmm2Xj7UtR6DotFfPTrx5hrsYJrzTbzvSZB2PxSmq/1Wb246JYKV7dXN967BdqemODi1A8iH+IpuHUjvKrAlGXxOOjn3ZXM96TCUeXaxnyzQhlDxsP6MTYVsye1cc+rFqzZLVnui2R8d8XXrPSO/pt90ZoX9108sY5QVgT1B4nCm4N4fdgkbemQ62w7LLlJs90m+Tpl1hBnleCNwklQmmpcDWa/SFe4+bx0cc/VSZVTscigPrfb7rh3+BGxtBSg3eFOrDtTwkQYPgxNSk3PNZNebgqawsV16A+vsfxovHcTZaBZKTf0FoFO7BmWWHHvydO1OVHpzI7EYzumnugWOxpIL0iP2bmtlwo3edgNIzFLWQq/SeGgXEwB6d24RSpLhtIBe3Ij3YC5XRqGPxYKKxauFKoUOxQTn94bTt+mMMemlFrU4X7hVpkjytvNo9uSRSpUtb1Bk5+d5r8pvLPSe4U/Y6VZYeZFziUZjZJO/+9CYdssHb31Md9GXtm/kgqnasf4VSh8b6i5jzQy+ZUKVaR5UhQYobtaVPNCppD3y8/jRcIJvVU4UhlIjnyzQpktSuN3zqOcZCwVqrq7W3/U7e5vH4xSaHhfN98NLj7XFXZ+t0KV8ctM4BSt2mEgFf5Mxk/u9QmFhru7/TbOHbIxhapqO1ZX7yqFynDjuqqNyvI9TfqnUyw9uVBoeHuV64oxzG5QYVl5V1evKVSV9/JxMlYdgMEyYp7H3D/KSJMREmc+0ArsBWtSoTo9rSqJVlPIl2JMTceCborLp6HIOf6twqxVTsyjKudaW96gQiOSJ2Dn9mikKSxPwIdqrrKzXfVEj4OJSGSzqsLsy9AcdsRt0gDaoEIqpw2sG1fUFZZdjA25iach2zsqhX5IQ5d73q6ehLyNutCgQsOVcWD8qXcRfE2h1olaD9WSbOouZ5nFiUikKnPZQM8PF4b2lkqKmHYbVWh3VRA4fbumw1PTo6brqHyYjSlPecGGMDOrzjz3kj2bga+i1bFwZVOettrdTGCydMVLKpuI7PvV7B4a9NpSzHarH6y7Pa9lxBelWqiNmcS75JzMZQnTobKsm+x90/SGsnmXltu+Z7up+a6PkU8IGcqzYloFN6rQoMu7NotCFqPWk64+38q/B+svvSM1CU5ExOqgN+jJWYLUmZtVaNDtg5qr4EPOR5f37YhCxif36loSrY+/HrywynpCDSs0ePR1v5D8cZfn0dC9e8OYsSZheuYd3H65kkYe/Hn/XJKsBm5aYRpuwumdqU5OF6LVATarlF9p+P2ieTluE119b+l9i5tNhu2KxPElL/KlwvItt+zKzn6PwqyHzzaL0ofGg6+j71Xec4fE3s3Ug/hoX9RLYpschfpJvPJDw9mKXfXNaL8bKJW9kVtIsg69WUrvouoM3p9lX/Wm7+4nalCPOPvO6JB0rpQ9/j+EtPzytpckHbK0SFcvEULW3SeHzdVk+WGaM34+JMesrRSajIX78+iw2VtE9esskqNNwln+zW8UmGGHRR/CfvLWk4shD35r0fJrnn4KRba3i/s+/y8WAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAID/Cf8AH7RvJ15fQIAAAAAASUVORK5CYII="
                                                                                              width="30" height="auto"/><br/>Gcash</a>
                                                            </li>
                                                        </ul>
                                                        <div className="tab-content" id="pills-cod-tab">
                                                            <div className="tab-pane fade show active" id="pills-cod"
                                                                 role="tabpanel" aria-labelledby="pills-cod-tab">
                                                                <div className="content">
                                                                    <p className="font-size-sm">
                                                                        Pay using our Cash on Delivery.
                                                                    </p>
                                                                    <p className="font-size-sm">
                                                                        our COD is currently supported arround metro
                                                                        manila area. message me on facebook if province
                                                                        area thank you.
                                                                    </p>
                                                                    <button onClick={this.confirmClick} id="payment_btn_confirm"
                                                                            className="btn btn-primary rounded text-white px-5 my-3">
                                                                        CONFIRM ORDER
                                                                    </button>
                                                                </div>
                                                            </div>
                                                            <div className="tab-pane fade" id="pills-gcash"
                                                                 role="tabpanel" aria-labelledby="pills-gcash-tab">
                                                                <div className="">Soon Payment Method</div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-sm-3 p-2">
                                            <OrderSummary/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        );
    }
}

export default WithContext(Payment);