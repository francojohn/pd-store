import Parse from "parse";

class PaymentPresenter {
    constructor(view) {
        this.view = view;
    }

    componentDidMount() {
        let order = this.view.getOrder();
        if (!order) {
            this.view.gotoCart();
        }

    }

    async confirmClick() {
        try {
            this.view.showProgress();
            let user = Parse.User.current();
            let order = this.view.getOrder();
            let carts = this.view.getCarts();
            let payment = this.view.getPayment();
            order.set("status", "ON PROCESS");
            order.set("payment", payment);

            //create my items
            let items = [];
            carts.map((cart) => {
                let product = cart.get("product");
                let product_variant = cart.get("product_variant");
                let variant;
                if(product_variant){
                    variant = product_variant.get("variant");
                }
                let name = product.get("name");
                let price = product.get("price");
                let quantity = cart.get("quantity");
                //create new item
                let item = new Parse.Object("Item");
                item.set("product", product);
                item.set("variant", variant);
                item.set("name", name);
                item.set("price", price);
                item.set("quantity", quantity);
                item.set("user", user);
                items.push(item);
            });
            items = await Parse.Object.saveAll(items);
            //add items to orders
            order.relation("items").add(items);
            await order.save();
            Parse.Object.destroyAll(carts);
            this.view.setCarts([]);
            this.view.setOrder(null);
            this.view.hideProgress();
            this.view.gotoOrderSuccess(order.id);
        } catch (e) {
            alert(e);
            this.view.hideProgress();
        }
    }
}

export default PaymentPresenter