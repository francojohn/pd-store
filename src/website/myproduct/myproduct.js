import React, {Component} from "react";
import {Link} from 'react-router-dom';
import Parse from 'parse';
import './style.css';

class myproduct extends Component {
    render() {

        return (
            <div className="myproduct container">
                <ul className="nav nav-pills my-5" id="pills-tab" role="tablist">
                    <li className="nav-item">
                        <a className="nav-link active font-size-xs" id="pills-ALL-tab" data-toggle="pill" href="#pills-ALL"
                           role="tab" aria-controls="pills-ALL" aria-selected="true">ALL</a>
                    </li>
                    <li className="nav-item">
                        <a className="nav-link font-size-xs" id="pills-LIVE-tab" data-toggle="pill" href="#pills-LIVE"
                           role="tab" aria-controls="pills-LIVE" aria-selected="false">LIVE</a>
                    </li>
                    <li className="nav-item">
                        <a className="nav-link font-size-xs" id="pills-SOLDOUT-tab" data-toggle="pill" href="#pills-SOLDOUT"
                           role="tab" aria-controls="pills-SOLDOUT" aria-selected="false">SOLD OUT</a>
                    </li>
                    <li className="nav-item">
                        <a className="nav-link font-size-xs" id="pills-SUSPENDED-tab" data-toggle="pill" href="#pills-SUSPENDED"
                           role="tab" aria-controls="pills-SUSPENDED" aria-selected="false">SUSPENDED</a>
                    </li>
                    <li className="nav-item">
                        <a className="nav-link font-size-xs" id="pills-UNLISTED-tab" data-toggle="pill" href="#pills-UNLISTED"
                           role="tab" aria-controls="pills-UNLISTED" aria-selected="false">UNLISTED</a>
                    </li>
                </ul>
                <div className="tab-content" id="pills-tabContent">
                    <div className="tab-pane fade show active" id="pills-ALL" role="tabpanel"
                         aria-labelledby="pills-ALL-tab">
                        <div className="row">
                            <div className="col-sm-6 mt-3">
                                <span className="float-left font-size-xs pr-3">
                                    24 Products
                                </span>
                                <div className="progress float-left" style={{width: "260px"}}>
                                    <div className="progress-bar" role="progressbar" style={{width: "24%"}} aria-valuenow="25"
                                         aria-valuemin="0" aria-valuemax="100">24
                                    </div>
                                </div>
                                <div className="clearfix"></div>
                            </div>
                            <div className="col-sm-6">
                                <div className="nav-item dropdown float-right">
                                    <a className="nav-link dropdown-toggle font-size-xs text-uppercase" data-toggle="dropdown" href="#" role="button"
                                       aria-haspopup="true" aria-expanded="false">Batch Actions</a>
                                    <div className="dropdown-menu">
                                        <a className="dropdown-item font-size-xs text-uppercase" href="#">Add New Products</a>
                                        <a className="dropdown-item font-size-xs text-uppercase" href="#">Edit Existing Products</a>
                                    </div>
                                </div>
                                <div className="clearfix"></div>
                            </div>
                        </div>
                        <hr />
                        <div className="row mt-5 mb-4">
                            <div className="col-sm-4 p-0">
                                <div className="form-row">
                                    <div className="col-1">
                                        <div className="input-group-prepend">
                                            <div className="input-group-text bg-white border-0 mt-1">
                                                <i className="fa fa-search pt-1" style={{"font-size":"12px","color": "#aaa"}}></i>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-11">
                                        <div className="form-group">
                                            <input type="text"
                                                   className="form-control custom-form-control rounded-0 p-1"
                                                   placeholder="Search" required id="signin_email"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-sm-8">
                                <ul className="sorting float-right pt-3">
                                    <li className="d-inline p-2 active">
                                        <a href="#" className="text-primary font-size-xs text-uppercase">
                                            Recent
                                        </a>
                                    </li>
                                    <li className="d-inline p-2">
                                        <a href="#" className="text-muted font-size-xs text-uppercase">
                                            Top Sales
                                        </a>
                                    </li>
                                    <li className="d-inline p-2">
                                        <a href="#" className="text-muted font-size-xs text-uppercase">
                                            Prices
                                        </a>
                                    </li>
                                </ul>
                                <div className="clearfix"></div>
                            </div>
                        </div>
                        <div className="row mb-5">
                            <div className="col-sm-2 p-1">
                                <div className="product-item-add mb-2 p-3 text-center">
                                    <div className="add-btn" style={{marginTop: "80%"}}>
                                        <a href="#">
                                            <div className="btn btn-primary rounded-circle" style={{width: "50px",height: "50px"}}>
                                                <i className="fa fa-plus text-white" style={{paddingTop: "10px"}}></i>
                                            </div>
                                            <br />
                                            <span className="font-size-xs">Add a New Product</span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div className="col-sm-2 p-1">
                                <div className=" product-item border mb-2 p-3">
                                    <input type="checkbox" className="form-check-input" style={{marginLeft: "0px"}} />
                                    <img src="/hoodie-woman-3.png" className="img-fluid w-auto" />
                                    <div className="content">
                                        <h4 className="font-size-sm mt-3 text-primary">Product Title</h4>
                                        <label className="float-left font-size-xs text-muted">₱350.00</label>
                                        <label className="float-right font-size-xs text-muted">₱350.00</label>
                                        <div className="clearfix"></div>
                                        <div className="float-left p-1 font-size-sm text-center" style={{width: "33.33%"}}>
                                            <a href="#" className="font-size-xs text-muted">
                                                <i className="fas fa-eye"></i>&nbsp;&nbsp;22
                                            </a>
                                        </div>
                                        <div className="float-left p-1 font-size-sm text-center" style={{width: "33.33%"}}>
                                            <a href="#" className="font-size-xs text-muted">
                                                <i className="fas fa-heart"></i>&nbsp;&nbsp;22
                                            </a>
                                        </div>
                                        <div className="float-left p-1 font-size-sm text-center" style={{width: "33.33%"}}>
                                            <a href="#" className="font-size-xs text-muted">
                                                Sales&nbsp;&nbsp;0
                                            </a>
                                        </div>
                                        <div className="clearfix"></div>
                                        <hr className="my-1" />
                                        <div className="nav-item dropdown text-center">
                                            <a className="nav-link dropdown-toggle font-size-xs text-uppercase" data-toggle="dropdown" href="#" role="button"
                                               aria-haspopup="true" aria-expanded="false">Batch Actions</a>
                                            <div className="dropdown-menu">
                                                <a className="dropdown-item font-size-xs text-uppercase" href="#">Add New Products</a>
                                                <a className="dropdown-item font-size-xs text-uppercase" href="#">Edit Existing Products</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-sm-2 p-1">
                                <div className=" product-item border mb-2 p-3">
                                    <input type="checkbox" className="form-check-input" style={{marginLeft: "0px"}} />
                                    <img src="/hoodie-woman-3.png" className="img-fluid w-auto" />
                                    <div className="content">
                                        <h4 className="font-size-sm mt-3 text-primary">Product Title</h4>
                                        <label className="float-left font-size-xs text-muted">₱350.00</label>
                                        <label className="float-right font-size-xs text-muted">₱350.00</label>
                                        <div className="clearfix"></div>
                                        <div className="float-left p-1 font-size-sm text-center" style={{width: "33.33%"}}>
                                            <a href="#" className="font-size-xs text-muted">
                                                <i className="fas fa-eye"></i>&nbsp;&nbsp;22
                                            </a>
                                        </div>
                                        <div className="float-left p-1 font-size-sm text-center" style={{width: "33.33%"}}>
                                            <a href="#" className="font-size-xs text-muted">
                                                <i className="fas fa-heart"></i>&nbsp;&nbsp;22
                                            </a>
                                        </div>
                                        <div className="float-left p-1 font-size-sm text-center" style={{width: "33.33%"}}>
                                            <a href="#" className="font-size-xs text-muted">
                                                Sales&nbsp;&nbsp;0
                                            </a>
                                        </div>
                                        <div className="clearfix"></div>
                                        <hr className="my-1" />
                                        <div className="nav-item dropdown text-center">
                                            <a className="nav-link dropdown-toggle font-size-xs text-uppercase" data-toggle="dropdown" href="#" role="button"
                                               aria-haspopup="true" aria-expanded="false">Batch Actions</a>
                                            <div className="dropdown-menu">
                                                <a className="dropdown-item font-size-xs text-uppercase" href="#">Add New Products</a>
                                                <a className="dropdown-item font-size-xs text-uppercase" href="#">Edit Existing Products</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-sm-2 p-1">
                                <div className=" product-item border mb-2 p-3">
                                    <input type="checkbox" className="form-check-input" style={{marginLeft: "0px"}} />
                                    <img src="/hoodie-woman-3.png" className="img-fluid w-auto" />
                                    <div className="content">
                                        <h4 className="font-size-sm mt-3 text-primary">Product Title</h4>
                                        <label className="float-left font-size-xs text-muted">₱350.00</label>
                                        <label className="float-right font-size-xs text-muted">₱350.00</label>
                                        <div className="clearfix"></div>
                                        <div className="float-left p-1 font-size-sm text-center" style={{width: "33.33%"}}>
                                            <a href="#" className="font-size-xs text-muted">
                                                <i className="fas fa-eye"></i>&nbsp;&nbsp;22
                                            </a>
                                        </div>
                                        <div className="float-left p-1 font-size-sm text-center" style={{width: "33.33%"}}>
                                            <a href="#" className="font-size-xs text-muted">
                                                <i className="fas fa-heart"></i>&nbsp;&nbsp;22
                                            </a>
                                        </div>
                                        <div className="float-left p-1 font-size-sm text-center" style={{width: "33.33%"}}>
                                            <a href="#" className="font-size-xs text-muted">
                                                Sales&nbsp;&nbsp;0
                                            </a>
                                        </div>
                                        <div className="clearfix"></div>
                                        <hr className="my-1" />
                                        <div className="nav-item dropdown text-center">
                                            <a className="nav-link dropdown-toggle font-size-xs text-uppercase" data-toggle="dropdown" href="#" role="button"
                                               aria-haspopup="true" aria-expanded="false">Batch Actions</a>
                                            <div className="dropdown-menu">
                                                <a className="dropdown-item font-size-xs text-uppercase" href="#">Add New Products</a>
                                                <a className="dropdown-item font-size-xs text-uppercase" href="#">Edit Existing Products</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-sm-2 p-1">
                                <div className=" product-item border mb-2 p-3">
                                    <input type="checkbox" className="form-check-input" style={{marginLeft: "0px"}} />
                                    <img src="/hoodie-woman-3.png" className="img-fluid w-auto" />
                                    <div className="content">
                                        <h4 className="font-size-sm mt-3 text-primary">Product Title</h4>
                                        <label className="float-left font-size-xs text-muted">₱350.00</label>
                                        <label className="float-right font-size-xs text-muted">₱350.00</label>
                                        <div className="clearfix"></div>
                                        <div className="float-left p-1 font-size-sm text-center" style={{width: "33.33%"}}>
                                            <a href="#" className="font-size-xs text-muted">
                                                <i className="fas fa-eye"></i>&nbsp;&nbsp;22
                                            </a>
                                        </div>
                                        <div className="float-left p-1 font-size-sm text-center" style={{width: "33.33%"}}>
                                            <a href="#" className="font-size-xs text-muted">
                                                <i className="fas fa-heart"></i>&nbsp;&nbsp;22
                                            </a>
                                        </div>
                                        <div className="float-left p-1 font-size-sm text-center" style={{width: "33.33%"}}>
                                            <a href="#" className="font-size-xs text-muted">
                                                Sales&nbsp;&nbsp;0
                                            </a>
                                        </div>
                                        <div className="clearfix"></div>
                                        <hr className="my-1" />
                                        <div className="nav-item dropdown text-center">
                                            <a className="nav-link dropdown-toggle font-size-xs text-uppercase" data-toggle="dropdown" href="#" role="button"
                                               aria-haspopup="true" aria-expanded="false">Batch Actions</a>
                                            <div className="dropdown-menu">
                                                <a className="dropdown-item font-size-xs text-uppercase" href="#">Add New Products</a>
                                                <a className="dropdown-item font-size-xs text-uppercase" href="#">Edit Existing Products</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-sm-2 p-1">
                                <div className=" product-item border mb-2 p-3">
                                    <input type="checkbox" className="form-check-input" style={{marginLeft: "0px"}} />
                                    <img src="/hoodie-woman-3.png" className="img-fluid w-auto" />
                                    <div className="content">
                                        <h4 className="font-size-sm mt-3 text-primary">Product Title</h4>
                                        <label className="float-left font-size-xs text-muted">₱350.00</label>
                                        <label className="float-right font-size-xs text-muted">₱350.00</label>
                                        <div className="clearfix"></div>
                                        <div className="float-left p-1 font-size-sm text-center" style={{width: "33.33%"}}>
                                            <a href="#" className="font-size-xs text-muted">
                                                <i className="fas fa-eye"></i>&nbsp;&nbsp;22
                                            </a>
                                        </div>
                                        <div className="float-left p-1 font-size-sm text-center" style={{width: "33.33%"}}>
                                            <a href="#" className="font-size-xs text-muted">
                                                <i className="fas fa-heart"></i>&nbsp;&nbsp;22
                                            </a>
                                        </div>
                                        <div className="float-left p-1 font-size-sm text-center" style={{width: "33.33%"}}>
                                            <a href="#" className="font-size-xs text-muted">
                                                Sales&nbsp;&nbsp;0
                                            </a>
                                        </div>
                                        <div className="clearfix"></div>
                                        <hr className="my-1" />
                                        <div className="nav-item dropdown text-center">
                                            <a className="nav-link dropdown-toggle font-size-xs text-uppercase" data-toggle="dropdown" href="#" role="button"
                                               aria-haspopup="true" aria-expanded="false">Batch Actions</a>
                                            <div className="dropdown-menu">
                                                <a className="dropdown-item font-size-xs text-uppercase" href="#">Add New Products</a>
                                                <a className="dropdown-item font-size-xs text-uppercase" href="#">Edit Existing Products</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-sm-2 p-1">
                                <div className=" product-item border mb-2 p-3">
                                    <input type="checkbox" className="form-check-input" style={{marginLeft: "0px"}} />
                                    <img src="/hoodie-woman-3.png" className="img-fluid w-auto" />
                                    <div className="content">
                                        <h4 className="font-size-sm mt-3 text-primary">Product Title</h4>
                                        <label className="float-left font-size-xs text-muted">₱350.00</label>
                                        <label className="float-right font-size-xs text-muted">₱350.00</label>
                                        <div className="clearfix"></div>
                                        <div className="float-left p-1 font-size-sm text-center" style={{width: "33.33%"}}>
                                            <a href="#" className="font-size-xs text-muted">
                                                <i className="fas fa-eye"></i>&nbsp;&nbsp;22
                                            </a>
                                        </div>
                                        <div className="float-left p-1 font-size-sm text-center" style={{width: "33.33%"}}>
                                            <a href="#" className="font-size-xs text-muted">
                                                <i className="fas fa-heart"></i>&nbsp;&nbsp;22
                                            </a>
                                        </div>
                                        <div className="float-left p-1 font-size-sm text-center" style={{width: "33.33%"}}>
                                            <a href="#" className="font-size-xs text-muted">
                                                Sales&nbsp;&nbsp;0
                                            </a>
                                        </div>
                                        <div className="clearfix"></div>
                                        <hr className="my-1" />
                                        <div className="nav-item dropdown text-center">
                                            <a className="nav-link dropdown-toggle font-size-xs text-uppercase" data-toggle="dropdown" href="#" role="button"
                                               aria-haspopup="true" aria-expanded="false">Batch Actions</a>
                                            <div className="dropdown-menu">
                                                <a className="dropdown-item font-size-xs text-uppercase" href="#">Add New Products</a>
                                                <a className="dropdown-item font-size-xs text-uppercase" href="#">Edit Existing Products</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-sm-2 p-1">
                                <div className=" product-item border mb-2 p-3">
                                    <input type="checkbox" className="form-check-input" style={{marginLeft: "0px"}} />
                                    <img src="/hoodie-woman-3.png" className="img-fluid w-auto" />
                                    <div className="content">
                                        <h4 className="font-size-sm mt-3 text-primary">Product Title</h4>
                                        <label className="float-left font-size-xs text-muted">₱350.00</label>
                                        <label className="float-right font-size-xs text-muted">₱350.00</label>
                                        <div className="clearfix"></div>
                                        <div className="float-left p-1 font-size-sm text-center" style={{width: "33.33%"}}>
                                            <a href="#" className="font-size-xs text-muted">
                                                <i className="fas fa-eye"></i>&nbsp;&nbsp;22
                                            </a>
                                        </div>
                                        <div className="float-left p-1 font-size-sm text-center" style={{width: "33.33%"}}>
                                            <a href="#" className="font-size-xs text-muted">
                                                <i className="fas fa-heart"></i>&nbsp;&nbsp;22
                                            </a>
                                        </div>
                                        <div className="float-left p-1 font-size-sm text-center" style={{width: "33.33%"}}>
                                            <a href="#" className="font-size-xs text-muted">
                                                Sales&nbsp;&nbsp;0
                                            </a>
                                        </div>
                                        <div className="clearfix"></div>
                                        <hr className="my-1" />
                                        <div className="nav-item dropdown text-center">
                                            <a className="nav-link dropdown-toggle font-size-xs text-uppercase" data-toggle="dropdown" href="#" role="button"
                                               aria-haspopup="true" aria-expanded="false">Batch Actions</a>
                                            <div className="dropdown-menu">
                                                <a className="dropdown-item font-size-xs text-uppercase" href="#">Add New Products</a>
                                                <a className="dropdown-item font-size-xs text-uppercase" href="#">Edit Existing Products</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-sm-2 p-1">
                                <div className=" product-item border mb-2 p-3">
                                    <input type="checkbox" className="form-check-input" style={{marginLeft: "0px"}} />
                                    <img src="/hoodie-woman-3.png" className="img-fluid w-auto" />
                                    <div className="content">
                                        <h4 className="font-size-sm mt-3 text-primary">Product Title</h4>
                                        <label className="float-left font-size-xs text-muted">₱350.00</label>
                                        <label className="float-right font-size-xs text-muted">₱350.00</label>
                                        <div className="clearfix"></div>
                                        <div className="float-left p-1 font-size-sm text-center" style={{width: "33.33%"}}>
                                            <a href="#" className="font-size-xs text-muted">
                                                <i className="fas fa-eye"></i>&nbsp;&nbsp;22
                                            </a>
                                        </div>
                                        <div className="float-left p-1 font-size-sm text-center" style={{width: "33.33%"}}>
                                            <a href="#" className="font-size-xs text-muted">
                                                <i className="fas fa-heart"></i>&nbsp;&nbsp;22
                                            </a>
                                        </div>
                                        <div className="float-left p-1 font-size-sm text-center" style={{width: "33.33%"}}>
                                            <a href="#" className="font-size-xs text-muted">
                                                Sales&nbsp;&nbsp;0
                                            </a>
                                        </div>
                                        <div className="clearfix"></div>
                                        <hr className="my-1" />
                                        <div className="nav-item dropdown text-center">
                                            <a className="nav-link dropdown-toggle font-size-xs text-uppercase" data-toggle="dropdown" href="#" role="button"
                                               aria-haspopup="true" aria-expanded="false">Batch Actions</a>
                                            <div className="dropdown-menu">
                                                <a className="dropdown-item font-size-xs text-uppercase" href="#">Add New Products</a>
                                                <a className="dropdown-item font-size-xs text-uppercase" href="#">Edit Existing Products</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-sm-2 p-1">
                                <div className=" product-item border mb-2 p-3">
                                    <input type="checkbox" className="form-check-input" style={{marginLeft: "0px"}} />
                                    <img src="/hoodie-woman-3.png" className="img-fluid w-auto" />
                                    <div className="content">
                                        <h4 className="font-size-sm mt-3 text-primary">Product Title</h4>
                                        <label className="float-left font-size-xs text-muted">₱350.00</label>
                                        <label className="float-right font-size-xs text-muted">₱350.00</label>
                                        <div className="clearfix"></div>
                                        <div className="float-left p-1 font-size-sm text-center" style={{width: "33.33%"}}>
                                            <a href="#" className="font-size-xs text-muted">
                                                <i className="fas fa-eye"></i>&nbsp;&nbsp;22
                                            </a>
                                        </div>
                                        <div className="float-left p-1 font-size-sm text-center" style={{width: "33.33%"}}>
                                            <a href="#" className="font-size-xs text-muted">
                                                <i className="fas fa-heart"></i>&nbsp;&nbsp;22
                                            </a>
                                        </div>
                                        <div className="float-left p-1 font-size-sm text-center" style={{width: "33.33%"}}>
                                            <a href="#" className="font-size-xs text-muted">
                                                Sales&nbsp;&nbsp;0
                                            </a>
                                        </div>
                                        <div className="clearfix"></div>
                                        <hr className="my-1" />
                                        <div className="nav-item dropdown text-center">
                                            <a className="nav-link dropdown-toggle font-size-xs text-uppercase" data-toggle="dropdown" href="#" role="button"
                                               aria-haspopup="true" aria-expanded="false">Batch Actions</a>
                                            <div className="dropdown-menu">
                                                <a className="dropdown-item font-size-xs text-uppercase" href="#">Add New Products</a>
                                                <a className="dropdown-item font-size-xs text-uppercase" href="#">Edit Existing Products</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-sm-2 p-1">
                                <div className=" product-item border mb-2 p-3">
                                    <input type="checkbox" className="form-check-input" style={{marginLeft: "0px"}} />
                                    <img src="/hoodie-woman-3.png" className="img-fluid w-auto" />
                                    <div className="content">
                                        <h4 className="font-size-sm mt-3 text-primary">Product Title</h4>
                                        <label className="float-left font-size-xs text-muted">₱350.00</label>
                                        <label className="float-right font-size-xs text-muted">₱350.00</label>
                                        <div className="clearfix"></div>
                                        <div className="float-left p-1 font-size-sm text-center" style={{width: "33.33%"}}>
                                            <a href="#" className="font-size-xs text-muted">
                                                <i className="fas fa-eye"></i>&nbsp;&nbsp;22
                                            </a>
                                        </div>
                                        <div className="float-left p-1 font-size-sm text-center" style={{width: "33.33%"}}>
                                            <a href="#" className="font-size-xs text-muted">
                                                <i className="fas fa-heart"></i>&nbsp;&nbsp;22
                                            </a>
                                        </div>
                                        <div className="float-left p-1 font-size-sm text-center" style={{width: "33.33%"}}>
                                            <a href="#" className="font-size-xs text-muted">
                                                Sales&nbsp;&nbsp;0
                                            </a>
                                        </div>
                                        <div className="clearfix"></div>
                                        <hr className="my-1" />
                                        <div className="nav-item dropdown text-center">
                                            <a className="nav-link dropdown-toggle font-size-xs text-uppercase" data-toggle="dropdown" href="#" role="button"
                                               aria-haspopup="true" aria-expanded="false">Batch Actions</a>
                                            <div className="dropdown-menu">
                                                <a className="dropdown-item font-size-xs text-uppercase" href="#">Add New Products</a>
                                                <a className="dropdown-item font-size-xs text-uppercase" href="#">Edit Existing Products</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-sm-2 p-1">
                                <div className=" product-item border mb-2 p-3">
                                    <input type="checkbox" className="form-check-input" style={{marginLeft: "0px"}} />
                                    <img src="/hoodie-woman-3.png" className="img-fluid w-auto" />
                                    <div className="content">
                                        <h4 className="font-size-sm mt-3 text-primary">Product Title</h4>
                                        <label className="float-left font-size-xs text-muted">₱350.00</label>
                                        <label className="float-right font-size-xs text-muted">₱350.00</label>
                                        <div className="clearfix"></div>
                                        <div className="float-left p-1 font-size-sm text-center" style={{width: "33.33%"}}>
                                            <a href="#" className="font-size-xs text-muted">
                                                <i className="fas fa-eye"></i>&nbsp;&nbsp;22
                                            </a>
                                        </div>
                                        <div className="float-left p-1 font-size-sm text-center" style={{width: "33.33%"}}>
                                            <a href="#" className="font-size-xs text-muted">
                                                <i className="fas fa-heart"></i>&nbsp;&nbsp;22
                                            </a>
                                        </div>
                                        <div className="float-left p-1 font-size-sm text-center" style={{width: "33.33%"}}>
                                            <a href="#" className="font-size-xs text-muted">
                                                Sales&nbsp;&nbsp;0
                                            </a>
                                        </div>
                                        <div className="clearfix"></div>
                                        <hr className="my-1" />
                                        <div className="nav-item dropdown text-center">
                                            <a className="nav-link dropdown-toggle font-size-xs text-uppercase" data-toggle="dropdown" href="#" role="button"
                                               aria-haspopup="true" aria-expanded="false">Batch Actions</a>
                                            <div className="dropdown-menu">
                                                <a className="dropdown-item font-size-xs text-uppercase" href="#">Add New Products</a>
                                                <a className="dropdown-item font-size-xs text-uppercase" href="#">Edit Existing Products</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-sm-2 p-1">
                                <div className=" product-item border mb-2 p-3">
                                    <input type="checkbox" className="form-check-input" style={{marginLeft: "0px"}} />
                                    <img src="/hoodie-woman-3.png" className="img-fluid w-auto" />
                                    <div className="content">
                                        <h4 className="font-size-sm mt-3 text-primary">Product Title</h4>
                                        <label className="float-left font-size-xs text-muted">₱350.00</label>
                                        <label className="float-right font-size-xs text-muted">₱350.00</label>
                                        <div className="clearfix"></div>
                                        <div className="float-left p-1 font-size-sm text-center" style={{width: "33.33%"}}>
                                            <a href="#" className="font-size-xs text-muted">
                                                <i className="fas fa-eye"></i>&nbsp;&nbsp;22
                                            </a>
                                        </div>
                                        <div className="float-left p-1 font-size-sm text-center" style={{width: "33.33%"}}>
                                            <a href="#" className="font-size-xs text-muted">
                                                <i className="fas fa-heart"></i>&nbsp;&nbsp;22
                                            </a>
                                        </div>
                                        <div className="float-left p-1 font-size-sm text-center" style={{width: "33.33%"}}>
                                            <a href="#" className="font-size-xs text-muted">
                                                Sales&nbsp;&nbsp;0
                                            </a>
                                        </div>
                                        <div className="clearfix"></div>
                                        <hr className="my-1" />
                                        <div className="nav-item dropdown text-center">
                                            <a className="nav-link dropdown-toggle font-size-xs text-uppercase" data-toggle="dropdown" href="#" role="button"
                                               aria-haspopup="true" aria-expanded="false">Batch Actions</a>
                                            <div className="dropdown-menu">
                                                <a className="dropdown-item font-size-xs text-uppercase" href="#">Add New Products</a>
                                                <a className="dropdown-item font-size-xs text-uppercase" href="#">Edit Existing Products</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-sm-2 p-1">
                                <div className=" product-item border mb-2 p-3">
                                    <input type="checkbox" className="form-check-input" style={{marginLeft: "0px"}} />
                                    <img src="/hoodie-woman-3.png" className="img-fluid w-auto" />
                                    <div className="content">
                                        <h4 className="font-size-sm mt-3 text-primary">Product Title</h4>
                                        <label className="float-left font-size-xs text-muted">₱350.00</label>
                                        <label className="float-right font-size-xs text-muted">₱350.00</label>
                                        <div className="clearfix"></div>
                                        <div className="float-left p-1 font-size-sm text-center" style={{width: "33.33%"}}>
                                            <a href="#" className="font-size-xs text-muted">
                                                <i className="fas fa-eye"></i>&nbsp;&nbsp;22
                                            </a>
                                        </div>
                                        <div className="float-left p-1 font-size-sm text-center" style={{width: "33.33%"}}>
                                            <a href="#" className="font-size-xs text-muted">
                                                <i className="fas fa-heart"></i>&nbsp;&nbsp;22
                                            </a>
                                        </div>
                                        <div className="float-left p-1 font-size-sm text-center" style={{width: "33.33%"}}>
                                            <a href="#" className="font-size-xs text-muted">
                                                Sales&nbsp;&nbsp;0
                                            </a>
                                        </div>
                                        <div className="clearfix"></div>
                                        <hr className="my-1" />
                                        <div className="nav-item dropdown text-center">
                                            <a className="nav-link dropdown-toggle font-size-xs text-uppercase" data-toggle="dropdown" href="#" role="button"
                                               aria-haspopup="true" aria-expanded="false">Batch Actions</a>
                                            <div className="dropdown-menu">
                                                <a className="dropdown-item font-size-xs text-uppercase" href="#">Add New Products</a>
                                                <a className="dropdown-item font-size-xs text-uppercase" href="#">Edit Existing Products</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-sm-2 p-1">
                                <div className=" product-item border mb-2 p-3">
                                    <input type="checkbox" className="form-check-input" style={{marginLeft: "0px"}} />
                                    <img src="/hoodie-woman-3.png" className="img-fluid w-auto" />
                                    <div className="content">
                                        <h4 className="font-size-sm mt-3 text-primary">Product Title</h4>
                                        <label className="float-left font-size-xs text-muted">₱350.00</label>
                                        <label className="float-right font-size-xs text-muted">₱350.00</label>
                                        <div className="clearfix"></div>
                                        <div className="float-left p-1 font-size-sm text-center" style={{width: "33.33%"}}>
                                            <a href="#" className="font-size-xs text-muted">
                                                <i className="fas fa-eye"></i>&nbsp;&nbsp;22
                                            </a>
                                        </div>
                                        <div className="float-left p-1 font-size-sm text-center" style={{width: "33.33%"}}>
                                            <a href="#" className="font-size-xs text-muted">
                                                <i className="fas fa-heart"></i>&nbsp;&nbsp;22
                                            </a>
                                        </div>
                                        <div className="float-left p-1 font-size-sm text-center" style={{width: "33.33%"}}>
                                            <a href="#" className="font-size-xs text-muted">
                                                Sales&nbsp;&nbsp;0
                                            </a>
                                        </div>
                                        <div className="clearfix"></div>
                                        <hr className="my-1" />
                                        <div className="nav-item dropdown text-center">
                                            <a className="nav-link dropdown-toggle font-size-xs text-uppercase" data-toggle="dropdown" href="#" role="button"
                                               aria-haspopup="true" aria-expanded="false">Batch Actions</a>
                                            <div className="dropdown-menu">
                                                <a className="dropdown-item font-size-xs text-uppercase" href="#">Add New Products</a>
                                                <a className="dropdown-item font-size-xs text-uppercase" href="#">Edit Existing Products</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-sm-2 p-1">
                                <div className=" product-item border mb-2 p-3">
                                    <input type="checkbox" className="form-check-input" style={{marginLeft: "0px"}} />
                                    <img src="/hoodie-woman-3.png" className="img-fluid w-auto" />
                                    <div className="content">
                                        <h4 className="font-size-sm mt-3 text-primary">Product Title</h4>
                                        <label className="float-left font-size-xs text-muted">₱350.00</label>
                                        <label className="float-right font-size-xs text-muted">₱350.00</label>
                                        <div className="clearfix"></div>
                                        <div className="float-left p-1 font-size-sm text-center" style={{width: "33.33%"}}>
                                            <a href="#" className="font-size-xs text-muted">
                                                <i className="fas fa-eye"></i>&nbsp;&nbsp;22
                                            </a>
                                        </div>
                                        <div className="float-left p-1 font-size-sm text-center" style={{width: "33.33%"}}>
                                            <a href="#" className="font-size-xs text-muted">
                                                <i className="fas fa-heart"></i>&nbsp;&nbsp;22
                                            </a>
                                        </div>
                                        <div className="float-left p-1 font-size-sm text-center" style={{width: "33.33%"}}>
                                            <a href="#" className="font-size-xs text-muted">
                                                Sales&nbsp;&nbsp;0
                                            </a>
                                        </div>
                                        <div className="clearfix"></div>
                                        <hr className="my-1" />
                                        <div className="nav-item dropdown text-center">
                                            <a className="nav-link dropdown-toggle font-size-xs text-uppercase" data-toggle="dropdown" href="#" role="button"
                                               aria-haspopup="true" aria-expanded="false">Batch Actions</a>
                                            <div className="dropdown-menu">
                                                <a className="dropdown-item font-size-xs text-uppercase" href="#">Add New Products</a>
                                                <a className="dropdown-item font-size-xs text-uppercase" href="#">Edit Existing Products</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <nav aria-label="Page navigation example">
                            <ul className="pagination justify-content-center">
                                <li className="page-item disabled">
                                    <a className="page-link font-size-xs text-muted" href="#" tabIndex="-1">Previous</a>
                                </li>
                                <li className="page-item font-size-xs text-muted active"><a className="page-link" href="#">1</a></li>
                                <li className="page-item font-size-xs text-muted"><a className="page-link" href="#">2</a></li>
                                <li className="page-item font-size-xs text-muted"><a className="page-link" href="#">3</a></li>
                                <li className="page-item font-size-xs text-muted">
                                    <a className="page-link" href="#">Next</a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                    <div className="tab-pane fade" id="pills-LIVE" role="tabpanel"
                         aria-labelledby="pills-LIVE-tab">LIVE
                    </div>
                    <div className="tab-pane fade" id="pills-SOLDOUT" role="tabpanel"
                         aria-labelledby="pills-SOLDOUT-tab">SOLDOUT
                    </div>
                    <div className="tab-pane fade" id="pills-SUSPENDED" role="tabpanel"
                         aria-labelledby="pills-SUSPENDED-tab">SUSPENDED
                    </div>
                    <div className="tab-pane fade" id="pills-UNLISTED" role="tabpanel"
                         aria-labelledby="pills-UNLISTED-tab">UNLISTED
                    </div>
                </div>

            </div>
        );
    }
}

export default myproduct;