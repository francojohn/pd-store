import React, {Component} from "react";
import {Link} from 'react-router-dom';

class Order extends Component {
    getOrderId() {
        return this.props.match.params.id;
    }
    componentDidMount(){
        window.scrollTo(0, 0);
    }
    render() {
        return (
            <div className="order jumbotron bg-transparent">
                <div className="container">
                    <div className="row">
                        <div className="col-sm-12 p-5 text-center">
                            <h1 style={{fontSize: '76px'}} className="mb-3 text-primary">THANK YOU</h1>
                            <label className="font-size-md">FOR SHOPPING WITH US</label>
                            <br/>
                            <br/>
                            <br/>
                            {/*<p>We will get in touch with you within 2 business days to confirm your order.</p>*/}
                            <h5 className="text-secondary">Order Reference No. <b>{this.getOrderId()}</b></h5>
                            <Link to="/" className="text-uppercase">
                                <small>CONTINUE SHOPPING</small>
                            </Link>
                        </div>
                    </div>
                </div>
                <br/><br/><br/>
                <div className="row">
                    <div className="col-sm-2">
                        {/*<ItemProduct/>*/}
                    </div>
                    <div className="col-sm-2">
                        {/*<ItemProduct/>*/}
                    </div>
                    <div className="col-sm-2">
                        {/*<ItemProduct/>*/}
                    </div>
                    <div className="col-sm-2">
                        {/*<ItemProduct/>*/}
                    </div>
                    <div className="col-sm-2">
                        {/*<ItemProduct/>*/}
                    </div>
                    <div className="col-sm-2">
                        {/*<ItemProduct/>*/}
                    </div>
                </div>
            </div>
        );
    }
}

export default Order;