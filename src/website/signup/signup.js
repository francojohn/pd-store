import React, {Component} from 'react';

import Parse from 'parse';
//router
import {Link} from 'react-router-dom';

class Signup extends Component {
    constructor() {
        super();
    }

    componentDidMount() {
    }


    signUp =(e)=> {
        e.preventDefault();
        console.log("click");
        //check password
        let password = e.target.signup_password.value;
        let re_password = e.target.signup_re_password.value;


        var user = new Parse.User();
        user.set("firstname", e.target.signup_firstname.value);
        user.set("lastname", e.target.signup_lastname.value);
        user.set("username", e.target.signup_email.value);
        user.set("email", e.target.signup_email.value);
        user.set("password", password);
        user.signUp(null).then((user) => {
            this.props.history.push('/');
        }, (err) => {
            alert(err);
        });

    }


    render() {
        return (
            <div>
                <div className="container-fluid">
                    <div className="container mt-4" style={{width: '38rem'}}>
                        <form onSubmit={this.signUp}>
                            <div className="form-group row pr-3">
                                <div className="input-group">
                                            <span className="input-group-text bg-white border-0"><i
                                                className="fa fa-user"></i></span>
                                    <input id="signup_firstname" type="text" className="form-control rounded-0"
                                           placeholder="First Name"
                                           required/>
                                </div>
                            </div>

                            <div className="form-group row pr-3">
                                <div className="input-group">
                                            <span className="input-group-text bg-white border-0"><i
                                                className="fa fa-user"></i></span>
                                    <input id="signup_lastname" type="text" className="form-control rounded-0"
                                           placeholder="Last Name"
                                           required/>
                                </div>
                            </div>

                            <div className="form-group row pr-3">
                                <div className="input-group">
                                            <span className="input-group-text bg-white border-0"><i
                                                className="fa fa-envelope"></i></span>
                                    <input id="signup_email" type="text" className="form-control rounded-0"
                                           placeholder="Email"
                                           required/>
                                </div>
                            </div>


                            <div className="form-group row pr-3">
                                <div className="input-group">
                                            <span className="input-group-text bg-white border-0"><i
                                                className="fa fa-lock"></i></span>
                                    <input id="signup_password" type="password" className="form-control rounded-0"
                                           placeholder="Password"
                                           required/>
                                </div>
                            </div>


                            <div className="form-group row pr-3">
                                <div className="input-group">
                                            <span className="input-group-text bg-white border-0"><i
                                                className="fa fa-lock"></i></span>
                                    <input id="signup_re_password" type="password" className="form-control rounded-0"
                                           placeholder="Confirm Password"
                                           required/>
                                </div>
                            </div>
                            <div className="form-group">
                                <button type="submit" className="btn btn-block text-white bg-info mt-1 rounded-0`">Sign
                                    up
                                </button>
                            </div>


                            <div className="form-group text-center mt-1">
                                <p className="text-muted"> Already have an account?  &nbsp;
                                    <Link to="/signin">Sign in here</Link>
                                </p>
                            </div>

                        </form>
                    </div>

                </div>
            </div>

        );
    }
}

export default Signup;