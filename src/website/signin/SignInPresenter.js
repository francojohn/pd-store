import Parse from "parse";

class SignInPresenter {
    constructor(view) {
        this.view = view;
    }

    componentDidMount() {
        let user = Parse.User.current();
        if (user) {
            this.view.gotoHome();
        }
    }

    async formSubmit() {
        let email = this.view.getEmail();
        let password = this.view.getPassword();
        try {
            let user = await Parse.User.logIn(email, password);
            this.view.hideProgress();
            if (user) {
                this.view.gotoHome();
            }

        } catch (e) {
            this.view.hideProgress();
            this.view.showErrorMessage(e.message);
        }

    }

    async facebookLoginClick() {
        try {
            let user = await Parse.FacebookUtils.logIn("public_profile,email", {});
            if (!user.has("name")) {
                //get userinfo
                let permission = {fields: 'name, email, first_name, last_name, gender,picture.width(200).height(200)'}
                let response = await new Promise(function (resolve, reject) {
                    window.FB.api('/me', permission, function (response) {
                        resolve(response);
                    });
                });
                let {name, first_name, last_name, email, picture} = response;
                let pictureUrl = picture.data.url;
                user.set('name', name);
                user.set('first_name', first_name);
                user.set('last_name', last_name);
                user.set('email', email);

                //get user picture
                let base64 = await new Promise(function (resolve, reject) {
                    let img = new Image();
                    img.src = pictureUrl;
                    img.setAttribute('crossOrigin', 'anonymous');
                    img.onload = (() => {
                        let canvas = document.createElement("canvas");
                        canvas.width = img.width;
                        canvas.height = img.height;
                        let ctx = canvas.getContext("2d");
                        ctx.drawImage(img, 0, 0);
                        let dataURL = canvas.toDataURL();
                        resolve(dataURL);
                    });
                });

                let parseFile = new Parse.File("newImage", {base64: base64});
                parseFile = await parseFile.save();
                user.set('picture', parseFile);
                await user.save();
            }
            this.view.gotoHome();
        } catch (e) {
            console.log(e);
        }
    }
}

export default SignInPresenter;
