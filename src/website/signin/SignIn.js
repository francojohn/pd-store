import React, {Component} from "react";
import {Link} from 'react-router-dom';
import SignInPresenter from "./SignInPresenter";
import MessageDialog from "../../components/MessageDialog";

class SignIn extends Component {
    constructor() {
        super();
        this.presenter = new SignInPresenter(this);
        this.state = {
            isError: false,
            errorMessage: '',
        };
    }

    componentDidMount() {
        this.presenter.componentDidMount();
    }

    getEmail() {
        return document.getElementById("signin_email").value;
    }

    getPassword() {
        return document.getElementById("signin_password").value;
    }

    gotoHome() {
        this.props.history.push('/');
    }

    formOnsubmit = (e) => {
        e.preventDefault();
        this.presenter.formSubmit();
    }

    showProgress() {
        let button = document.getElementById("signin_btn_login");
        button.disabled = true;
        button.classList.add('bg-progress');
        button.innerHTML = "Please Wait..";
    }

    hideProgress() {
        let button = document.getElementById("signin_btn_login");
        button.disabled = false;
        button.classList.remove('bg-progress');
        button.innerHTML = "SIGN IN";
    }


    showErrorMessage(message) {
        this.setState({isError: true, errorMessage: message});
    }

    facebookLoginClick = () => {
        this.presenter.facebookLoginClick();
    }

    hideErrorMessage = () => {
        this.setState({isError: false});
    }

    render() {
        let dialog = null;
        if (this.state.isError) {
            dialog = (<MessageDialog title="Ooops!" message={this.state.errorMessage} close={this.hideErrorMessage}/>);
        }
        return (
            <div className="Login">
                {dialog}
                <div className="container">
                    <div className="row">
                        <div className="col-sm-6 offset-sm-3 my-5">
                            <div className="card border-0 my-3">
                                <form onSubmit={this.formOnsubmit}>
                                    <div className="card-body px-4 py-5">
                                        <blockquote className="text-center font-size-lg text-primary mb-3">Login you
                                            Account
                                        </blockquote>
                                        <div className="form-row">
                                            <div className="col-1">
                                                <div className="input-group-prepend">
                                                    <div className="input-group-text bg-white border-0 mt-1">
                                                        <i className="fa fa-envelope pt-1"
                                                           style={{"font-size": "12px", "color": "#aaa"}}></i>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="col-11">
                                                <div className="form-group">
                                                    <input type="email"
                                                           className="form-control custom-form-control rounded-0 p-1"
                                                           placeholder="Email" required id="signin_email"/>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="form-row">
                                            <div className="col-1">
                                                <div className="input-group-prepend">
                                                    <div className="input-group-text bg-white border-0 mt-1">
                                                        <i className="fa fa-lock pt-1"
                                                           style={{"font-size": "15px", "color": "#aaa"}}></i>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="col-11">
                                                <div className="form-group">
                                                    <input type="password"
                                                           className="form-control custom-form-control rounded-0 p-1"
                                                           placeholder="Password" required id="signin_password"/>
                                                    <a href="#"
                                                       className="float-right d-block font-size-sm my-2 text-right">Forgot
                                                        Password?</a>
                                                    <div className="clearfix"></div>
                                                    <br/><br/>
                                                </div>
                                            </div>
                                        </div>
                                        <button
                                            className="btn btn-primary rounded text-white font-size-sm px-5 d-block w-100"
                                            id="signin_btn_login">SIGN IN
                                        </button>
                                        <br/>
                                        <div className="dropdown-divider"></div>
                                        <br/>
                                        <button type="button" onClick={this.facebookLoginClick}
                                                className="btn rounded text-white d-block text-uppercase w-100 font-size-sm"
                                                style={{background: '#3b5998 '}}><i
                                            className="fab fa-facebook"></i>&nbsp;&nbsp;Login with facebook
                                        </button>
                                    </div>
                                </form>
                            </div>
                            <div className="text-center">
                                <h6 className="font-size-xs">New member? <Link to="/signup">Register here</Link></h6>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default SignIn;
