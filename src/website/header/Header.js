import React, {Component, Fragment} from 'react';
import {Link} from 'react-router-dom';
import Parse from 'parse';
import './style.css';
import HeaderPresenter from "./HeaderPresenter";

import BadgeNotify from "../../components/BadgeNotify";


class Header extends Component {
    constructor() {
        super();
        this.presenter = new HeaderPresenter(this)
    }
    logoutClick = () => {
        this.presenter.logoutClick();
    }
    signedUser(){
        let user = Parse.User.current();
        let name = user.get("name");
        return(
            <li className="nav-item m-auto">
                <a className="nav-link">
                    <div className="dropdown">
                        <a className="nav-link text-white font-size-xs d-inline"
                           data-toggle="dropdown" href="#">
                            <div className="media align-items-center">
                                <img src="/download.jpg" alt="header profile"
                                     style={{'height': '35px', 'width': '35px'}}
                                     className="img-fluid rounded-circle" />
                                <div className="dropdowntoggle media-body pl-2 text-nowrap text-capitalize">
                                    {name}
                                </div>
                            </div>
                        </a>
                        <ul className="dropdown-menu">
                            <li>
                                <Link to="/account" className="dropdown-item text-muted">
                                    <i className="fa fa-user" style={{'width': '10px'}}/>&nbsp;&nbsp;&nbsp;Profile
                                </Link>
                            </li>
                            <li>
                                <Link to="/account/wishlist" className="dropdown-item text-muted">
                                    <i className="fa fa-heart" style={{'width': '10px'}}/>&nbsp;&nbsp;&nbsp;Wishlist
                                </Link>
                            </li>
                            <li>
                                <Link to="/account/setting" className="dropdown-item text-muted">
                                    <i className="fa fa-cog" style={{'width': '10px'}}/>&nbsp;&nbsp;&nbsp;Settings
                                </Link>
                            </li>
                            <li>
                                <Link to="/account/help" className="dropdown-item text-muted">
                                    <i className="fa fa-question" style={{'width': '10px'}}/>&nbsp;&nbsp;&nbsp;Help
                                </Link>
                            </li>
                            <div role="separator" className="dropdown-divider"></div>
                            <li>
                                <button onClick={this.logoutClick} className="dropdown-item text-muted">
                                    <i className="fa fa-sign-out" style={{'width': '10px'}}/>&nbsp;&nbsp;&nbsp;Log out
                                </button>
                            </li>
                        </ul>
                    </div>
                </a>
            </li>
        );
    }

    noUser(){
        return(
            <li className="list-inline-item m-auto py-4">
                <Link to="/signin" className="nav-link text-white font-size-xs">
                    <BadgeNotify
                        badge='badge-danger'
                        text='Signin'/>
                </Link>
            </li>
        );
    }
    render() {
        let user = Parse.User.current();

        return (

                <header className="navbar navbar-light navbar-expand-md bg-primary sticky-top">
                    <div className="container">
                        <Link to="/" className="navbar-brand">
                            <img className="d-none d-sm-block" height="60" alt="header logo" src="/logo.png"/>
                        </Link>
                        <button className="navbar-toggler p-0 border-0" type="button" data-toggle="offcanvas">
                            <span className="navbar-toggler-icon"></span>
                        </button>

                        <div className="navbar-collapse offcanvas-collapse" id="navbarsExampleDefault">
                            <button className="navbar-toggler p-0 border-0 my-4" type="button" data-toggle="offcanvas">
                                <span className="fa fa-times"></span>
                            </button>
                            <div className="input-group ml-auto col-sm-6">
                                <input type="text" className="form-control" aria-label="Large" placeholder="To search, Type and hit Enter.."
                                       aria-describedby="inputGroup-sizing-sm" />
                                <div className="input-group-append">
                                    <button type="button" className="btn btn-outline-light rounded-0"><i
                                        className="fa fa-search" aria-hidden="true"></i>&nbsp;Search</button>
                                </div>
                            </div>
                            <ul className="navbar-nav ml-auto">
                                <li className="nav-item py-4 m-auto">
                                <Link to="/account/mywallet"
                                      className="nav-link text-white font-size-xs font-weight-bold">
                                    <BadgeNotify
                                        icon="fas fa-wallet fa-sm"
                                        text="  0.00"
                                    />
                                </Link>
                                </li>
                                <li className="nav-item py-4 m-auto">
                                <Link to="/cart" className="nav-link text-white font-size-xs">
                                    <BadgeNotify
                                        icon="fa fa-shopping-cart fa-sm"
                                        badge='badge-danger'
                                        text='&nbsp;&nbsp;Cart'
                                        value="32"/>
                                </Link>
                            </li>
                            <Fragment>
                                <li className="list-inline-item m-auto py-4">
                                    <div className="dropdown hover-dropdown">
                                        <a className="nav-link text-white font-size-xs"
                                           href="#">
                                            <BadgeNotify
                                                icon="fa fa-bell fa-sm"
                                                badge='badge-danger'
                                                text='&nbsp;&nbsp;Notification'/>
                                        </a>
                                        <ul className="dropdown-menu">
                                            <li className="text-center p-2">
                                                <div className="font-size-sm">Notification</div>
                                            </li>
                                            <li className="dropdown-notification overflow-y">
                                                <ul className="list-unstyled">
                                                    <li className="media  border-top p-2">
                                                        <img className="img-fluid rounded-circle" src="download.jpg" style={{width: '40px', height: '40px'}} />
                                                        <div className="media-body pl-2">
                                                            <div className="font-size-sm text-muted">John Doe</div>
                                                            <div className="font-size-xs font-weight-light">Just see the admin!
                                                            </div>
                                                            <div className="font-size-xxs font-weight-light">9:30 AM</div>
                                                        </div>
                                                    </li>
                                                    <li className="media  border-top p-2">
                                                        <img className="img-fluid rounded-circle"
                                                             style={{width: '40px', height: '40px'}}
                                                             src="/download.jpg"/>
                                                        <div className="media-body pl-2">
                                                            <div className="font-size-sm text-muted">john carlo franco</div>
                                                            <div className="font-size-xs font-weight-light">Just see the admin!
                                                            </div>
                                                            <div className="font-size-xxs font-weight-light">9:30 AM</div>
                                                        </div>
                                                    </li>
                                                    <li className="media  border-top p-2">
                                                        <img className="img-fluid rounded-circle"
                                                             style={{width: '40px', height: '40px'}}
                                                             src="http://www.leapcms.com/images/100pixels1.gif"/>
                                                        <div className="media-body pl-2">
                                                            <div className="font-size-sm text-muted">john carlo franco</div>
                                                            <div className="font-size-xs font-weight-light">Just see the admin!
                                                            </div>
                                                            <div className="font-size-xxs font-weight-light">9:30 AM</div>
                                                        </div>
                                                    </li>
                                                    <li className="media  border-top p-2">
                                                        <img className="img-fluid rounded-circle"
                                                             style={{width: '40px', height: '40px'}}
                                                             src="http://www.leapcms.com/images/100pixels1.gif"/>
                                                        <div className="media-body pl-2">
                                                            <div className="font-size-sm text-muted">john carlo franco</div>
                                                            <div className="font-size-xs font-weight-light">Just see the admin!
                                                            </div>
                                                            <div className="font-size-xxs font-weight-light">9:30 AM</div>
                                                        </div>
                                                    </li>
                                                    <li className="media  border-top p-2">
                                                        <img className="img-fluid rounded-circle"
                                                             style={{width: '40px', height: '40px'}}
                                                             src="http://www.leapcms.com/images/100pixels1.gif"/>
                                                        <div className="media-body pl-2">
                                                            <div className="font-size-sm text-muted">john carlo franco</div>
                                                            <div className="font-size-xs font-weight-light">Just see the admin!
                                                            </div>
                                                            <div className="font-size-xxs font-weight-light">9:30 AM</div>
                                                        </div>
                                                    </li>
                                                    <li className="media  border-top p-2">
                                                        <img className="img-fluid rounded-circle"
                                                             style={{width: '40px', height: '40px'}}
                                                             src="http://www.leapcms.com/images/100pixels1.gif"/>
                                                        <div className="media-body pl-2">
                                                            <div className="font-size-sm text-muted">john carlo franco</div>
                                                            <div className="font-size-xs font-weight-light">Just see the admin!
                                                            </div>
                                                            <div className="font-size-xxs font-weight-light">9:30 AM</div>
                                                        </div>
                                                    </li>
                                                    <li className="media  border-top p-2">
                                                        <img className="img-fluid rounded-circle"
                                                             style={{width: '40px', height: '40px'}}
                                                             src="http://www.leapcms.com/images/100pixels1.gif"/>
                                                        <div className="media-body pl-2">
                                                            <div className="font-size-sm text-muted">john carlo franco</div>
                                                            <div className="font-size-xs font-weight-light">Just see the admin!
                                                            </div>
                                                            <div className="font-size-xxs font-weight-light">9:30 AM</div>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </div>
                                </li>
                                {
                                    user ? this.signedUser() : this.noUser()
                                }
                            </Fragment>
                            </ul>
                        </div>
                    </div>
                </header>

        );
    }

}

export default Header;
