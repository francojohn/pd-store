import React, {Component, Fragment} from 'react';
import {Link} from 'react-router-dom';
import Parse from 'parse';
import './style.css';

import BadgeNotify from "../../components/BadgeNotify";
import {WithContext} from "../../Context";

class Header extends Component {

    state = {
        wallets: [],
    }

    componentDidMount() {
    }


    logOut() {
        Parse.User.logOut().then(() => {
            window.location.reload();
        });
    }

    walletTotal() {
        let available = 0;
        let total = 0;
        let current = 0;

        let wallet = this.state.wallets[0];
        if (wallet) {
            available = wallet.get('available_balance');
            current = wallet.get('current_balance');
            current = current ? current : 0;
            total = available + current;
        }
        let text = "\u00A0\u00A0" + total + "php";
        return text;
    }

    renderUser() {
        let user = Parse.User.current();
        if (user) {
            let first_name = user.get('first_name');
            let picture = user.get('picture');

            let pictureUrl = picture ? picture._url : "/anonymous_user.png";

            return (
                <Fragment>
                    <li className="list-inline-item mr-0">
                        <div className="dropdown hover-dropdown">
                            <a className="nav-link text-white font-size-xs font-weight-bold"
                               href="#">
                                <BadgeNotify
                                    icon="fa fa-bell fa-sm"
                                    badge='badge-danger'
                                    text='&nbsp;&nbsp;Notification'/>
                            </a>
                            <ul className="dropdown-menu">
                                <li className="text-center p-2">
                                    <div className="font-size-sm">Notification</div>
                                </li>
                                <li className="dropdown-notification overflow-y">
                                    <ul className="list-unstyled">
                                        <li className="media  border-top p-2">
                                            <img className="img-fluid rounded-circle"
                                                 style={{width: '40px', height: '40px'}}
                                                 src="http://www.leapcms.com/images/100pixels1.gif"/>
                                            <div className="media-body pl-2">
                                                <div className="font-size-sm text-muted">john carlo franco</div>
                                                <div className="font-size-xs font-weight-light">Just see the admin!
                                                </div>
                                                <div className="font-size-xxs font-weight-light">9:30 AM</div>
                                            </div>
                                        </li>
                                        <li className="media  border-top p-2">
                                            <img className="img-fluid rounded-circle"
                                                 style={{width: '40px', height: '40px'}}
                                                 src="http://www.leapcms.com/images/100pixels1.gif"/>
                                            <div className="media-body pl-2">
                                                <div className="font-size-sm text-muted">john carlo franco</div>
                                                <div className="font-size-xs font-weight-light">Just see the admin!
                                                </div>
                                                <div className="font-size-xxs font-weight-light">9:30 AM</div>
                                            </div>
                                        </li>
                                        <li className="media  border-top p-2">
                                            <img className="img-fluid rounded-circle"
                                                 style={{width: '40px', height: '40px'}}
                                                 src="http://www.leapcms.com/images/100pixels1.gif"/>
                                            <div className="media-body pl-2">
                                                <div className="font-size-sm text-muted">john carlo franco</div>
                                                <div className="font-size-xs font-weight-light">Just see the admin!
                                                </div>
                                                <div className="font-size-xxs font-weight-light">9:30 AM</div>
                                            </div>
                                        </li>
                                        <li className="media  border-top p-2">
                                            <img className="img-fluid rounded-circle"
                                                 style={{width: '40px', height: '40px'}}
                                                 src="http://www.leapcms.com/images/100pixels1.gif"/>
                                            <div className="media-body pl-2">
                                                <div className="font-size-sm text-muted">john carlo franco</div>
                                                <div className="font-size-xs font-weight-light">Just see the admin!
                                                </div>
                                                <div className="font-size-xxs font-weight-light">9:30 AM</div>
                                            </div>
                                        </li>
                                        <li className="media  border-top p-2">
                                            <img className="img-fluid rounded-circle"
                                                 style={{width: '40px', height: '40px'}}
                                                 src="http://www.leapcms.com/images/100pixels1.gif"/>
                                            <div className="media-body pl-2">
                                                <div className="font-size-sm text-muted">john carlo franco</div>
                                                <div className="font-size-xs font-weight-light">Just see the admin!
                                                </div>
                                                <div className="font-size-xxs font-weight-light">9:30 AM</div>
                                            </div>
                                        </li>
                                        <li className="media  border-top p-2">
                                            <img className="img-fluid rounded-circle"
                                                 style={{width: '40px', height: '40px'}}
                                                 src="http://www.leapcms.com/images/100pixels1.gif"/>
                                            <div className="media-body pl-2">
                                                <div className="font-size-sm text-muted">john carlo franco</div>
                                                <div className="font-size-xs font-weight-light">Just see the admin!
                                                </div>
                                                <div className="font-size-xxs font-weight-light">9:30 AM</div>
                                            </div>
                                        </li>
                                        <li className="media  border-top p-2">
                                            <img className="img-fluid rounded-circle"
                                                 style={{width: '40px', height: '40px'}}
                                                 src="http://www.leapcms.com/images/100pixels1.gif"/>
                                            <div className="media-body pl-2">
                                                <div className="font-size-sm text-muted">john carlo franco</div>
                                                <div className="font-size-xs font-weight-light">Just see the admin!
                                                </div>
                                                <div className="font-size-xxs font-weight-light">9:30 AM</div>
                                            </div>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </li>

                    <li className="nav-item mr-4">
                        <div className="dropdown">
                            <a className="nav-link text-white font-size-xs font-weight-bold"
                               data-toggle="dropdown" href="#">
                                <div className="media align-items-center">
                                    <img alt="header profile" style={{'height': '40px', 'width': '40px'}}
                                         className="img-fluid rounded-circle" src={pictureUrl}/>
                                    <div className="dropdowntoggle media-body pl-2 text-nowrap">
                                        {first_name}
                                    </div>
                                </div>
                            </a>

                            <ul className="dropdown-menu font-size-xs">
                                <li>
                                    <Link to="/account" className="dropdown-item">
                                        <div className="row text-muted">
                                            <div className="col-2 text-center p-0">
                                                <i className="fa fa-user"/>
                                            </div>
                                            <div className="col-9 p-0">
                                                Profile
                                            </div>
                                        </div>
                                    </Link>
                                </li>
                                <li>
                                    <Link to="/account/wishlist" className="dropdown-item">
                                        <div className="row text-muted">
                                            <div className="col-2 text-center p-0">
                                                <i className="fa fa-heart"/>
                                            </div>
                                            <div className="col-9 p-0">
                                                Wishlist
                                            </div>
                                        </div>
                                    </Link>
                                </li>
                                <li>
                                    <Link to="/account/setting" className="dropdown-item">
                                        <div className="row text-muted">
                                            <div className="col-2 text-center p-0">
                                                <i className="fa fa-cog"/>
                                            </div>
                                            <div className="col-9 p-0">
                                                Settings
                                            </div>
                                        </div>
                                    </Link>
                                </li>

                                <li>
                                    <Link to="/account/help" className="dropdown-item">
                                        <div className="row text-muted">
                                            <div className="col-2 text-center p-0">
                                                <i className="fa fa-question"/>
                                            </div>
                                            <div className="col-9 p-0">
                                                Help
                                            </div>
                                        </div>
                                    </Link>
                                </li>
                                <div role="separator" className="dropdown-divider"></div>
                                <li>
                                    <button onClick={this.logOut} className="dropdown-item">
                                        <div className="row text-muted">
                                            <div className="col-2 text-center p-0">
                                                <i className="fas fa-sign-out-alt"/>
                                            </div>
                                            <div className="col-9 p-0">
                                                Log out
                                            </div>
                                        </div>
                                    </button>
                                </li>
                            </ul>
                        </div>
                    </li>
                </Fragment>
            )
        } else {
            return (
                <Fragment>

                    <li className="list-inline-item">

                        <Link to="/signin"
                              className="nav-link text-white font-size-xs font-weight-bold">
                            <BadgeNotify
                                icon="fas fa-user fa-sm"
                                text="&nbsp;Login"
                            />
                        </Link>
                    </li>
                </Fragment>
            );
        }
    }

    render() {
        let user = Parse.User.current();
        let {carts} = this.props;
        return (
            <header className="navbar navbar-light navbar-expand bg-primary sticky-top d-sm-none d-sm-block">
                <div className="container">
                    <Link to="/" className="navbar-brand">
                        <img className="" height="40" alt="header logo" src="/logo.png"/>
                    </Link>

                    <form className="input-group mr-auto ml-5">
                        <input className="form-control form-control-sm rounded-0 col-5" type="text"
                               placeholder="Search for product, brands and shops" id="search_keyword"/>
                        <span className="input-group-btn">
                                    <button className="btn btn-sm btn-outline-light rounded-0" type="submit"><i
                                        className="fa fa-search"
                                        aria-hidden="true"></i>&nbsp;Search</button>
                                </span>
                    </form>

                    <ul className="navbar-nav align-items-center">
                        <li className="nav-item">
                            <Link to="/account/mywallet"
                                  className="nav-link text-white font-size-xs font-weight-bold">
                                <BadgeNotify
                                    icon="fas fa-wallet fa-sm"
                                    text={this.walletTotal()}
                                />
                            </Link>
                        </li>
                        <li className="nav-item">
                            <Link to="/cart" className="nav-link text-white font-size-xs font-weight-bold">
                                <BadgeNotify
                                    icon="fa fa-shopping-cart fa-sm"
                                    badge='badge-danger'
                                    text='&nbsp;&nbsp;Cart'
                                    value={carts.length > 0 ? carts.length : null}/>
                            </Link>
                        </li>
                        {
                            this.renderUser()
                        }

                    </ul>
                </div>
            </header>
        );
    }

}

export default WithContext(Header);
