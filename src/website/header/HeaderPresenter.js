import Parse from 'parse';

class HeaderPresenter {
    constructor (view){
        this.view=view;
    }
    logoutClick() {
        Parse.User.logOut().then(() => {
            window.location.reload();
        });
    }
}
export default HeaderPresenter;