
export function getIcon(online) {
    if (online.className === "ChatGroup") {
        let file = online.get('icon');
        return file._url;
    } else if (online.className === "_User") {
        let file = online.get('profile');
        if (file) {
            return file._url;
        } else {
            return require('./ic_face_black.png');
        }
    }
}

export function getName(online) {
    if (online.className === "ChatGroup") {

    } else if (online.className === "_User") {
        return online.getUsername();
    }
}