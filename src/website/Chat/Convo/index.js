import React, {Component} from 'react';
import Parse from "parse";

import * as util from '../util.js';

class Convo extends Component {
    constructor() {
        super();
        this.state = {
            chats: [],
            isShow: true
        };
    }

    componentDidMount() {
        this.getChat();
    }

    getChat() {
        let convo = this.props.children;

        //don get chat dont have conversation
        if(convo.dirty()){
            return true;
        }

        var query = new Parse.Query('Chat');
        query.equalTo("convo", convo);
        // query.include('from');
        query.find().then((chats) => {
            this.setState({chats: chats});
        }).catch((err) => {
            alert(err);
        });
        // live query for chat
        let subscribe = query.subscribe();
        subscribe.on('create', (chat) => {
           this.addNewChat(chat);
        });
    }

    sendChat(e) {
        e.preventDefault();
        let convo = this.props.children;
        if(convo.dirty()){
            convo.save().then(()=>{
                this.getChat();
            });
        }

        //get data from form
        var message = e.target.chat_message.value;
        var Chat = Parse.Object.extend("Chat");
        var chat = new Chat();
        //get current user
        var user = Parse.User.current();
        //create chat
        chat.set("message", message);
        chat.set("from", user);
        chat.set("convo", convo);
        chat.save().then((chat) => {

        });
        //empty message
        e.target.chat_message.value = "";
        // this.addNewChat(chat);
    }

    addNewChat(chat) {
        let newChats = this.state.chats;
        newChats.push(chat);
        this.setState({chats: newChats});
    }

    chatMe(chat) {
        return (
            <div key={chat.id} className="row mt-2">
                <div className="col-md-12 py-0 pr-2">
                    <div className="rounded float-right bg-primary">
                        <div className="bg-opacity p-2 font-size-sm text-white" style={{whiteSpace:'pre-wrap'}}>{chat.get('message')}</div>
                    </div>
                </div>
            </div>
        );
    }

    chatOther(chat) {
        let icon = util.getIcon(chat.get('from'));
        return (
            <div key={chat.id} className="row mt-2">
                <div className="col-md-2 py-1 pl-2 pr-0">
                    <img src={icon} alt={icon} className="img-fluid rounded-circle" style={{'height': '2rem'}}/>
                </div>
                <div className="col-md-10 pt-1 pb-1 pl-0">
                    <div className="rounded float-left bg-light">
                        <div className="p-2 font-size-sm text-dark" style={{whiteSpace:'pre-wrap'}}>{chat.get('message')}</div>
                    </div>
                </div>
            </div>
        );
    }

    renderChat() {
        return this.state.chats.map((chat) => {
            if (chat.get('from').isCurrent()) {
                return this.chatMe(chat);
            } else {
                return this.chatOther(chat);
            }
        });
    }

    render() {
        let {margin} = this.props;
        let {onClose} = this.props;
        let {to} = this.props;
        let convo = this.props.children;

        let name = util.getName(to);

        return (
            <div className="chat-box box-shadow bg-white rounded-top" style={{right: margin + "px"}}>
                <div className="bg-primary p-2 text-white rounded-top font-size-sm" style={{cursor: 'pointer'}}
                     onClick={() => this.setState({isShow: !this.state.isShow})}>
                    <span className="dot bg-success"/>&nbsp;{name}
                    <button type="button" onClick={() => onClose(convo)}
                            className="close btn btn-link"><span>&times;</span>
                    </button>
                </div>
                {
                    this.state.isShow &&
                    <div className="msg-wrap">
                        <div className="container msg-body">
                            <div>
                                {this.renderChat()}
                            </div>
                        </div>
                        <form onSubmit={this.sendChat.bind(this)} className="p-2">
                            <input id="chat_message" className="form-control rounded-0 font-size-sm"
                                   placeholder="Write your message here..."></input>
                        </form>
                    </div>
                }
            </div>
        );
    }

}

export default Convo;
