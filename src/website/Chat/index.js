import React, {Component} from 'react';
import './style.css';
import Parse from 'parse';
import Convo from "./Convo";

import * as util from './util.js';

class Chat extends Component {
    constructor() {
        super();
        this.state = {
            convos: [],
            onlines: [],
            isShow: false
        };
    }

    componentDidMount() {
        var user = Parse.User.current();
        if (user) {
            this.getOnline();
        }
    }

    getOnline() {
        var query = new Parse.Query(Parse.User);
        //query.equalTo("username", "admin@yahoo.com");
        query.find().then((users) => {
            this.addNewOnline(users);
        }).catch((err) => {
            alert(err);
        });
    }

    addNewOnline(online) {
        var user = Parse.User.current();
        let newOnlines = this.state.onlines;

        if (online instanceof Array) {
            //remove same account
            let filterOnline = online.filter(obj1 => obj1.id !== user.id);
            newOnlines.push(...filterOnline);
        } else {
            newOnlines.push(online);
        }
        this.setState({online: newOnlines});
    }

    /*...............all about render views...............*/
    addConvo(convo, to) {
        let newConvos = this.state.convos;
        //create object
        let convoObject = {};
        convoObject["to"] = to;
        convoObject["convo"] = convo;

        //add to state
        var add = (conObj) => {
            //add new object to convos array
            newConvos.push(conObj);
            //set state
            this.setState({convos: newConvos});
        }

        if (convo.dirty()) {
            add(convoObject);
        } else if (newConvos.findIndex(convoObj => convoObj["convo"].id === convo.id) < 0) {
            add(convoObject);
        }

    }

    createNewConvo(users, to) {
        var ChatConvo = Parse.Object.extend('ChatConvo');
        var convo = new ChatConvo();
        convo.set("users", users);
        this.addConvo(convo, to);
    }

    chat(to) {
        let fromUser = Parse.User.current();
        var users = [to.id, fromUser.id];
        //find conversation
        let query = new Parse.Query('ChatConvo');
        query.containsAll("users", users);
        query.first().then((convo) => {
            //they have existing conversation
            if (convo) {
                this.addConvo(convo, to);
            } else {
                //new conversation
                this.createNewConvo(users, to);
            }
        }).catch((err) => {
            console.log(err);
        });
    }

    /*render online*/
    renderOnline() {
        return this.state.onlines.map((online) => {
            let icon = util.getIcon(online);
            let name = util.getName(online);

            return (
                <div key={online.id} onClick={() => this.chat(online)}
                     className="chat-user d-flex justify-content-between align-items-center p-2">
                    <div><img src={icon} alt={icon} className="img-fluid rounded-circle"/>&nbsp;{name}</div>
                    <span className="dot bg-success"/>
                </div>
            );
        });
    }

    onClose(convo) {
        let newConvos = this.state.convos;
        let index = newConvos.findIndex(convoObj => convoObj["convo"].id === convo.id);
        newConvos.splice(index, 1);
        this.setState({convos: newConvos});
    }

    render() {
        return (
            <div>
                {
                    this.state.convos.map((convoObject, index) => {
                        let convo = convoObject["convo"];
                        let to = convoObject["to"];
                        index += 1;
                        let delta = 320;
                        let margin = delta * index;
                        return (
                            <Convo key={convo.id} to={to} onClose={this.onClose.bind(this)}
                                   margin={margin}>{convo}</Convo>
                        )
                    })
                }
                <div className="chat-box box-shadow bg-white rounded-top font-size-sm">
                    <div style={{cursor: 'pointer'}} onClick={() => this.setState({isShow: !this.state.isShow})}
                         className="bg-secondary rounded-top p-2 text-white">Chat
                    </div>
                    {this.state.isShow &&
                    <div className="chat-body">
                        {this.renderOnline()}
                    </div>
                    }
                </div>
            </div>
        );
    }

}

export default Chat;
