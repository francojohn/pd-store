import {getShippingFee} from "../../utils";

class OrderSummaryPresenter {
    constructor(view) {
        this.view = view;
    }
    componentDidMount() {
        this.calculateShippingFee();
    }
    componentWillReceiveProps(){
        this.calculateShippingFee();
    }

    calculateShippingFee(){
        let address = this.view.getAddress();
        if (address) {
            let state = address.get("state");
            let region = address.get("region");
            let city = address.get("city");
            let shipping_fee = getShippingFee(state, region, city);
            this.view.setShippingFee(shipping_fee);
        }
    }

}

export default OrderSummaryPresenter