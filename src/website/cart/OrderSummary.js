import React, {Component} from "react";
import {WithContext} from "../../Context";
import OrderSummaryPresenter from "./OrderSummaryPresenter";
import {getCartTotal, getPrice} from "../../utils";

class OrderSummary extends Component {
    constructor() {
        super();
        this.presenter = new OrderSummaryPresenter(this);
        this.state = {shipping_total: 0};
    }

    componentDidMount() {
        this.presenter.componentDidMount();
    }

    componentWillReceiveProps() {
        this.presenter.componentWillReceiveProps();
    }

    setShippingFee(shipping_fee) {
        this.setState({shipping_total: shipping_fee});
    }

    getAddress() {
        let {address} = this.props;
        return address;
    }

    render() {
        let {carts} = this.props;
        let {shipping_total} = this.state;
        let item_total = getCartTotal(carts);
        let {address} = this.props;
        return (
            <div className="card">
                <div className="card-header">
                    Order Summary
                </div>
                <div className="card-body">
                    {
                        !address &&
                        <p className="font-size-sm mb-3 text-secondary">Shipping fee depends on your location</p>
                    }

                    <div className="dropdown-divider"></div>
                    {
                        address &&
                        <p className="font-size-xs font-weight-light mb-3">
                            Shipping Total: <span className="font-weight-normal pull-right">&#8369;&nbsp; {shipping_total}</span>
                        </p>
                    }
                    <p className="font-size-xs font-weight-light mb-3">Item Total: <span
                        className="font-weight-normal pull-right">&#8369;&nbsp; {item_total}</span>
                    </p>
                    <div className="input-group mb-3">
                        <input type="text" className="form-control form-control-sm" placeholder="Voucher Code"/>
                        <div className="input-group-append">
                            <button className="btn btn-sm rounded-0 btn-secondary text-white" type="button">
                                <i className="fas fa-ticket-alt"></i>
                            </button>
                        </div>
                    </div>
                    <div className="dropdown-divider"></div>
                    <p className="font-size-xs"><b>Order Total:</b> <span
                        className="font-weight-normal pull-right"><b></b>&#8369;&nbsp; {item_total + shipping_total}</span>
                    </p>
                </div>
            </div>
        );
    }
}

export default WithContext(OrderSummary);
