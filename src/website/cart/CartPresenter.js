export function getDeliveryDate(currentDate) {
    let fromIncrementDate = 2;
    let toIncrementDate = 2;
    //get current date
    let fromDate = new Date(currentDate);
    //increment from date
    fromDate.setDate(fromDate.getDate() + fromIncrementDate);
    //assign from date to
    let toDate = new Date(fromDate);
    //increment to date
    toDate.setDate(toDate.getDate() + toIncrementDate);
    //set date
    let Fdate = fromDate.getDate();
    let Tdate = toDate.getDate();
    //set month
    let fMonth = fromDate.toLocaleString("en-us", { month: "short"});
    let tMonth = toDate.toLocaleString("en-us", { month: "short"});
    //set year
    let tYear = toDate.getFullYear();
    let tMonthlabel = fMonth === tMonth ? "" : ` ${tMonth}`;
    let result = `${fMonth} ${Fdate} to${tMonthlabel} ${Tdate} ${tYear}`;
    return result.trim();
}

class CartPresenter {
    constructor(view) {
        this.view = view;
    }

    componentDidMount() {

    }

}

export default CartPresenter