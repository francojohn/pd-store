import {getDeliveryDate} from "./CartPresenter";

describe("test date", () => {
    test("test expected date deliver",()=>{
        let currentDate = new Date(2018, 10, 12);
        let expectedResult = "Nov 14 to 16 2018";
        let result = getDeliveryDate(currentDate);
        expect(result).toBe(expectedResult);
    });
    test("test expected date not same month deliver",()=>{
        let currentDate = new Date(2018, 10, 28);
        let expectedResult = "Nov 30 to Dec 2 2018";
        let result = getDeliveryDate(currentDate);
        expect(result).toBe(expectedResult);
    });
});