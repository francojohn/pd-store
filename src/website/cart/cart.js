import React, {Component} from "react";
import {Link} from 'react-router-dom';
import './style.css';
import {WithContext} from "../../Context";
import CartPresenter, {getDeliveryDate} from "./CartPresenter";
import CartContent from "./CartContent";
import OrderSummary from "./OrderSummary";

class Cart extends Component {
    constructor() {
        super();
        this.presenter = new CartPresenter(this);
    }

    componentDidMount(){
        window.scrollTo(0, 0);
    }
    update = () => {
        this.forceUpdate();
    }

    render() {
        let {carts} = this.props;
        return (
            <div className="container">
                <div className="card rounded-0 py-5 px-2 my-5">
                    <div className="card-body">
                        <div className="mt-3">
                            <div className="container mt-3">
                                <div className="row">
                                    <div className="col-sm-9 p-2">
                                        <Link to="/"
                                              className="text-secondary d-block mb-5 font-weight-bold font-size-xs">
                                            <i className="fa fa-angle-left"></i>&nbsp;&nbsp;CONTINUE SHOPPING
                                        </Link>
                                        <CartContent update={this.update}/>
                                    </div>
                                    <div className="col-sm-3 p-2">
                                        {
                                            carts.length > 0 &&
                                            <Link to="/checkout"
                                                  className="btn btn-primary rounded text-white px-5 my-3 w-100 d-block text-uppercase">
                                                <small>PROCEED TO CHECK OUT</small>
                                            </Link>
                                        }
                                        <OrderSummary/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default WithContext(Cart);