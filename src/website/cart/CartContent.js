import React, {Component} from "react";
import ItemProduct from "../ItemProduct";
import {WithContext} from "../../Context";
import {getDeliveryDate} from "./CartPresenter";
import CartContentPresenter from "./CartContentPresenter";
import {getPrice} from "../../utils";

class CartContent extends Component {

    constructor() {
        super();
        this.presenter = new CartContentPresenter(this);
    }

    removeClick(cart) {
        this.presenter.removeClick(cart);
    }

    setCarts(carts) {
        let {setCarts} = this.props;
        if (setCarts) {
            setCarts(carts);
        }
    }

    getCarts() {
        let {carts} = this.props;
        return carts;
    }

    quantityDecreaseClick(cart) {
        this.presenter.quantityDecreaseClick(cart);
    }

    quantityIncreaseClick(cart) {
        this.presenter.quantityIncreaseClick(cart);
    }

    update() {
        let {update} = this.props;
        if (update) update();
    }

    render() {
        let {carts} = this.props;
        let expectedDelivery = getDeliveryDate(new Date());
        // let shipping_total = 40;
        return (
            <div className="row">
                <div className="col-sm-12">
                    <div className="py-4">
                        <h5>Preferred Delivery Option</h5>
                        <div className="btn-group btn-check btn-group-toggle"
                             data-toggle="buttons">
                            <label className="btn btn-sm mr-2 text-capitalize active rounded-0 py-3 px-4 mr-3 mt-3 custom">
                                <input type="radio" name="options" checked/>
                                <div className="pick">
                                    <div className="content text-left">
                                        <h7 className="text-secondary"><b>Standard</b>
                                        </h7>
                                        <p className="text-muted m-0">
                                            Get by {expectedDelivery}
                                        </p>
                                        {/*<b className="mt-1 d-block">&#8369;&nbsp; {shipping_total}</b>*/}
                                    </div>
                                </div>
                            </label>
                        </div>
                    </div>
                    <div className="table-responsive">
                        <table className="table" style={{width: "700px"}}>
                            <thead>
                                <tr>
                                    <th scope="col" className="border-0"></th>
                                    <th scope="col" className="border-0">ITEM(S)</th>
                                    <th scope="col" className="border-0">PRICE</th>
                                    <th scope="col" className="border-0">QUANTITY</th>
                                    <th scope="col" className="border-0">TOTAL</th>
                                    <th scope="col" className="border-0">REMOVE</th>
                                </tr>
                            </thead>
                            <tbody>
                            {
                                carts.map((cart) => {
                                    let product = cart.get("product");
                                    let product_variant = cart.get("product_variant");
                                    let price = getPrice(product, product_variant);
                                    let name = product.get("name");
                                    let quantity = cart.get("quantity");
                                    let total = price * quantity;
                                    let variant = {};
                                    if (product_variant) {
                                        variant = product_variant.get("variant");
                                    }
                                    return (
                                        <tr className="border-0">
                                            <td scope="row" className="border-0"
                                                style={{width: '280px'}}>
                                                <ItemProduct no_footer={true}>{product}</ItemProduct>
                                            </td>
                                            <td scope="row"
                                                className="border-0 pt-4 text-primary"
                                                style={{width: '350px'}}>
                                                <h6 className="m-0">{name}</h6>
                                                {
                                                    Object.keys(variant).map((key) => {
                                                        let value = variant[key];
                                                        return (
                                                            <label
                                                                className="text-muted font-size-xs">{`${key} ${value}`}</label>
                                                        )
                                                    })
                                                }
                                            </td>
                                            <td className="border-0 pt-4"
                                                style={{width: '220px'}}>
                                                <label>&#8369; {price}</label>

                                            </td>
                                            <td className="border-0 pt-4 text-center"
                                                style={{width: '360px'}}>
                                                <div className="input-group input-group-sm">
                                                    <div className="input-group-append">
                                                        <div className="col-auto">
                                                            <div className="input-group mb-2">
                                                                <div className="input-group-prepend">
                                                                    <div className="">
                                                                        <button
                                                                            onClick={() => this.quantityDecreaseClick(cart)}
                                                                            className="btn btn-light"
                                                                            type="button">
                                                                            -
                                                                        </button>
                                                                    </div>
                                                                </div>
                                                                <input type="text" className="form-control ml-1 mr-1"
                                                                       type="number" value={quantity}
                                                                       style={{width: '60px'}}/>
                                                                <div className="input-group-prepend">
                                                                    <div className="">
                                                                        <button
                                                                            onClick={() => this.quantityIncreaseClick(cart)}
                                                                            className="btn btn-light"
                                                                            type="button">
                                                                            +
                                                                        </button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                            <td className="border-0 pt-4"
                                                style={{width: '220px'}}>
                                                <b>&#8369;&nbsp; {total}</b></td>
                                            <td className="border-0 pt-4 text-center"
                                                style={{width: '40px'}}>
                                                <button
                                                    onClick={() => this.removeClick(cart)}
                                                    className="btn btn-sm btn-link">
                                                    <i className="fa fa-trash"
                                                       aria-hidden="true"></i>
                                                </button>
                                            </td>
                                        </tr>
                                    )
                                })
                            }
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        );
    }
}

export default WithContext(CartContent);
