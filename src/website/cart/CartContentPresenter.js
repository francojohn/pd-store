class CartContentPresenter {
    constructor(view){
        this.view = view;
    }
    componentDidMount(){
       
    }
    async removeClick(cart) {
        let carts = this.view.getCarts();
        await cart.destroy();
        let index = carts.indexOf(cart);
        carts.splice(index, 1);
        this.view.setCarts(carts);
    }

    async quantityDecreaseClick(cart) {
        let quantity = cart.get("quantity");
        if (quantity > 1) {
            cart.increment("quantity", -1);
            await cart.save();
            this.view.update();
        }
    }

    async quantityIncreaseClick(cart) {
        cart.increment("quantity", 1);
        await cart.save();
        this.view.update();
    }
}

export default CartContentPresenter