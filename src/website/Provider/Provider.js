import React from "react";
import PropTypes from 'prop-types';

import Parse from "parse";


import Context from './Context';
import {menu} from "../panel/SideBar";

class Provider extends React.Component {

    static childContextTypes = {
        state: PropTypes.object,
        removeCartsAll: PropTypes.func,
        removeCart: PropTypes.func
    }

    getChildContext() {
        return {
            state: this.state,
            removeCartsAll: this.removeCartsAll,
            removeCart: this.removeCart,
        };
    }

    removeCartsAll = () => {
        Parse.Object.destroyAll(this.state.carts).then(() => {
            this.setState({carts: []});
        }).catch((err) => {
            alert(err);
        });
    }
    state = {
        carts: [],
        sideBar: [],
    }

    componentDidMount() {
        this.fetchCart();
        this.setSideBar();
    }

    setSideBar() {

        let historys = [
            new menu('/profile/depositehistory', 'fa fa-donate', 'Deposit History'),
            new menu('/profile/withdrawhistory', 'fa fa-wallet', 'Withdraw History'),
        ]

        let sectionTwo = [
            new menu('/profile/account', 'fa fa-user', 'My Account'),
            new menu(historys, 'fa fa-history', 'Account History'),
            new menu('/profile/settings', 'fa fa-cog', 'Settings'),
        ];


        let sectionOne = [
            new menu('/profile/myorder', 'fas fa-box', 'My Order'),
            new menu('/profile/mywallet', 'fas fa-wallet', 'My Wallet'),
            new menu('/profile/myitem', 'fa fa-calendar-check', 'My Item'),
            new menu('/profile/mywishlist', 'fas fa-heart', 'My Wishlist'),
        ];


        let menus = [
            sectionOne,
            sectionTwo,
        ];

        this.setState({sideBar: menus});
    }

    fetchCart() {
        let user = Parse.User.current();
        if (user) {
            let query = new Parse.Query('Cart');
            query.equalTo('user', user);
            query.include('product.thumbnail');
            query.find().then((carts) => {
                this.setState({carts: carts});
            });
        }
    }


    removeCart(cart) {
        cart.destroy().then(() => {
            let carts = this.state.carts;
            let index = carts.indexOf(cart);
            carts.splice(index, 1);
            // this.setState({carts: carts});
            window.location.reload();
            // this.forceUpdate();
        });
    }

    addCart = (cart) => {
        let carts = this.state.carts;
        let product = cart.get('product');

        let index = carts.findIndex(c => c.get('product').id === product.id);

        if (index < 0) {//if not exist
            cart.save().then((obj) => {
                carts.push(obj);
                this.setState({carts: carts});
            });
        } else {
            let c = carts[index];
            let quantity_new = cart.get('quantity');
            let quantity_old = c.get('quantity');
            c.set('quantity', quantity_old + quantity_new);
            c.save().then((obj) => {
                // carts.push(obj);
                // this.setState({carts:carts});
            });
        }

    }

    render() {
        return (
            <Context.Provider value={{
                state: this.state,
                addCart: this.addCart,
                removeCart: this.removeCart
            }}>
                {this.props.children}
            </Context.Provider>
        );
    }
}

export default Provider;