import React, {Component} from "react";
import {Link} from 'react-router-dom';
import CartContent from "../cart/CartContent";
import Parse from "parse";
import OrderSummary from "../cart/OrderSummary";
import Deliver from "../delivery-form/delivery";
import CheckOutPresenter from "./CheckOutPresenter";
import {WithContext} from "../../Context";
import {getPrice} from "../../utils";

class CheckOut extends Component {
    constructor() {
        super();
        this.presenter = new CheckOutPresenter(this);
        this.state = {
            isShowAddressForm: false,
            regions: [],
            states: [],
            cities: [],
            barangays: [],
        };
    }

    componentDidMount() {
        window.scrollTo(0, 0);
        this.presenter.componentDidMount();
    }

    getAddress() {
        let {address} = this.props;
        return address;
    }

    /*geo location*/
    setStates(states) {
        this.setState({states: states});
    }

    setRegions(regions) {
        this.setState({regions: regions});
    }

    setCities(cities) {
        this.setState({cities: cities});
    }

    setBarangays(barangays) {
        this.setState({barangays: barangays});
    }

    showAddressForm() {
        this.setState({isShowAddressForm: true});
    }

    hideAddressForm() {
        this.setState({isShowAddressForm: false});
    }

    update = () => {
        this.forceUpdate();
    }

    setAddress(address) {
        let {setAddress} = this.props;
        if (setAddress) {
            setAddress(address);
        }
    }

    formSubmit = (e) => {
        e.preventDefault();
        this.presenter.formSubmit();
    }

    getFullName() {
        return document.getElementById("address_full_name").value;
    }

    getMobileNumber() {
        return document.getElementById("address_mobile_number").value;
    }

    getNotes() {
        return document.getElementById("address_notes").value;
    }

    getHouseNumberStreetName() {
        return document.getElementById("address_house_number_street_name").value;
    }

    getState() {
        let selectedIndex = document.getElementById("address_select_state").selectedIndex;
        let {states} = this.state;
        return states[selectedIndex - 1];
    }

    getRegion() {
        let selectedIndex = document.getElementById("address_select_region").selectedIndex;
        let {regions} = this.state;
        return regions[selectedIndex - 1];
    }

    getCity() {
        let selectedIndex = document.getElementById("address_select_city").selectedIndex;
        let {cities} = this.state;
        return cities[selectedIndex - 1];
    }

    getBarangay() {
        // let selectedIndex = document.getElementById("address_select_barangay").selectedIndex;
        // let {barangays} = this.state;
        let value = document.getElementById("address_select_barangay").value;
        let _barangay = {name:value};
        // return barangays[selectedIndex - 1];
        return _barangay;
    }

    showProgress() {
        let button = document.getElementById("address_btn_save");
        button.disabled = true;
        button.classList.add('bg-progress');
        button.innerHTML = "Please Wait..";
    }

    hideProgress() {
        let button = document.getElementById("address_btn_save");
        button.disabled = false;
        button.classList.remove('bg-progress');
        button.innerHTML = "SAVE";
    }

    editInfoClick = () => {
        this.presenter.editInfoClick();
    }
    cancelClick = () => {
        this.presenter.cancelClick();
    }
    placeOrderClick = () => {
        this.presenter.placeOrderClick();
    }

    getShippingTotal() {
        return 40;
    }

    getCarts() {
        let {carts} = this.props;
        return carts;
    }

    setOrder(order) {
        let {setOrder} = this.props;
        if (setOrder) {
            setOrder(order);
        }
    }

    gotoPayment() {
        this.props.history.push('/payment');
    }

    gotoCart() {
        this.props.history.push('/cart');
    }

    statesChange = (e) => {
        let value = e.target.value;
        this.presenter.statesChange(value);
    }

    regionsChange = (e) => {
        let value = e.target.value;
        this.presenter.regionsChange(value);
    }

    citiesChange = (e) => {
        let value = e.target.value;
        this.presenter.citiesChange(value);
    }

    render() {
        let user = Parse.User.current();
        let {isShowAddressForm} = this.state;
        let {address} = this.props;
        let {states, regions, cities, barangays} = this.state;
        let full_name;
        let complete_address;
        let mobile_number;
        let notes;
        let _state = {name:""};
        let _region = {name:""};
        let _city = {name:""};
        let _barangay = {name:""};
        let house_number_street_name;
        if (address) {
            full_name = address.get("full_name");
            complete_address = address.get("complete_address");
            mobile_number = address.get("mobile_number");
            notes = address.get("notes");
            _state = address.get("state");
            _region = address.get("region");
            _city = address.get("city");
            _barangay = address.get("barangay");
            house_number_street_name = address.get("house_number_street_name");
        } else {
            full_name = user.get("name");
        }

        return (
            <div className="ship">
                <div className="container">
                    <div className="card rounded-0 py-5 px-2 my-5">
                        <div className="card-body">
                            <div className="mt-3">
                                <div className="container mt-3">
                                    <div className="row">
                                        <div className="col-sm-9 p-2">
                                            <Link to="/cart"
                                                  className="text-secondary d-block mb-5 font-weight-bold font-size-xs">
                                                <i className="fa fa-angle-left"></i>&nbsp;&nbsp;BACK
                                            </Link>
                                            {
                                                isShowAddressForm &&
                                                <div className="deliver-form card mb-5">
                                                    <form onSubmit={this.formSubmit} className="card-body">
                                                        <h5 className="mb-4">Delivery Information</h5>
                                                        <div className="row">
                                                            <div className="col-sm-6">
                                                                <div className="form-group">
                                                                    <label className="font-size-xs mb-1 pl-2">Full
                                                                        Name</label>
                                                                    <input type="text" className="form-control"
                                                                           placeholder="Full Name"
                                                                           id="address_full_name" required
                                                                           defaultValue={full_name}/>
                                                                </div>
                                                                <div className="form-group">
                                                                    <label className="font-size-xs mb-1 pl-2">Mobile
                                                                        Number</label>
                                                                    <input type="text" className="form-control"
                                                                           placeholder="Mobile Number"
                                                                           id="address_mobile_number" required
                                                                           defaultValue={mobile_number}/>
                                                                </div>
                                                                <div className="form-group">
                                                                    <label
                                                                        className="font-size-xs mb-1 pl-2">Notes</label>
                                                                    <textarea type="text" className="form-control"
                                                                              placeholder="Notes For Rider (optional)"
                                                                              id="address_notes" defaultValue={notes}/>
                                                                </div>
                                                            </div>
                                                            <div className="col-sm-6">
                                                                <div className="form-group">
                                                                    <label className="font-size-xs mb-1 pl-2">State /
                                                                        Provice</label>
                                                                    <select className="form-control"
                                                                            id="address_select_state"
                                                                            onChange={this.statesChange} required>
                                                                        <option value="" selected>Choose...</option>
                                                                        {
                                                                            states.map((state) => {
                                                                                let {name, geonameId} = state;
                                                                                return (
                                                                                    <option selected={_state.name === name}
                                                                                        value={geonameId}>{name}</option>
                                                                                )
                                                                            })
                                                                        }
                                                                    </select>
                                                                </div>


                                                                <div className="form-group">
                                                                    <label
                                                                        className="font-size-xs mb-1 pl-2">County /
                                                                        Region</label>
                                                                    <select className="form-control"
                                                                            id="address_select_region"
                                                                            onChange={this.regionsChange} required
                                                                            disabled={regions.length < 1}>

                                                                        {
                                                                            regions.length > 0 &&
                                                                            <option value="" selected>Choose...</option>
                                                                        }
                                                                        {
                                                                            regions.map((region) => {
                                                                                let {name, geonameId} = region;
                                                                                return (
                                                                                    <option value={geonameId}
                                                                                            selected={_region.name === name}>{name}</option>
                                                                                )
                                                                            })
                                                                        }
                                                                    </select>
                                                                </div>

                                                                <div className="form-group">
                                                                    <label className="font-size-xs mb-1 pl-2">City /
                                                                        Municipality</label>
                                                                    <select className="form-control"
                                                                            onChange={this.citiesChange}
                                                                            id="address_select_city" required
                                                                            disabled={cities.length < 1}>
                                                                        {
                                                                            cities.length > 0 &&
                                                                            <option value="" selected>Choose...</option>
                                                                        }
                                                                        {
                                                                            cities.map((city) => {
                                                                                let {name, geonameId} = city;
                                                                                return (
                                                                                    <option value={geonameId}
                                                                                            selected={_city.name === name}>{name}</option>
                                                                                )
                                                                            })
                                                                        }
                                                                    </select>
                                                                </div>

                                                                {/*<div className="form-group">*/}
                                                                    {/*<label*/}
                                                                        {/*className="font-size-xs mb-1 pl-2">Barangay</label>*/}
                                                                    {/*<select className="form-control" required*/}
                                                                            {/*id="address_select_barangay"*/}
                                                                            {/*disabled={barangays.length < 1}>*/}

                                                                        {/*{*/}
                                                                            {/*barangays.length > 0 &&*/}
                                                                            {/*<option value="" selected>Choose...</option>*/}
                                                                        {/*}*/}
                                                                        {/**/}
                                                                        {/*{*/}
                                                                            {/*barangays.map((barangay) => {*/}
                                                                                {/*let {name, geonameId} = barangay;*/}
                                                                                {/*return (*/}
                                                                                    {/*<option value={geonameId}*/}
                                                                                            {/*selected={_barangay.name === name}>{name}</option>*/}
                                                                                {/*)*/}
                                                                            {/*})*/}
                                                                        {/*}*/}
                                                                    {/*</select>*/}
                                                                {/*</div>*/}
                                                                <div className="form-group">
                                                                    <label className="font-size-xs mb-1 pl-2">Barangay</label>
                                                                    <input type="text" className="form-control"
                                                                           id="address_select_barangay"
                                                                           required
                                                                           defaultValue={_barangay.name}/>
                                                                </div>

                                                                <div className="form-group">
                                                                    <label className="font-size-xs mb-1 pl-2">House
                                                                        Number and Street Name / Building and
                                                                        Floor</label>
                                                                    <input type="text" className="form-control"
                                                                           id="address_house_number_street_name"
                                                                           required
                                                                           defaultValue={house_number_street_name}/>
                                                                </div>


                                                                <div className="text-right">
                                                                    <button type="submit"
                                                                            className="btn btn-sm btn-primary rounded text-white px-5 my-3 text-uppercase"
                                                                            id="address_btn_save">SAVE
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            }

                                            <div className="row">
                                                <div className="col-sm-12">
                                                    <CartContent update={this.update}/>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-sm-3 p-2">
                                            {
                                                address &&
                                                <button onClick={this.placeOrderClick}
                                                        className="btn btn-primary rounded text-white px-5 my-3 w-100 d-block text-uppercase">
                                                    <small>PLACE ORDER</small>
                                                </button>
                                            }
                                            {
                                                address &&
                                                <div className="card mb-1">
                                                    <div className="card-header">
                                                        Shipping & Billing
                                                    </div>
                                                    <div className="card-body">
                                                        <label className="font-size-xs float-left">Ship to</label>
                                                        {
                                                            !isShowAddressForm &&
                                                            <button onClick={this.editInfoClick}
                                                                    className="btn btn-link p-0 font-size-xs text-secondary float-right">
                                                                Edit Info
                                                            </button>
                                                        }
                                                        {
                                                            isShowAddressForm &&
                                                            <button onClick={this.cancelClick}
                                                                    className="btn btn-link p-0 font-size-xs text-secondary float-right">
                                                                Cancel Edit
                                                            </button>
                                                        }

                                                        <div className="clearfix"></div>
                                                        <h6 className="font-size-sm">{full_name}</h6>
                                                        <p className="font-size-sm mb-1">{complete_address}</p>
                                                        <p className="font-size-sm mb-1">{mobile_number}</p>
                                                    </div>
                                                </div>
                                            }
                                            <OrderSummary/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default WithContext(CheckOut);
