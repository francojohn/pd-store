import Parse from "parse";
import {regions} from "./region";
import {getCartTotal, getPrice, getShippingFee} from "../../utils";


class CheckOutPresenter {
    constructor(view) {
        this.view = view;
    }

    componentDidMount() {
        let carts = this.view.getCarts();
        if (carts.length < 1) {
            this.view.gotoCart();
        }
        this.view.setStates(regions);

        let address = this.view.getAddress();
        if (address) {
            this.view.setAddress(address);
        } else {
            this.view.showAddressForm();
        }
    }

    async citiesChange(geonameId) {
        this.view.setBarangays([]);
        let response = await this.getGeonamesById(geonameId);
        let {geonames} = response;
        this.view.setBarangays(geonames);
    }

    async regionsChange(geonameId) {
        this.view.setCities([]);
        this.view.setBarangays([]);
        let response = await this.getGeonamesById(geonameId);
        let {geonames} = response;
        this.view.setCities(geonames);
    }

    async statesChange(geonameId) {
        this.view.setRegions([]);
        this.view.setCities([]);
        this.view.setBarangays([]);
        let response = await this.getGeonamesById(geonameId);
        let {geonames} = response;
        this.view.setRegions(geonames);
    }

    getGeonamesById(geonameId) {
        let url = `https://www.geonames.org/childrenJSON?&geonameId=${geonameId}&username=francojohnc`;
        return fetch(url).then((resp) => resp.json());
    }

    // async fetchAddress() {
    //     try {
    //         let user = Parse.User.current();
    //         let query = new Parse.Query("Address");
    //         query.equalTo("user", user);
    //         query.equalTo("default", true);
    //         let address = await query.first();
    //         if (address) {
    //             this.view.setAddress(address);
    //         } else {
    //             this.view.showAddressForm();
    //         }
    //     }catch (e) {
    //         console.log(e);
    //     }
    // }

    geonamesToParseObject(geoname) {
        let geolocation = new Parse.Object("Geolocation");
        let {adminCode1, lng, geonameId, toponymName, countryId, fcl, population, countryCode} = geoname;
        let {name, fclName, adminCodes1, countryName, fcodeName, adminName1, lat, fcode} = geoname;
        geolocation.set("adminCode1", adminCode1);
        geolocation.set("lng", lng);
        geolocation.set("geonameId", geonameId);
        geolocation.set("toponymName", toponymName);
        geolocation.set("countryId", countryId);
        geolocation.set("fcl", fcl);
        geolocation.set("population", population);
        geolocation.set("countryCode", countryCode);
        geolocation.set("name", name);
        geolocation.set("fclName", fclName);
        geolocation.set("adminCodes1", adminCodes1);
        geolocation.set("countryName", countryName);
        geolocation.set("fcodeName", fcodeName);
        geolocation.set("adminName1", adminName1);
        geolocation.set("lat", lat);
        geolocation.set("fcode", fcode);
        return geolocation;
    }

    async geoNamesRecursive(geonameId) {
        let response = await this.getGeonamesById(geonameId);
        let mydata = {};
        this.responseLoop(response, true, mydata);
    }

    async responseLoop(response, isParent, mydata) {
        console.log(" ");

        // console.log("current " + current);
        // console.log("lastLength " + lastLength);
        let {geonames} = response;
        for (let i = 0; i < geonames.length; i++) {
            let {fcode, geonameId, name} = geonames[i];
            if (isParent) {
                mydata["current"] = i;
                mydata["target"] = geonames.length - 1;
            }
            console.log("name " + name);
            console.log("geonameId " + geonameId);
            console.log("fcode " + fcode);
            if (fcode === "ADM1" || fcode === "ADM2" || fcode === "ADM3") {
                let response = await this.getGeonamesById(geonameId);
                console.log(response);
                this.responseLoop(response, false, mydata);
            } else {
                if (i === geonames.length - 1 && isParent.current === isParent.target) {
                    console.log("done with " + name);
                    // console.log("current " + current);
                    // console.log("lastLength " + lastLength);
                }
            }
        }
    }

    async formSubmit() {
        this.view.showProgress();
        let address = this.view.getAddress();
        if (!address) {
            address = new Parse.Object("Address");
        }
        try {
            let user = Parse.User.current();
            let full_name = this.view.getFullName();
            let mobile_number = this.view.getMobileNumber();

            let notes = this.view.getNotes();
            //
            let state = this.view.getState();
            let region = this.view.getRegion();
            let city = this.view.getCity();
            let barangay = this.view.getBarangay();
            if (!barangay) {
                barangay = {name: ""};
            }


            let house_number_street_name = this.view.getHouseNumberStreetName();
            let complete_address = `${house_number_street_name} ${barangay.name} ${city.name} ${region.name} ${state.name}`;


            address.set("full_name", full_name);
            address.set("mobile_number", mobile_number);
            address.set("complete_address", complete_address);
            address.set("notes", notes);
            address.set("user", user);
            address.set("address_name", "home");
            address.set("state", state);
            address.set("region", region);
            address.set("city", city);
            address.set("barangay", barangay);
            address.set("house_number_street_name", house_number_street_name);
            address.set("default", true);
            await address.save();
            this.view.setAddress(address);
            this.view.hideProgress();
            this.view.hideAddressForm();
        } catch (e) {
            alert(e);
            this.view.hideProgress();
        }
    }

    editInfoClick() {
        let address = this.view.getAddress();
        if (address) {
            let state;
            let region;
            let city;
            let house_number_street_name;

            state = address.get("state");
            region = address.get("region");
            city = address.get("city");
            this.statesChange(state.geonameId);
            this.regionsChange(region.geonameId);
            // this.citiesChange(city.geonameId);
        }
        this.view.showAddressForm();
    }

    cancelClick() {
        this.view.hideAddressForm();
    }

    getShippingTotal(address) {
        let state = address.get("state");
        let region = address.get("region");
        let city = address.get("city");
        return getShippingFee(state, region, city);
    }

    placeOrderClick() {
        let user = Parse.User.current();
        let order = new Parse.Object("Order");
        let address = this.view.getAddress();
        let full_name = address.get("full_name");
        let mobile_number = address.get("mobile_number");
        let complete_address = address.get("complete_address");
        let notes = address.get("notes");
        let ship_to = `${full_name}\n${mobile_number}\n${complete_address}`;
        let shipping_total = this.getShippingTotal(address);


        //cart
        let carts = this.view.getCarts();
        let item_total = getCartTotal(carts);
        //set
        order.set("status", "PENDING");
        order.set("ship_to", ship_to);
        order.set("notes", notes);
        order.set("shipping_total", shipping_total);
        order.set("item_total", item_total);
        order.set("order_total", item_total + shipping_total);
        order.set("buyer", user);
        this.view.setOrder(order);
        this.view.gotoPayment();
    }
}

export default CheckOutPresenter