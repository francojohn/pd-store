import React, {Component} from 'react';
import Relation from "../../panel/Browser/BrowserTable/Relation";
import Images from "../Images/index2";


class Editor extends Component {


    onChange(obj, field, value) {
        switch (obj.constructor.name) {
            case 'Object':
                obj[field] = value;
                break;
            case 'ParseObject':
            case 'ParseObjectSubclass':
                obj.set(field, value);
                break;
            default:
        }
    }

    getDefaultValue(obj, field) {
        switch (obj.constructor.name) {
            case 'Object':
                return obj[field];
            case 'ParseObject':
            case 'ParseObjectSubclass':
                return obj.get(field);
            default:
        }
    }

    getDefaultChecked(obj, field) {
        switch (obj.constructor.name) {
            case 'Object':
                return obj[field];
            case 'ParseObject':
            case 'ParseObjectSubclass':
                return obj.get(field);
            default:
        }
    }


    getInput() {
        let obj = this.props.children;
        let {type, field} = this.props;
        switch (type.type) {
            case 'String':
            case 'string':
                return <input onChange={(e) => this.onChange(obj, field, e.target.value)} defaultValue={this.getDefaultValue(obj, field)}  className="form-control form-control-sm" type="text" style={{minWidth:'150px'}}/>;
            case 'Number':
            case 'number':
                return <input onChange={(e) => this.onChange(obj, field, parseFloat(e.target.value))} defaultValue={this.getDefaultValue(obj, field)} className="form-control form-control-sm" type="number" style={{minWidth:'100px'}}/>;
            case 'text':
                return <textarea onChange={(e) => this.onChange(obj, field, e.target.value)} defaultValue={this.getDefaultValue(obj, field)}  rows="3" type="text" className="form-control form-control-sm" style={{minWidth:'150px'}}/>;
            case 'Pointer':
            case 'ReversePointer':
            case 'Array':
            case 'Relation':
                return <Relation {...this.props} column={field} className={type.targetClass}></Relation>;
            case 'Images':
                return <Images {...this.props} column={field} className={type.targetClass}>{obj}</Images>;
            default:
                return <div/>;
        }
    }

    render() {
        return this.getInput();
    }
}

export default Editor;
