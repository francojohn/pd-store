import React, {Component, Fragment} from 'react';
import Parse from "parse";
import ImageCrop from "../ImageCrop";
import * as imgUtil from "../../lib/Img";


class ImageCropper extends Component {
    constructor() {
        super();
        this.state = {
            isEdit: false,
            scale: 1,
            src: null,
            status: "",
        }
    }

    componentDidMount() {
        let {src} = this.props;
        //check if source is from local file or from
        //server url
        if (typeof src === 'string') {
            this.setState({src: src});
        } else {
            imgUtil.resize(src).then((base64) => {
                this.setState({src: base64});
                this.props.onCropped(base64);
            }).catch((err) => {
                console.log(err);
            });
        }
    }


    setStatus(status) {
        this.setState({status: status});

    }

    editSave() {
        this.setState({isEdit: !this.state.isEdit});
        if (this.state.isEdit) {
            const base64 = this.editor.getImageScaledToCanvas().toDataURL();
            this.setState({src: base64});
            this.props.onCropped(base64);
        }
    }

    rangeOnchange(e) {
        const scale = parseFloat(e.target.value)
        this.setState({scale});
    }

    setEditorRef = editor => {
        if (editor) this.editor = editor;
    }

    logCallback() {
        // eslint-disable-next-line
        console.log('callback');
    }

    setImage = img => {
        if (img) this.img = img;
    }

    renderImage(img) {
        let {src} = this.props;

        if (this.state.isEdit) {
            return (
                <ImageCrop
                    style={{width: '217px', height: '217px'}}
                    ref={this.setEditorRef}
                    onLoadSuccess={this.logCallback.bind(this)}
                    image={this.img}
                    crossOrigin='anonymous'
                    width={500}
                    height={500}
                    scale={parseFloat(this.state.scale)}
                />
            );
        } else {
            return (
                <img className="img-fluid"
                     style={{'height': '217px'}}
                     ref={this.setImage}
                     src={this.state.src} id="img"/>
            );
        }
    }

    addImage(parseFile) {
        // let {fields} = this.props;
        // let parentObject = this.props.children;
        // let className = fields.targetClass;
        //
        // let newObject = new Parse.Object(className);
        // newObject.set('file', parseFile);
        // newObject.set('product', parentObject);
        // newObject.save();
    }

    render() {
        return (
            <div className="col-4 mt-1">
                <div className="card">
                    <div className="card-body p-0">
                        <button onClick={this.editSave.bind(this)}
                                className="btn btn-sm">
                            <i className={this.state.isEdit ? 'fa fa-save' : 'fa fa-pencil-alt'}></i>
                        </button>
                        <button onClick={this.props.setThumbnail}
                                className="btn btn-sm btn-light">
                            thumbnail
                        </button>
                        <p>{this.state.status}</p>
                        <div className="card-image">
                            {
                                this.renderImage()
                            }
                        </div>
                    </div>
                    {
                        this.state.isEdit &&
                        <div className="card-footer bg-white p-0">
                            <input
                                name="scale"
                                type="range"
                                min='1'
                                max="2"
                                step="0.01"
                                defaultValue="1"
                                onChange={this.rangeOnchange.bind(this)}
                            />
                        </div>
                    }
                </div>
            </div>
        );
    }
}

class MyImage {
    parseObject = null;
    parseFile = null;
    base64 = null;
    parentObject = null;

    save() {
        if (this.base64) {
            this.crop.setStatus("uploading...");
            let name = this.parseFile._name;
            this.parseFile = new Parse.File(name, {base64: this.base64});
            this.parseFile.save().then((parsefile) => {
                this.parseObject.set('file', parsefile);
                return this.parseObject.save();
            }).then((parseObject) => {
                this.crop.setStatus("upload success");
                this.base64 = null;
            }).catch((err) => {
                console.log(err);
            });
        }
    }

    setCropRef = crop => {
        if (crop) this.crop = crop;
    }
    setThumbnail =()=>{
        if(this.parentObject.dirty()){
            this.parentObject.save().then((obj)=>{
                this.parentObject.set('thumbnail', this.parseObject);
                this.parentObject.save().then((obj) => {
                }).catch((err) => {
                    alert(err);
                });
            })
        }
    }
    render() {
        let src;
        let url = this.parseFile._url;
        if (this.parseFile._url) {
            src = url;
        } else {
            src = this.parseFile._source.file;
        }
        return (
            <ImageCropper
                setThumbnail={this.setThumbnail}
                ref={this.setCropRef}
                src={src}
                onCropped={(base64) => this.base64 = base64}/>
        )
    }
}

class Images extends Component {
    constructor() {
        super();
        this.state = {objects: [], parseFiles: [], images: [], isShow: false};
    }

    componentDidMount() {
        // this.getImages();
    }


    getImages() {
        let {fields} = this.props;
        let className = fields.targetClass;
        let targetColumn = fields.targetColumn;
        let parentObject = this.props.children;

        if (!parentObject.dirty()) {
            var query = new Parse.Query(className);
            query.equalTo(targetColumn, parentObject);
            query.find().then((objs) => {
                let objects = this.state.objects;
                objs.map((parseObject) => {
                    let parseFile = parseObject.get('file');
                    let myImage = new MyImage;
                    myImage.parseObject = parseObject;
                    myImage.parseFile = parseFile;
                    objects.push(myImage);

                });
                this.setState({objects: objects});
            }).catch((err) => {
                alert(err);
            });
        }
    }

    show() {
        this.setState({isShow: true});
    }




    imagesFileChange(e) {
        // let files = e.target.files;
        // let objects = this.state.objects;
        // for (var file of files) {
        //     var parseFile = new Parse.File(file.name, file);
        //     objects.push(parseFile);
        // }
        // this.setState({objects: objects});

        let files = e.target.files;
        let objects = this.state.objects;

        let {className} = this.props;
        let parentObject = this.props.children;
        // let className = fields.targetClass;

        for (let file of files) {
            let parseObject = new Parse.Object(className);
            parseObject.set('product', parentObject);

            let parseFile = new Parse.File(file.name, file);

            let myImage = new MyImage;
            myImage.parseObject = parseObject;
            myImage.parseFile = parseFile;
            myImage.parentObject = parentObject;

            // newObject.set('file', parseFile);
            // newObject.set('product', parentObject);

            objects.push(myImage);
        }
        this.setState({objects: objects})
    }

    defaultClick() {
        let {onDefault} = this.props;
        let object = this.props.children;
        if (onDefault) {
            onDefault(object);
        }
    }

    delete(object) {
        let objects = this.state.objects;
        let index = objects.indexOf(object);
        console.log(index);
        objects.splice(index, 1);
        this.setState({objects: objects});
        object.destroy((obj) => {

        });
    }

    edit() {

    }

    save() {
        this.state.objects.map((obj,index) => {
            obj.save();
        });
    }

    cropped(img) {
        console.log(img);
        // Convert Base64 image to binary
        // var file = dataURItoBlob(img);
        let parseFile = new Parse.File("newImage", {base64: img});
        // parseFile.save().then((newparseFile)=>{
        //     console.log("saved");
        //     console.log(newparseFile);
        // }).catch((xhl)=>{
        //    console.log(xhl) ;
        // });
    }

    cancel() {
        this.setState({isShow: false});
    }

    render() {
        return (
            <div>
                <button onClick={this.show.bind(this)} type="button"
                        className="btn btn-secondary btn-sm">attach
                </button>
                {
                    this.state.isShow &&
                    <div className='modal fade show d-block'
                         style={{overflowY: 'scroll'}}>
                        <div className="modal-dialog modal-dialog-centered modal-lg">
                            <div className="modal-content bg-light">
                                <div className="modal-body">
                                    <button onClick={() => this.setState({isShow: false})} type="button"
                                            className="close btn btn-link"><span>&times;</span>
                                    </button>
                                    <div className="row">
                                        {
                                            this.state.objects.map((obj) => {
                                                return obj.render();
                                            })
                                        }
                                        <div className="col-3 mt-1 card text-center">
                                            <div className="card-body d-flex align-items-center">
                                            <span className="container">
                                              <button className="btn btn-lg btn-outline-primary"
                                                      onClick={() => document.getElementById("images_file").click()}
                                                      type="button"><i
                                                  className="fa fa-plus"></i>
                                              </button>
                                              <p className="font-size-xs mt-2">Add More</p>
                                                <input hidden type="file" onChange={this.imagesFileChange.bind(this)}
                                                       id="images_file" multiple/>
                                            </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="modal-footer py-2">
                                    <button type="button" onClick={this.cancel.bind(this)}
                                            className="btn btn-sm btn-outline-primary"
                                    >Cancel
                                    </button>
                                    <button type="button" onClick={this.save.bind(this)}
                                            className="btn btn-sm btn-primary text-white"
                                    >Save
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                }
                {
                    this.state.isShow &&
                    <div className="modal-backdrop fade show"/>
                }
            </div>

        );
    }

}

export default Images;
