import React, {Component} from 'react';
import Parse from "parse";

class ImageFile extends Component {
    constructor() {
        super();
        this.state = {url: null};
    }

    componentDidMount() {
        this.saveObject();
    }

    saveObject() {
        let object = this.props.children;
        let parseFile = object.get('file');
        if (object.dirty()) {
            var reader = new FileReader();
            reader.onload = ((e) => {
                let url = e.target.result;
                this.setState({url: url});
            });
            let file = parseFile._source.file;
            reader.readAsDataURL(file);
            parseFile.save().then(() => {
                return object.save();
            }).then((obj) => {

            });
        } else {
            let url = parseFile._url;
            this.setState({url: url});
        }
    }
    defaultClick(){
        let {onDefault} = this.props;
        let object = this.props.children;
        if (onDefault) {
            onDefault(object);
        }
    }

    delete() {
        let {onDelete} = this.props;
        let object = this.props.children;
        if (onDelete) {
            onDelete(object);
        }
    }
    getClass(object){
        let {parentObject} = this.props;
        let thumbnail = parentObject.get('thumbnail');
        if(thumbnail && thumbnail.id === object.id){
            return "btn-block btn btn-sm btn-primary rounded-0";
        }else{
            return "btn-block btn btn-sm btn-outline-primary rounded-0 border-0";
        }
    }
    render() {
        let obj = this.props.children;
        return (
            <div className="card">
                <div className="card-body p-0">
                    <div className="card-image">
                         <span className="p-0 position-relative">
                                       <img className="img-fluid"
                                            style={{'height': '150px'}}
                                            src={this.state.url}/>
                                        <button onClick={this.delete.bind(this)}
                                                className="btn btn-sm rounded-0 bg-opacity position-absolute text-primary"
                                                style={{right: '0'}}>
                                            <i className="fa fa-trash"></i>
                                        </button>
                          </span>
                    </div>
                </div>
                <div className="card-footer bg-white p-0">
                    <button onClick={this.defaultClick.bind(this)} className={this.getClass(obj)}>Default</button>
                </div>
            </div>

        );
    }

}

class Images extends Component {
    constructor() {
        super();
        this.state = {objects: [], parseFiles: [], images: [], isShow: false};
    }

    componentDidMount() {
        // this.getImage();
    }

    getImage() {
        let {fields} = this.props;
        let className = fields.targetClass;
        let targetColumn = fields.targetColumn;
        let parentObject = this.props.children;

        if(!parentObject.dirty()){
            var query = new Parse.Query(className);
            query.equalTo(targetColumn, parentObject);
            query.find().then((objs) => {
                this.setState({objects: objs});
            }).catch((err) => {
                alert(err);
            });
        }
    }

    show() {
        this.setState({isShow: true});
    }


    onDelete(object) {
        let objects = this.state.objects;
        let index = objects.indexOf(object);
        console.log(index);
        objects.splice(index, 1);
        this.setState({objects: objects});
        object.destroy((obj) => {

        });
    }
    onDefault(object){
        let parentObject = this.props.children;
        parentObject.set('thumbnail',object);
        parentObject.save().then((obj)=>{
            this.setState({});
        }).catch((err) => {
            alert(err);
        });
    }

    imagesFileChange(e) {
        let files = e.target.files;
        let objects = this.state.objects;

        let {className} = this.props;
        let parentObject = this.props.children;
        // let className = fields.targetClass;

        for (let file of files) {
            let parseFile = new Parse.File(file.name, file);
            let newObject = new Parse.Object(className);
            newObject.set('file', parseFile);
            newObject.set('product', parentObject);
            objects.push(newObject);
        }
        this.setState({objects: objects});
    }

    render() {
        return (
            <div>
                <button onClick={this.show.bind(this)} type="button"
                        className="btn btn-secondary btn-sm">attach
                </button>
                {
                    this.state.isShow &&
                    <div className={this.state.isShow ? 'modal fade show d-block' : 'modal'}
                         style={{overflowY: 'scroll'}}>
                        <div className="modal-dialog modal-dialog-centered modal-lg">
                            <div className="modal-content">
                                <div className="modal-body">
                                    <button onClick={() => this.setState({isShow: false})} type="button"
                                            className="close btn btn-link"><span>&times;</span>
                                    </button>
                                    <div className="row">

                                        {
                                            this.state.objects.map((obj) => {
                                                return (
                                                    <div className="col-3 mt-1">
                                                        <ImageFile parentObject={this.props.children} onDefault={this.onDefault.bind(this)} onDelete={this.onDelete.bind(this)}>{obj}</ImageFile>
                                                    </div>
                                                );
                                            })
                                        }
                                        <div className="col-3 mt-1 card text-center">
                                            <div className="card-body d-flex align-items-center">
                                            <span className="container">
                                              <button className="btn btn-lg btn-outline-primary"
                                                      onClick={() => document.getElementById("images_file").click()}
                                                      type="button"><i
                                                  className="fa fa-plus"></i>
                                              </button>
                                              <p className="font-size-xs mt-2">Add More</p>
                                                <input hidden type="file" onChange={this.imagesFileChange.bind(this)}
                                                       id="images_file" multiple/>
                                            </span>
                                            </div>
                                        </div>
                                    </div>


                                </div>
                            </div>
                        </div>
                    </div>
                }
                {
                    this.state.isShow &&
                    <div className="modal-backdrop fade show"/>
                }
            </div>

        );
    }

}

export default Images;
