import React, {Component} from 'react';
import Editor from "../Editor";


class Form extends Component {


    render() {
        const {fields} = this.props;
        const {newObjects} = this.props;
        return (
            <div className="m-4 p-4 bg-white">
                {
                    newObjects.map((object, index) => {
                        return (
                            <div>
                                {
                                    (index > 0) &&
                                    <h1 className="font-size-md">item {index + 1}</h1>
                                }
                                <div>
                                    <div className="row">
                                        {
                                            Object.keys(fields).map((field) => {
                                                const type = fields[field];
                                                let fieldTrim = field.replace('_', ' ');
                                                return (
                                                    <div class="col-sm-6">
                                                        <div className="form-group">
                                                            <label
                                                                className="font-size-xs font-weight-bold text-capitalize">{fieldTrim}</label>
                                                            <Editor
                                                                type={type}
                                                                field={field}>
                                                                {object}
                                                            </Editor>
                                                        </div>
                                                    </div>
                                                )
                                            })
                                        }
                                    </div>
                                </div>
                            </div>
                        )
                    })
                }
            </div>
        );
    }
}

export default Form;
