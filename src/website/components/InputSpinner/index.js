import React, {Component} from 'react';


class InputSpinner extends Component {
    constructor(props) {
        super();
        this.state = {value: props.defaultValue};
    }

    onIncrement() {
        this.setState({value: this.state.value += 1});
    }

    onDecrement() {
        // this.props.value +=1;
        let value = this.state.value;
        if (value > 1) {
            this.setState({value: value -= 1});
        }
    }
    getValue(){
        return this.state.value;
    }

    render() {
        return (
            <div className="input-group input-group-sm">
                <div className="input-group-prepend">
                    <button onClick={this.onDecrement.bind(this)} className="btn btn-outline-warning" type="button">
                        <i className="fa fa-minus"></i>
                    </button>
                </div>
                <input type="number" value={this.state.value} className="form-control col-2" {...this.props} />
                <div className="input-group-append">
                    <button onClick={this.onIncrement.bind(this)} className="btn btn-outline-success" type="button">
                        <i className="fa fa-plus"></i>
                    </button>
                </div>
            </div>
        );
    }

}

export default InputSpinner;
