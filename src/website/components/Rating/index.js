import React, {Component, Fragment} from 'react';


class Rating extends Component {

    constructor(props) {
        super();
        this.state = {
            scores: props.value,
        };
    }
    rate(scores) {
        if(this.props.onRate){
            this.setState({scores: scores});
            this.props.onRate(scores);
        }
    }
    renderStart(count) {
        let scores = this.state.scores;
        if (count <= scores) {
            return (
                <i className="fa fa-star"></i>
            );
        } else {
            return (
                <i className="fa fa-star-o"></i>
            );
        }
    }

    render() {
        return (
            <Fragment>
                {
                    [1, 2, 3, 4, 5].map((count) => {
                        return (
                            <button onClick={this.rate.bind(this, count)} className="btn btn-link p-0">
                                {
                                    this.renderStart(count)
                                }
                            </button>
                        );
                    })
                }
            </Fragment>
        );
    }

}

export default Rating;
