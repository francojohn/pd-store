import React, {Component} from 'react';


class Modal extends Component {
    render() {
        return (
            <div>
                <div className='modal d-block anim-zoom' style={{overflowY: 'auto'}}>
                    <div className="modal-dialog modal-dialog-centered">
                        <div className="modal-content">
                            {
                                this.props.children
                            }
                        </div>
                    </div>
                </div>
                <div className="modal-backdrop show anim-fade-in"/>
            </div>
        );
    }
}

export default Modal;
