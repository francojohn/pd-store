import React from 'react';
import './Notification.css';

export default class Notification extends React.Component {
    constructor(props) {
        super();
        this.state = {
            lastNote: props.note,
            hiding: false,
        };
        this.timeout = null;
    }

    componentWillUnmount() {
        clearTimeout(this.timeout);
    }

    componentWillReceiveProps(nextProps) {
        if (this.state.lastNote !== nextProps.note) {
            clearTimeout(this.timeout);
            this.setState({lastNote: nextProps.note, hiding: false})
        }
        if (!nextProps.note) {
            return;
        }
        this.timeout = setTimeout(() => {
            this.setState({hiding: true});
            this.timeout = setTimeout(() => {
                this.setState({lastNote: null});
                let {onHide} = this.props;
                if(onHide){
                    onHide();
                }
            }, 190);
        }, 3000);
    }

    render() {
        if (!this.state.lastNote) {
            return null;
        }
        let classes = [];

        classes.push("notificationMessage");

        if (this.state.hiding) {
            classes.push("notificationHide");
        }
        return (
            <div className={classes.join(' ')}>{this.state.lastNote}</div>
        );
    }
}
