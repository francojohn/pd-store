import React, {Component} from 'react';

class DeleteRowsDialog extends Component {
    render() {
        return (
            <div>
                <div className='modal fade show d-block'
                     style={{overflowY: 'scroll'}}>
                    <div className="modal-dialog modal-dialog-centered">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h5 className="modal-title font-size-sm">Are you sure you want to Delete?</h5>
                            </div>
                            <div className="modal-footer py-2">
                                <button type="button"
                                        className="btn btn-sm btn-outline-primary"
                                        onClick={this.props.onCancel}>Cancel
                                </button>
                                <button type="button"
                                        className="btn btn-sm btn-primary text-white"
                                        onClick={this.props.onConfirm}>{this.props.confirmText}
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="modal-backdrop fade show"/>
            </div>
        );
    }

}

export default DeleteRowsDialog;
