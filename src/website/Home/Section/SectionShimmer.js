import React, {Component} from 'react';
import ItemProductShimmer from "../../items/ItemProduct/ItemProductShimmer";


class SectionShimmer extends Component {
    render() {
        return (
            <div>
                <div className="pt-2">
                    <h6 className="shine" style={{width: "70px", height: "20px"}}></h6>

                    <h6 className="shine float-right" style={{width: "70px", height: "20px"}}></h6>
                </div>
                <div className="carousel slide" data-interval="false" data-ride="carousel">
                    <div className="carousel-inner">
                        <div className='carousel-item active'>
                            <div className="row">
                                <div className="col-6 col-sm-2 pr-1 mb-3">
                                    <ItemProductShimmer></ItemProductShimmer>
                                </div>
                                <div className="col-6 col-sm-2 pr-1 mb-3">
                                    <ItemProductShimmer></ItemProductShimmer>
                                </div>
                                <div className="col-6 col-sm-2 pr-1 mb-3">
                                    <ItemProductShimmer></ItemProductShimmer>
                                </div>
                                <div className="col-6 col-sm-2 pr-1 mb-3">
                                    <ItemProductShimmer></ItemProductShimmer>
                                </div>
                                <div className="col-6 col-sm-2 pr-1 mb-3">
                                    <ItemProductShimmer></ItemProductShimmer>
                                </div>
                                <div className="col-6 col-sm-2 pr-1 mb-3">
                                    <ItemProductShimmer></ItemProductShimmer>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        );
    }

}

export default SectionShimmer;
