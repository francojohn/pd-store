import React, {Component} from 'react';
import './style.css';
import ItemProduct from "../../items/ItemProduct";

class Section extends Component {
    constructor() {
        super();
        this.state = {products: []};
    }

    componentDidMount() {
        let section = this.props.children;
        var relation = section.relation("products");
        var query = relation.query();
        query.include('thumbnail');
        query.find().then((objs) => {
            this.setState({products: objs});
        }).catch((err) => {
            alert(err);
        });
    }

    renderRow(from, to) {
        let col = [];
        if (to > this.state.products.length) {
            to = this.state.products.length;
        }
        for (from; from < to; from++) {
            let product = this.state.products[from];
            col.push(
                <div key={product.id} className="col-6 col-sm-2 pr-1 mb-3">
                    <ItemProduct footer={true}>{product}</ItemProduct>
                </div>
            );
        }
        return col;
    }

    render() {
        let section = this.props.children;
        return (
            <div>
                <div className="font-size-sm font-weight-bold">
                    {section.get('name')}
                    <span className="font-size-sm float-right"><a href="#">See All</a></span>
                </div>
                <div id={section.id} className="carousel slide" data-interval="false" data-ride="carousel">
                    <div className="carousel-inner">
                        {
                            this.state.products.map((product, index) => {
                                if (index % 6 === 0) {
                                    return (
                                        <div key={product.id}
                                             className={index === 0 ? 'carousel-item active' : 'carousel-item'}>
                                            <div className="row">
                                                {
                                                    this.renderRow(index, index + 6)
                                                }
                                            </div>
                                        </div>
                                    )
                                }
                            })
                        }
                    </div>
                    <a className="carousel-control-prev-custom justify-content-start" href={'#' + section.id}
                       role="button" data-slide="prev">
                        <span className="carousel-control-prev-icon"></span>
                    </a>
                    <a className="carousel-control-next-custom justify-content-end" href={'#' + section.id}
                       role="button" data-slide="next">
                        <span className="carousel-control-next-icon"></span>
                    </a>

                </div>

            </div>
        );
    }
}

export default Section;