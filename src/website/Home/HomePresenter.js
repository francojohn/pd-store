import Parse from 'parse';
import {parseToProducts} from "../product/ProductMapper";
import ProductModel from "../product/ProductModel";

class HomePresenter {
    constructor(view) {
        this.view = view;
    }

    componentDidMount() {
        this.getProduct();
        // this.testProduct();
    }

    testProduct() {
        fetch("https://itunes.apple.com/in/rss/topalbums/limit=100/json")
            .then((resp) => resp.json())
            .then(({feed}) => {
                let products = [];
                for (let d of feed.entry) {
                    let id = d.id.attributes["im:id"];
                    let link = d.id.label;
                    let title = d.title.label;
                    let image = d["im:image"][2].label;
                    let price = d["im:price"].label;
                    //map data
                    let product = new ProductModel();
                    product.id = id;
                    product.name = title;
                    product.price = price;
                    product.thumbnail = image;
                    //add to array
                    products.push(product);
                }
                //display
                this.view.setProducts(products)
            });
    }

    async getProduct() {
        //get all have variantions product
        let variantQuery = new Parse.Query("Product");
        variantQuery.matchesQuery("variations", new Parse.Query("Product"));
        //get all product don't have variant
        let notVariantQuery = new Parse.Query("Product");
        notVariantQuery.doesNotExist("variant");

        let mainQuery = Parse.Query.or(variantQuery, notVariantQuery);
        mainQuery.includeAll();
        let parseProducts = await mainQuery.find();
        let products = parseToProducts(parseProducts);
        this.view.setProducts(products);
    }
}

export default HomePresenter;
