import React, {Suspense, lazy, Fragment, Component} from 'react';
import './style.css';
import NavScoller, {menu} from "../NavScoller";
import HomePresenter from "./HomePresenter";
import ItemProduct from "../ItemProduct";

// const ItemProduct = lazy(() => import('../ItemProduct'));


class Home extends Component {
    constructor() {
        super();
        this.state = {
            sections: [0],
            products: [],
        };
        this.presenter = new HomePresenter(this)
    }

    componentDidMount() {
        this.presenter.componentDidMount();
        window.scrollTo(0, 0);
    }

    setProducts(products) {
        this.setState({products: products});
    }

    renderCarousel() {
        return (
            <div id="home_banner" className="carousel slide" data-ride="carousel">
                <ol className="carousel-indicators">
                    <li data-target="#home_banner" data-slide-to="0" className="active"></li>
                </ol>

                <div className="carousel-inner">
                    <div className="carousel-item active">
                        <img className="img-fluid w-100"
                             src="/banner1.jpg" alt="First slide"/>
                    </div>
                    <div className="carousel-item">
                        <img className="img-fluid w-100"
                             src="/banner1.jpg" alt="First slide"/>
                    </div>
                    <div className="carousel-item">
                        <img className="img-fluid w-100"
                             src="/banner1.jpg" alt="First slide"/>
                    </div>
                </div>

                <a className="carousel-control-prev" href="#home_banner" role="button"
                   data-slide="prev">
                    <i className="fa fa-angle-left fa-3x text-secondary font-weight-bold"></i>
                </a>

                <a className="carousel-control-next" href="#home_banner" role="button"
                   data-slide="next">
                    <i className="fa fa-angle-right fa-3x text-secondary font-weight-bold"></i>
                </a>

            </div>
        );
    }

    renderProduct() {
        return this.state.products.map((product) => {
            return (
                <div key={product.id} className="col-6 col-md-3 p-2">
                    <ItemProduct footer={true}>{product}</ItemProduct>
                </div>
            )
        })
    }

    render() {
        let menus = [
            new menu('/', 'Tshirts'),
            new menu('/', 'Mug'),
            new menu('/', 'ID Lace'),
            new menu('/', 'Sticker'),
            new menu('/', 'HTML Templates'),
            new menu('/', 'PSD'),
            new menu('/', 'Ebook'),
            new menu('/', 'Logo'),
            new menu('/', 'C/C++'),
            new menu('/', 'Java'),
            new menu('/', 'Android App'),
            new menu('/', 'IOS App'),
            new menu('/', 'Desktop App'),
            new menu('/', 'Website'),
            new menu('/', 'Javascript'),
        ];

        return (
            <div className="bg-white">
                <NavScoller menus={menus}/>
                {/*{*/}
                {/*this.renderCarousel()*/}
                {/*}*/}
                <div className="container py-3" style={{marginBottom: "80px"}}>
                    <div className="row">
                        {
                            this.renderProduct()
                        }
                    </div>
                </div>

            </div>
        );
    }
}


export default Home;
