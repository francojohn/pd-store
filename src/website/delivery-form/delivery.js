import React,{Component} from "react";
class Deliver extends Component {
    formSubmit =(e)=>{
        e.preventDefault();
        let {formSubmit} = this.props;
        if(formSubmit){
            formSubmit();
        }
    }
    render() {
        return (
            <div className="deliver-form card mb-5">
                <form onSubmit={this.formSubmit} className="card-body">
                    <h5 className="mb-4">Delivery Information</h5>
                    <div className="row">
                        <div className="col-sm-6">
                            <div className="form-group">
                                <label className="font-size-xs mb-1 pl-2">Full Name</label>
                                <input type="text" className="form-control" placeholder="Full Name" id="delivery_full_name"/>
                            </div>
                            <div className="form-group">
                                <label className="font-size-xs mb-1 pl-2">Mobile Number</label>
                                <input type="text" className="form-control" placeholder="Mobile Number" id="delivery_mobile_number" />
                            </div>
                            <div className="form-group">
                                <label className="font-size-xs mb-1 pl-2">Notes</label>
                                <textarea type="text" className="form-control" placeholder="Notes For Rider (optional)" id="delivery_notes" />
                            </div>
                        </div>
                        <div className="col-sm-6">
                            <div className="form-group">
                                <label className="font-size-xs mb-1 pl-2">House Number, Street Name, Barangay, City
                                </label>
                                <textarea type="text" rows="3"  className="form-control" placeholder="Complete Address" id="delivery_complete_address"/>
                            </div>
                            <div className="text-right">
                                <button type="submit" className="btn btn-sm btn-primary rounded text-white px-5 my-3 text-uppercase">SAVE</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        );
    }
}

export default Deliver;