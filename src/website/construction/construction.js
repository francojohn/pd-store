import React, {Component} from "react";
import {Link} from 'react-router-dom';
import './style.css';
import Parse from 'parse';

class Construction extends Component {
    render() {

        return (
            <div className="construction">
                <div className="container">
                    <div className="row">
                        <div className="col-sm-6 offset-sm-3 text-center">
                            <img src="/constructicon.png" className="img-fluid w-25" />
                            <br />
                            <br />
                            <h1 className="text-white font-size-xl">Under Construction</h1>
                            <p className="text-muted"><b>Site is under Construction process.</b></p>
                            <Link to="/" className="btn btn-primary text-white font-weight-bold text-uppercase rounded py-2 px-3 font-size-xs m-1"><i
                                className="fas fa-long-arrow-alt-left"></i>&nbsp;&nbsp;GO BACK TO HOME</Link>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Construction;