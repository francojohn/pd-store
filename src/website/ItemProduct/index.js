import React, {Component,Suspense, lazy} from 'react';
import Parse from 'parse';
import './style.css';
import {Link} from 'react-router-dom';
import LazyLoad from 'react-lazy-load';
// const LazyLoad = lazy(() => import('react-lazy-load'));

class Index extends Component {
    constructor() {
        super();
    }

    render() {
        let {no_footer} = this.props;
        let product = this.props.children;
        const loadingImg = <h1>
            Loading....
        </h1>
        return (
            <div className="card border-0">
                <div className="card-body p-0">
                    <div className="card-content"></div>
                    <div className="card-image">
                        <Link to={`/product/${product.id}`}>
                                <LazyLoad width={250}
                                          height={250}
                                          offsetVertical={500}>
                                    <img className="img-fluid w-100 p-4"
                                         style={{'height': 'auto'}}
                                         src={product.thumbnail}/>
                                </LazyLoad>
                        </Link>
                    </div>
                    {
                        !no_footer &&
                        <div className="prod-details p-1 font-size-sm text-dark">
                            <div className="font-size-sm  mt-3 mb-2">
                                <i className="fa fa-star text-warning"></i>
                                <i className="fa fa-star text-warning"></i>
                                <i className="fa fa-star text-warning"></i>
                                <i className="fa fa-star text-warning"></i>
                                <i className="fa fa-star text-secondary"></i>
                            </div>
                            <h6 className="text-capitalize text-primary mb-1">{product.name}</h6>
                            {/*<span className="text-muted">&#8369;*/}
                            {/*<del>300</del>*/}
                            {/*&nbsp;&nbsp;</span>*/}
                            <span>₱<b>{product.price}</b></span>
                        </div>
                    }

                </div>
            </div>
        );
    }
}


export default Index;
