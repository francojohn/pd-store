import React, {Component} from 'react';


class ItemProductShimmer extends Component {

    render() {
        return (

            <div className="card">
                <div className="card-body p-0">
                    <a>
                        <div className="card-image p-1">
                            <div className="shine w-100"
                                 style={{'height': '10rem'}}/>

                        </div>
                        <div className="p-1 font-size-sm text-dark">
                            <i className='shine' style={{height: '14px', width: '120px'}}></i>
                            <div>

                          <span>
                              <i className='shine' style={{height: '14px', width: '35px'}}></i>
                              &nbsp;&nbsp;
                          </span>
                                <i className='shine' style={{height: '14px', width: '35px'}}></i>
                            </div>
                            <i className='shine' style={{height: '14px', width: '70px'}}></i>
                        </div>
                    </a>
                    <div className="card-footer bg-white py-1">
                        <div className="row font-size-sm text-center">
                            <div className="col-6 p-0 border-right">
                                <i className='shine' style={{height: '14px', width: '30px'}}></i>
                            </div>

                            <div className="col-6 p-0">
                                <i className='shine' style={{height: '14px', width: '30px'}}></i>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        );
    }

}

export default ItemProductShimmer;

