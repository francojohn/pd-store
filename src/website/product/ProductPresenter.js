import Parse from 'parse';
import {getPrice} from "../../utils";

class ProductPresenter {
    constructor(view) {
        this.view = view;
    }

    componentDidMount() {
        this.fetchProduct();
    }

    getProductThumbnail(product) {
        return product.get("thumbnail");
    }

    async fetchProduct() {
        let carts = this.view.getCarts();

        let allImages = [];
        let productId = this.view.getProductId();
        let product = await this.getProductById(productId);
        let selectedVariant;
        let variations = await this.getVariantsByProduct(product);
        let variant = product.get("variant");

        //get images result
        for (let p of variations) {
            let thumbnail = this.getProductThumbnail(p);
            if (thumbnail) {
                allImages.push(thumbnail);
            }
        }
        // let variations = this.getProductVariations(variants);
        //check if have variations
        if (variations.length) {
            selectedVariant = variations[0];
        }

        let images = await this.getProductImageByProduct(product);
        allImages.push(...images);
        let price = getPrice(product, selectedVariant);
        let stock = this.getStock(product, selectedVariant);

        //
        this.view.setSelectedProductVariant(selectedVariant);
        this.view.setProduct(product);
        this.view.setPrice(price);
        this.view.setStock(stock);
        this.view.setImages(allImages);
        this.view.setVariants(variant ? variant : {});
        this.view.setVariations(variations);
    }

    getStock(product, selectedVariant) {
        let stock = product.get("stock");
        if (selectedVariant) {
            let selectedStock = selectedVariant.get("stock");
            stock = selectedStock;
        }
        return stock ? stock : 0;
    }

    getProductById(id) {
        let query = new Parse.Query("Product");
        query.equalTo("objectId", id);
        query.includeAll();
        return query.first();
    }

    getProductImageByProduct(product) {
        let query = new Parse.Query("ProductImage");
        query.equalTo("product", product);
        query.includeAll();
        return query.find();
    }

    getVariantsByProduct(product) {
        let relation = product.relation("variations");
        let query = relation.query();
        query.includeAll();
        return query.find();
    }

    getNewStock(oldStock, newQuantity, oldQuantity) {
        if (oldQuantity !== 1) {
            let changeQuantity = newQuantity - oldQuantity;
            return oldStock - changeQuantity
        }
        return oldStock - newQuantity;
    }

    getQuantityChanges(newQuantity, oldQuantity) {
        return newQuantity - oldQuantity;
    }

    quantityChange(newQuantity) {
        if (newQuantity < 1) {
            return;
        }
        // let oldQuantity = this.view.getQuantity();
        let stock = this.view.getStock();
        // let newStock = this.getNewStock(oldStock, newQuantity, oldQuantity);
        if (stock >= newQuantity) {
            this.view.setQuantity(newQuantity);
        }
    }


    getSelectedVariant(variations, selectedVariant) {
        let variants = {};
        for (let product of variations) {
            let variant = product.get('variant');
            let key = JSON.stringify(variant);
            variants[key] = product;
        }
        let key2 = JSON.stringify(selectedVariant);
        return variants[key2];
    }

    variantChange(newVariant) {
        let variations = this.view.getVariations();
        let last_selectedVariant = this.view.getSelectedProductVariant();
        let selectedVariantKey = last_selectedVariant.get("variant");
        let new_selectedVariant = this.getSelectedVariant(variations, newVariant);
        if (new_selectedVariant) {
            let thumbnail = this.getProductThumbnail(new_selectedVariant);
            if (thumbnail) {
                this.view.activeImage(thumbnail.id);
            }
            let stock = new_selectedVariant.get("stock");
            let parentProduct = this.view.getProduct();
            let price = getPrice(parentProduct, new_selectedVariant);
            //
            this.view.setPrice(price);
            this.view.setStock(stock);
            this.view.setQuantity(1);
            this.view.setSelectedProductVariant(new_selectedVariant);
        } else {
            this.view.setStock(0);
        }
    }

    isProductIntheCart(product, carts, selectedProductVariant) {
        //get cart by product
        let cart_old;
        if (selectedProductVariant) {//check if have variant
            cart_old = carts.find((c) => {
                let product_variant = c.get('product_variant');
                if(product_variant){
                    return product_variant.id === selectedProductVariant.id
                }
            })
        } else {
            cart_old = carts.find(c => c.get('product').id === product.id);
        }
        return cart_old;
    }

    async addToCartClick() {
        //get
        let user = Parse.User.current();
        if (!user) {
            this.view.gotoLogin();
            return;
        }
        try {
            let carts = this.view.getCarts();
            let product = this.view.getProduct();
            let selectedProductVariant = this.view.getSelectedProductVariant();
            let quantity = this.view.getQuantity();
            //check if product already from the cart
            let cart_new = new Parse.Object("Cart");


            let cart_old = this.isProductIntheCart(product, carts, selectedProductVariant);
            if (cart_old) {
                cart_old.increment('quantity', quantity);
                await cart_old.save();
            } else {
                //create
                cart_new.set("product", product);
                cart_new.set("product_variant", selectedProductVariant);
                cart_new.set("quantity", quantity);
                cart_new.set("user", user);
                await cart_new.save();
                carts.push(cart_new);
                this.view.setCarts(carts);
            }
            //set stock
            let stock = this.view.getStock();
            this.view.setStock(stock - quantity);
        } catch (e) {
            alert(e);
        }

    }
}

export default ProductPresenter