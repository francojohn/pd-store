import ProductPresenter from "./ProductPresenter";

describe("test quantity change", () => {
    let presenter = new ProductPresenter(this);
    test("test quantity first", () => {
        let newQuantity = 2;
        let oldQuantity = 1;
        let oldStock = 10;
        let result = presenter.getNewStock(oldStock, newQuantity, oldQuantity);
        let expectedResult = 8;
        expect(result).toBe(expectedResult);
    })
    test("test quantity second", () => {
        let newQuantity = 3;
        let oldQuantity = 2;
        let oldStock = 8;
        // let quantityChange = presenter.getQuantityChanges(newQuantity,oldQuantity);
        let result = presenter.getNewStock(oldStock, newQuantity, oldQuantity);
        let expectedResult = 7;
        expect(result).toBe(expectedResult);
    })
});
describe("test variations", () => {
    let presenter = new ProductPresenter(this);
    let variantWhite = {"color:": "white"};
    let variantBlack = {"color:": "black"};
    let p1 = {
        name: "tshirt white", get: function (key) {
            return variantWhite;
        }
    };
    let p2 = {
        name: "tshirt black", get: function (key) {
            return variantBlack;
        }
    };
    let variations = [p1, p2];
    test("test white", () => {
        let expectedResult = p1;
        let result = presenter.getSelectedVariant(variations, variantWhite);
        expect(result).toEqual(expectedResult);
    });
    test("test black", () => {
        let expectedResult = p2;
        let result = presenter.getSelectedVariant(variations, variantBlack);
        expect(result).toEqual(expectedResult);
    });

});

describe("test add to cart", () => {
    let presenter = new ProductPresenter(this);
    test("no variant and have a variant", () => {
        let product = {};
        let productNoVariant = {
            get: function (key) {
                //no return
            }
        };
        let carts = [productNoVariant];//no variant from the cart
        let selectedProductVariant = {id: "123"};
        let result = presenter.isProductIntheCart(product, carts, selectedProductVariant);
        expect(result).toBeUndefined();
    });
    test("have a variant and have a variant", () => {
        let newProduct = {};
        let productNoVariant = {
            get: function (key) {
                //no return
            }
        };
        let productHaveVariant = {
            id:"456",
            get: function (key) {
               return this;
            }
        };
        let carts = [productNoVariant,productHaveVariant];//existing product on the carts
        let selectedProductVariant = {id: "456"};//new selected variant
        let expectedResult = productHaveVariant;
        let result = presenter.isProductIntheCart(newProduct, carts, selectedProductVariant);
        expect(result).toEqual(expectedResult);
    });
    test("no variant and no variant", () => {
        //new selected product
        let product = {
            id: "123",
            get: function (key) {
                return this;
            }
        };
        let product2 = {
            id: "456",
            get: function (key) {
                return this;
            }
        };
        let product3 = {
            id: "789",
            get: function (key) {
                return this;
            }
        };
        let carts = [product2,product,product3]; //existing product on the carts
        let selectedProductVariant; //no variant
        let expectedResult = product;
        let result = presenter.isProductIntheCart(product, carts, selectedProductVariant);
        expect(result).toEqual(expectedResult);
    });


});