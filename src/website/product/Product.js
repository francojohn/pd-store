import React, {Component, Fragment} from "react";
import "./style.css";
import ProductPresenter from "./ProductPresenter";
import {WithContext} from "../../Context";

class Product extends Component {

    constructor() {
        super();
        this.presenter = new ProductPresenter(this);
        this.state = {
            product: null,
            selectedProductVariant: null,
            images: [],
            variant: {},
            variations: {},
            comments: [],
            isLoaded: false,
            quantity: 1,
            stock: 0,
            price: 0,
        };
    }

    componentDidMount() {
        window.scrollTo(0, 0);
        this.presenter.componentDidMount();
    }

    getProductId() {
        return this.props.match.params.id;
    }

    getStock() {
        let {stock} = this.state;
        return stock;
    }

    getQuantity() {
        let {quantity} = this.state;
        return quantity;
    }

    setPrice(price){
        this.setState({price: price});
    }
    setProduct(product) {
        this.setState({product: product});
    }

    setSelectedProductVariant(product) {
        this.setState({selectedProductVariant: product});
    }

    getSelectedProductVariant() {
        let {selectedProductVariant} = this.state;
        return selectedProductVariant;
    }

    getProduct() {
        let {product} = this.state;
        return product;
    }

    setQuantity(quantity) {
        this.setState({quantity: quantity});
    }

    setStock(stock) {
        this.setState({stock: stock});
    }

    setVariants(variant) {
        this.setState({variant: variant});
    }

    setImages(images) {
        this.setState({images: images});
    }

    setVariations(variations) {
        this.setState({variations: variations});
    }

    getVariations() {
        let {variations} = this.state;
        return variations;
    }

    quantityOnchange = (e) => {
        let quantity = e.target.value;
        this.presenter.quantityChange(parseInt(quantity));
    }
    variantChange = (variant) => {
        this.presenter.variantChange(variant);
    }

    activeImage(id) {
        let div = document.getElementById(id);
        if (div) {
            div.click();
        }
    }

    addToCartClick = () => {
        this.presenter.addToCartClick();
    }
    setCarts(carts){
        let {setCarts} = this.props;
        if(setCarts){
            setCarts(carts);
        }
    }
    getCarts(){
        let {carts} = this.props;
        return carts;
    }
    gotoLogin(){
        this.props.history.push("/signin");
    }
    render() {
        let {product} = this.state;
        let {price} = this.state;
        let {images} = this.state;
        let {stock} = this.state;
        let {quantity} = this.state;
        let {variant} = this.state;
        let {selectedProductVariant} = this.state;
        if (!product) {
            return (<h1>loading</h1>);
        }
        let name = product.get("name");
        let description = product.get("description");
        let available = stock - quantity;
        let stockLabel = `${stock === 1 ? 1 : available} piece available`;
        if (!stock) {
            stockLabel = "out of stock";
        }

        return (
            <div className="product">
                <div className="container">
                    <div className="card rounded-0 py-5 px-2 my-5">
                        <div className="card-body">
                            <div className="row">
                                <div className="col-sm-12 col-lg-5">
                                    <div id="carousel-payment" className="carousel slide" data-ride="carousel"
                                         data-interval="false">
                                        <div className="carousel-inner">
                                            {
                                                images.map((image, index) => {
                                                    let file = image.get('file');
                                                    if (!file) {
                                                        return;
                                                    }
                                                    let image_url = file._url.replace("http", "https");
                                                    return (
                                                        <div
                                                            className={index === 0 ? 'carousel-item active' : 'carousel-item'}>
                                                            <img className="img-fluid w-100"
                                                                 style={{'height': 'auto'}}
                                                                 src={image_url}
                                                            />
                                                        </div>
                                                    )
                                                })
                                            }

                                        </div>
                                        <div id="circle">
                                            <a href="#carousel-payment" role="button"
                                               data-slide="prev">
                                                <div className="left">
                                                    <span className="fa fa-angle-left" aria-hidden="true"></span>
                                                    <span className="sr-only">Previous</span>
                                                </div>
                                            </a>
                                            <a href="#carousel-payment" role="button"
                                               data-slide="next">
                                                <div className="right">
                                                    <span className="fa fa-angle-right" aria-hidden="true"></span>
                                                    <span className="sr-only">Next</span>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                    <ul className="scrollmenu mx-0">
                                        {
                                            images.map((image, index) => {
                                                let file = image.get('file');
                                                if (!file) {
                                                    return;
                                                }
                                                let id = image.id;
                                                let image_url = file._url.replace("http", "https");
                                                return (
                                                    <li data-target="#carousel-payment"
                                                        className={index === 0 ? 'active p-2 m-3' : 'p-2 m-3'}
                                                        data-slide-to={index}>
                                                        <a className="thumbnail" id={id}>
                                                            <img className="img-thumbnail-size"
                                                                 src={image_url}/>
                                                        </a>
                                                    </li>
                                                );
                                            })
                                        }

                                    </ul>
                                </div>
                                <div className="col-sm-12 col-lg-7 jumbotron bg-transparent mb-0 pt-3">
                                    <div className="product-information">
                                        <h3 className="mb-3">
                                            {name}
                                        </h3>
                                        <label className="font-size-sm bg-primary text-white py-2 px-3">
                                            ₱ {price}
                                        </label>
                                        <span
                                            className="ml-3 text-secondary font-size-xs">By ProgrammersDevelopers</span>
                                        <br/>
                                        <div className="dropdown-divider"/>
                                        <label className="font-size-xs font-weight-bold mb-2 text-uppercase">
                                            Description
                                        </label>
                                        <p>{description}</p>
                                        <div className="dropdown-divider"></div>
                                        {
                                            Object.keys(variant).map((key) => {
                                                let variants = variant[key];
                                                return (
                                                    <Fragment>
                                                        <label
                                                            className="font-size-xs font-weight-bold mb-2 mt-3 text-uppercase">{key}</label>
                                                        <div
                                                            className="btn-group btn-check btn-group-toggle d-block"
                                                            data-toggle="buttons">
                                                            {
                                                                variants.map((va, index) => {
                                                                    let newSelectedVariant = {};
                                                                    newSelectedVariant[key] = va;
                                                                    let isSelected = selectedProductVariant.get("variant")[key] === va;
                                                                    // if (index === 0) {
                                                                    //     if (!this.selectedVariant[key]) {
                                                                    //         this.selectedVariant[key] = va;
                                                                    //     }
                                                                    // }

                                                                    return (
                                                                        <label
                                                                            onClick={() => this.variantChange(newSelectedVariant)}
                                                                            className={`m-1 btn btn-sm mr-2 text-capitalize rounded-0 py-2 px-3 ${isSelected ? 'active' : ''}`}>
                                                                            <input type="radio" name="options"
                                                                                   checked/> {va}
                                                                        </label>

                                                                    )
                                                                })
                                                            }
                                                            <div className="clearfix"></div>
                                                        </div>
                                                    </Fragment>
                                                )
                                            })
                                        }

                                        <div className="row">
                                            <div className="col-sm-6">
                                                <label
                                                    className="font-size-xs font-weight-bold mb-2 mt-3 text-uppercase d-block">
                                                    Quantity
                                                </label>
                                                <div>
                                                    <input type="number" value={quantity}
                                                           className="form-control form-control-sm d-inline w-25"
                                                           onChange={this.quantityOnchange}/>
                                                    <span className="text-muted font-size-xs pl-2">{stockLabel}</span>
                                                </div>
                                            </div>
                                            <div className="col-sm-6">
                                                <label
                                                    className="font-size-xs font-weight-bold mb-2 mt-3 text-uppercase d-block">
                                                    Delivery
                                                </label>
                                                <p className="font-size-sm">2 to 3 Days, Depending on the
                                                    destination</p>
                                            </div>

                                        </div>
                                        <div className="dropdown-divider"/>
                                        <div className="row">
                                            <div className="col-sm-12 mt-4">
                                                <div className="content">
                                                    <button onClick={this.addToCartClick} disabled={stock === 0}
                                                            className="btn btn-primary text-white font-weight-bold text-uppercase rounded py-2 px-3 font-size-xs m-1">
                                                        <i className="fa fa-shopping-cart"></i>&nbsp;&nbsp;&nbsp;Add to
                                                        cart
                                                    </button>
                                                    <button
                                                        className="btn btn-outline-dark text-muted font-weight-bold text-uppercase rounded py-2 px-3 font-size-xs m-1">
                                                        <i className="fa fa-heart"
                                                           aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Add to favorites
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="card mt-3 border-0">
                                <div className="card-body">
                                    <h5 className="mb-4">Ratings & Reviews PD T-Shirt</h5>
                                    <div className="row">
                                        <div className="col-sm-12 col-md-12 col-lg-2">
                                            <div className="text-center">
                                                <h1 className="display-5 font-size-xl m-0">4.5</h1>
                                                <i className="fa fa-star text-secondary fa-sm" aria-hidden="true"></i>
                                                <i className="fa fa-star text-secondary fa-sm" aria-hidden="true"></i>
                                                <i className="fa fa-star text-secondary fa-sm" aria-hidden="true"></i>
                                                <i className="fa fa-star text-secondary fa-sm" aria-hidden="true"></i>
                                                <i className="fa fa-star text-secondary fa-sm text-dark"
                                                   aria-hidden="true"></i>
                                                <div className="my-address-title text-muted font-size-xs mt-1">2,799
                                                    total
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-sm-12 col-md-12 col-lg-3">
                                            <div className="progress-bar-content my-2">
                                                <div className="float-left text-center font-size-xs"
                                                     style={{width: '10%'}}>5&nbsp;
                                                    <i className="fa fa-star text-secondary fa-sm"
                                                       aria-hidden="true"></i>
                                                </div>
                                                <div className="float-left" style={{width: '80%'}}>
                                                    <div className="progress mx-1">
                                                        <div className="progress-bar bg-warning" role="progressbar"
                                                             style={{width: '25%'}} aria-valuenow="25"
                                                             aria-valuemin="0" aria-valuemax="100"></div>
                                                    </div>
                                                </div>
                                                <div className="float-left text-center font-size-xs"
                                                     style={{width: '10%'}}>
                                                    <div className="font-weight-light text-dark">4</div>
                                                </div>
                                            </div>
                                            <div className="progress-bar-content my-2">
                                                <div className="float-left text-center font-size-xs"
                                                     style={{width: '10%'}}>4&nbsp;
                                                    <i className="fa fa-star text-secondary fa-sm"
                                                       aria-hidden="true"></i>
                                                </div>
                                                <div className="float-left" style={{width: '80%'}}>
                                                    <div className="progress mx-1">
                                                        <div className="progress-bar bg-warning" role="progressbar"
                                                             style={{width: '25%'}} aria-valuenow="25"
                                                             aria-valuemin="0" aria-valuemax="100"></div>
                                                    </div>
                                                </div>
                                                <div className="float-left text-center font-size-xs"
                                                     style={{width: '10%'}}>
                                                    <div className="font-weight-light text-dark">4</div>
                                                </div>
                                            </div>
                                            <div className="progress-bar-content my-2">
                                                <div className="float-left text-center font-size-xs"
                                                     style={{width: '10%'}}>3&nbsp;
                                                    <i className="fa fa-star text-secondary fa-sm"
                                                       aria-hidden="true"></i>
                                                </div>
                                                <div className="float-left" style={{width: '80%'}}>
                                                    <div className="progress mx-1">
                                                        <div className="progress-bar bg-warning" role="progressbar"
                                                             style={{width: '25%'}} aria-valuenow="25"
                                                             aria-valuemin="0" aria-valuemax="100"></div>
                                                    </div>
                                                </div>
                                                <div className="float-left text-center font-size-xs"
                                                     style={{width: '10%'}}>
                                                    <div className="font-weight-light text-dark">4</div>
                                                </div>
                                            </div>
                                            <div className="progress-bar-content my-2">
                                                <div className="float-left text-center font-size-xs"
                                                     style={{width: '10%'}}>2&nbsp;
                                                    <i className="fa fa-star text-secondary fa-sm"
                                                       aria-hidden="true"></i>
                                                </div>
                                                <div className="float-left" style={{width: '80%'}}>
                                                    <div className="progress mx-1">
                                                        <div className="progress-bar bg-warning" role="progressbar"
                                                             style={{width: '25%'}} aria-valuenow="25"
                                                             aria-valuemin="0" aria-valuemax="100"></div>
                                                    </div>
                                                </div>
                                                <div className="float-left text-center font-size-xs"
                                                     style={{width: '10%'}}>
                                                    <div className="font-weight-light text-dark">4</div>
                                                </div>
                                            </div>
                                            <div className="progress-bar-content my-2">
                                                <div className="float-left text-center font-size-xs"
                                                     style={{width: '10%'}}>1&nbsp;
                                                    <i className="fa fa-star text-secondary fa-sm"
                                                       aria-hidden="true"></i>
                                                </div>
                                                <div className="float-left" style={{width: '80%'}}>
                                                    <div className="progress mx-1">
                                                        <div className="progress-bar bg-warning" role="progressbar"
                                                             style={{width: '25%'}} aria-valuenow="25"
                                                             aria-valuemin="0" aria-valuemax="100"></div>
                                                    </div>
                                                </div>
                                                <div className="float-left text-center font-size-xs"
                                                     style={{width: '10%'}}>
                                                    <div className="font-weight-light text-dark">4</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <nav className="mt-5">
                                <div className="nav nav-pills" id="nav-tab" role="tablist">
                                    <a className="nav-item nav-link active" id="nav-specs-tab" data-toggle="tab"
                                       href="#nav-specs" role="tab" aria-controls="nav-specs"
                                       aria-selected="true">Description</a>
                                    <a className="nav-item nav-link" id="nav-rating-tab" data-toggle="tab"
                                       href="#nav-rating" role="tab" aria-controls="nav-rating" aria-selected="true">Additional
                                        Specification</a>
                                    <a className="nav-item nav-link" id="nav-comments-tab" data-toggle="tab"
                                       href="#nav-comments" role="tab" aria-controls="nav-comments"
                                       aria-selected="false">Reviews</a>
                                </div>
                            </nav>
                            <div className="tab-content" id="nav-tabContent">
                                <div className="tab-pane fade show active" id="nav-specs" role="tabpanel"
                                     aria-labelledby="nav-specs-tab">
                                    <div className="content py-5 px-2">
                                        <p>
                                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                            tempor
                                            incididunt ut labore et dolore magna aliqua. LOLUt enim ad minim veniam,
                                            quis
                                            nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                            consequat.
                                            LOLDuis aute irure dolor in reprehenderit in voluptate velit esse cillum
                                            dolore
                                            eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident,
                                            sunt
                                            in culpa qui officia deserunt mollit anim id est laborum.
                                        </p>
                                    </div>
                                </div>
                                <div className="tab-pane fade" id="nav-rating" role="tabpanel"
                                     aria-labelledby="nav-rating-tab">
                                    <div className="content py-5 px-2">
                                        <table className="table table-striped">
                                            <tbody>
                                            <tr>
                                                <th scope="col" className="font-size-sm">Brand</th>
                                                <td>Apple</td>
                                            </tr>
                                            <tr>
                                                <th scope="col" className="font-size-sm">Model</th>
                                                <td>Airpods</td>
                                            </tr>
                                            <tr>
                                                <th scope="col" className="font-size-sm">Warranty Period</th>
                                                <td>1 Year</td>
                                            </tr>
                                            <tr>
                                                <th scope="col" className="font-size-sm">SKU</th>
                                                <td>AP675ELAAYU001ANPH-74893853</td>
                                            </tr>
                                            <tr>
                                                <th scope="col" className="font-size-sm">Warranty Type</th>
                                                <td>Local Manufacturer Warranty</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div className="tab-pane fade" id="nav-comments" role="tabpanel"
                                     aria-labelledby="nav-comments-tab">
                                    <div className="content py-5 px-2">
                                        <div className="row">
                                            <div className="col-sm-12">
                                                <div className="prod-review">
                                                    <div className="comment py-3 border-bottom">
                                                        <div className="float-left" style={{width: '8%'}}>
                                                            <img src="/download.jpg"
                                                                 style={{width: 'auto', height: 'auto'}}
                                                                 className="rounded-circle img-fluid"/>
                                                        </div>
                                                        <div className="float-right pl-3" style={{width: '92%'}}>
                                                            <div className="row">
                                                                <div className="col-sm-8">
                                                                    <h7><b>Romy Dagohoy Jr.</b></h7>
                                                                    <span
                                                                        className="d-block mb-0 text-muted font-size-sm"
                                                                        style={{'font-size': '11px'}}>08:00 - January 12, 2018</span>
                                                                </div>
                                                                <div className="col-sm-4 text-right">
                                                                    <i className="fa fa-star text-secondary fa-sm"
                                                                       aria-hidden="true"></i>
                                                                    <i className="fa fa-star text-secondary fa-sm"
                                                                       aria-hidden="true"></i>
                                                                    <i className="fa fa-star text-secondary fa-sm"
                                                                       aria-hidden="true"></i>
                                                                    <i className="fa fa-star text-secondary fa-sm"
                                                                       aria-hidden="true"></i>
                                                                    <i className="fa fa-star text-secondary fa-sm text-dark"
                                                                       aria-hidden="true"></i>
                                                                </div>
                                                            </div>
                                                            <p className="m-0">Lorem ipsum dolor sit amet, consectetur
                                                                adipisicing elit, sed do eiusmod tempor incididunt ut
                                                                labore
                                                                et dolore magna aliqua.</p>
                                                        </div>
                                                        <div className="clearfix"></div>
                                                    </div>
                                                    <div className="comment py-3 border-bottom">
                                                        <div className="float-left" style={{width: '8%'}}>
                                                            <img src="/download.jpg"
                                                                 style={{width: 'auto', height: 'auto'}}
                                                                 className="rounded-circle img-fluid"/>
                                                        </div>
                                                        <div className="float-right pl-3" style={{width: '92%'}}>
                                                            <div className="row">
                                                                <div className="col-sm-8">
                                                                    <h7><b>Romy Dagohoy Jr.</b></h7>
                                                                    <span
                                                                        className="d-block mb-0 text-muted font-size-sm"
                                                                        style={{'font-size': '11px'}}>08:00 - January 12, 2018</span>
                                                                </div>
                                                                <div className="col-sm-4 text-right">
                                                                    <i className="fa fa-star text-secondary fa-sm"
                                                                       aria-hidden="true"></i>
                                                                    <i className="fa fa-star text-secondary fa-sm"
                                                                       aria-hidden="true"></i>
                                                                    <i className="fa fa-star text-secondary fa-sm"
                                                                       aria-hidden="true"></i>
                                                                    <i className="fa fa-star text-secondary fa-sm"
                                                                       aria-hidden="true"></i>
                                                                    <i className="fa fa-star text-secondary fa-sm text-dark"
                                                                       aria-hidden="true"></i>
                                                                </div>
                                                            </div>
                                                            <p className="m-0">Lorem ipsum dolor sit amet, consectetur
                                                                adipisicing elit, sed do eiusmod tempor incididunt ut
                                                                labore
                                                                et dolore magna aliqua.</p>
                                                        </div>
                                                        <div className="clearfix"></div>
                                                    </div>
                                                    <div className="comment py-3 border-bottom">
                                                        <div className="float-left" style={{width: '8%'}}>
                                                            <img src="/download.jpg"
                                                                 style={{width: 'auto', height: 'auto'}}
                                                                 className="rounded-circle img-fluid"/>
                                                        </div>
                                                        <div className="float-right pl-3" style={{width: '92%'}}>
                                                            <div className="row">
                                                                <div className="col-sm-8">
                                                                    <h7><b>Romy Dagohoy Jr.</b></h7>
                                                                    <span
                                                                        className="d-block mb-0 text-muted font-size-sm"
                                                                        style={{'font-size': '11px'}}>08:00 - January 12, 2018</span>
                                                                </div>
                                                                <div className="col-sm-4 text-right">
                                                                    <i className="fa fa-star text-secondary fa-sm"
                                                                       aria-hidden="true"></i>
                                                                    <i className="fa fa-star text-secondary fa-sm"
                                                                       aria-hidden="true"></i>
                                                                    <i className="fa fa-star text-secondary fa-sm"
                                                                       aria-hidden="true"></i>
                                                                    <i className="fa fa-star text-secondary fa-sm"
                                                                       aria-hidden="true"></i>
                                                                    <i className="fa fa-star text-secondary fa-sm text-dark"
                                                                       aria-hidden="true"></i>
                                                                </div>
                                                            </div>
                                                            <p className="m-0">Lorem ipsum dolor sit amet, consectetur
                                                                adipisicing elit, sed do eiusmod tempor incididunt ut
                                                                labore
                                                                et dolore magna aliqua.</p>
                                                        </div>
                                                        <div className="clearfix"></div>
                                                    </div>
                                                    <div className="comment py-3 border-bottom">
                                                        <div className="float-left" style={{width: '8%'}}>
                                                            <img src="/download.jpg"
                                                                 style={{width: 'auto', height: 'auto'}}
                                                                 className="rounded-circle img-fluid"/>
                                                        </div>
                                                        <div className="float-right pl-3" style={{width: '92%'}}>
                                                            <div className="row">
                                                                <div className="col-sm-8">
                                                                    <h7><b>Romy Dagohoy Jr.</b></h7>
                                                                    <span
                                                                        className="d-block mb-0 text-muted font-size-sm"
                                                                        style={{'font-size': '11px'}}>08:00 - January 12, 2018</span>
                                                                </div>
                                                                <div className="col-sm-4 text-right">
                                                                    <i className="fa fa-star text-secondary fa-sm"
                                                                       aria-hidden="true"></i>
                                                                    <i className="fa fa-star text-secondary fa-sm"
                                                                       aria-hidden="true"></i>
                                                                    <i className="fa fa-star text-secondary fa-sm"
                                                                       aria-hidden="true"></i>
                                                                    <i className="fa fa-star text-secondary fa-sm"
                                                                       aria-hidden="true"></i>
                                                                    <i className="fa fa-star text-secondary fa-sm text-dark"
                                                                       aria-hidden="true"></i>
                                                                </div>
                                                            </div>
                                                            <p className="m-0">Lorem ipsum dolor sit amet, consectetur
                                                                adipisicing elit, sed do eiusmod tempor incididunt ut
                                                                labore
                                                                et dolore magna aliqua.</p>
                                                        </div>
                                                        <div className="clearfix"></div>
                                                    </div>
                                                    <div className="content mt-4">
                                                        <div className="form-group">
                                                            <div className="float-left" style={{width: '90%'}}>
                                                            <textarea className="form-control"
                                                                      placeholder="Add Comment..." rows="3"></textarea>
                                                            </div>
                                                            <div className="float-right" style={{width: '10%'}}>
                                                                <div className="input-group upload-img">
                                                                    <div className="custom-file">
                                                                        <input type="file"
                                                                               className="custom-file-input"/>
                                                                        <label>
                                                                            <figure className="text-center">
                                                                                <i className="fa fa-camera"></i>
                                                                            </figure>
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div className="clearfix"></div>
                                                            <div className="row mt-1">
                                                                <div className="col-sm-12">
                                                               <span className="text-muted font-size-xs">
                                                                   Rate and Reviews the product.
                                                               </span>
                                                                    &nbsp;&nbsp;&nbsp;
                                                                    <i className="fa fa-star text-secondary fa-sm text-dark"
                                                                       aria-hidden="true"></i>
                                                                    <i className="fa fa-star text-secondary fa-sm text-dark"
                                                                       aria-hidden="true"></i>
                                                                    <i className="fa fa-star text-secondary fa-sm text-dark"
                                                                       aria-hidden="true"></i>
                                                                    <i className="fa fa-star text-secondary fa-sm text-dark"
                                                                       aria-hidden="true"></i>
                                                                    <i className="fa fa-star text-secondary fa-sm text-dark"
                                                                       aria-hidden="true"></i>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default WithContext(Product);