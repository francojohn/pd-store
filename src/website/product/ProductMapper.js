import ProductModel from "./ProductModel";
import Parse from "parse";

export function parseToProduct(parseProduct) {
    let product = new ProductModel();
    product.id = parseProduct.id;
    product.name = parseProduct.get("name");
    product.price = parseProduct.get("price");
    product.thumbnail = getProductThumbnail(parseProduct);
    return product;
}

export function productToParse(product) {
    let parseProduct = new Parse.Object("Product");
    parseProduct.set("name",product.name);
    parseProduct.set("price",product.price);
    parseProduct.set("description",product.description);
    parseProduct.set("variant",product.variant);
    return parseProduct;
}

export function parseToProducts(parseProducts) {
    let products = [];
    for (let p of parseProducts) {
        products.push(parseToProduct(p));
    }
    return products;
}

export function getProductThumbnail(parseProduct) {
    let thumbnail = parseProduct.get("thumbnail");
    let image_url;
    if (thumbnail) {
        let file = thumbnail.get("file");
        if (file)
            image_url = file._url.replace("http", "https");
    }
    return image_url;
}
