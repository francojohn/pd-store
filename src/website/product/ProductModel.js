export default class ProductModel {
    id;//string
    name;//string
    description;//string
    price;//number
    thumbnail;//string
    visible;//boolean
    active;//boolean
    createdAt;//date
    productImages;//array of object
    variant;//json object
}
