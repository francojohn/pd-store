import React, {Component} from "react";
import {Link} from 'react-router-dom';
import Parse from 'parse';

class Mobilemenu extends Component {
    render() {

    return (
        <div className="mobileview" style={{height:"45px"}}>
            <div className="p-1 bg-transparent">
                <input type="text" className="form-control" style={{position:"relative"}} placeholder="Search"/>
                <button className="bg-transparent border-0" style={{position:"absolute",top: "10px",right: "10px"}}>
                    <i className="fa fa-search text-muted"></i>
                </button>
            </div>
            <div className="text-center pt-1 border-top bg-white fixed-bottom">
                <div className="container">
                    <div className="py-3" style={{width: "25%", float: "left"}}>
                        <Link to="/" className="font-size-xs d-block">
                        <img src="/pdlogosmall.png" style={{width: "11px"}} className="img-fluid mb-1"/><br />
                        HOME</Link>
                    </div>
                    <div className="py-3" style={{width: "25%", float: "left"}}>
                        <Link to="/cart" className="font-size-xs d-block text-muted">
                        <i className="fa fa-shopping-cart fa-xl d-block mb-2"></i>
                        CART</Link>
                    </div>
                    <div className="py-3" style={{width: "25%", float: "left"}}>
                        <Link to="/construction" className="font-size-xs d-block text-muted">
                        <i className="fa fa-wallet fa-xl d-block mb-2"></i>
                        WALLET</Link>
                    </div>
                    <div className="py-3" style={{width: "25%", float: "left"}}>
                        <Link to="/account" className="font-size-xs d-block text-muted">
                        <i className="fa fa-shopping-cart fa-xl d-block mb-2"></i>
                        ACCOUNT</Link>
                    </div>
                    <div className="clearfix"></div>
                </div>
            </div>
        </div>
        );
    }
}

export default Mobilemenu;