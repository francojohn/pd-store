import React, {Component} from 'react';
import './style.css';
import {Link} from 'react-router-dom';
class Footer extends Component {
    render() {
        return (
            <footer className="footer py-4 px-3 bg-dark">
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-12 col-sm-3">
                            <div className="single-widget">
                                <img className="img-fluid w-75" alt="header logo" src="/logo.png"/>
                                <p className="text-light mt-3 font-size-xs">Ⓒ PD. All rights reserved
                                    2018</p>
                            </div>
                        </div>
                        <div className="col-12 col-sm-4">
                            <span className="text-primary font-size-sm">Contact Us</span>
                            <div className="dropdown-divider"></div>
                            <ul className="footer-ul list-unstyled text-light font-size-xs">
                                <li>
                                    <div className="row">
                                        <div className="col-1 p-0 text-center text-primary">
                                            <i className="fa fa-phone"/>
                                        </div>
                                        <div className="col-9 p-0">
                                            (02)9451579
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div className="row">
                                        <div className="col-1 p-0 text-center text-primary">
                                            <i className="fa fa-mobile-alt"/>
                                        </div>
                                        <div className="col-9 p-0">
                                            +63-997-608-6635
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div className="row">
                                        <div className="col-1 p-0 text-center text-primary">
                                            <i className="fa fa-envelope"/>
                                        </div>
                                        <div className="col-9 p-0">
                                            support@programmersdevelopers.com
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div className="row">
                                        <div className="col-1 p-0 text-center text-primary">
                                            <i className="fa fa-map-marker-alt"/>
                                        </div>
                                        <div className="col-9 p-0">
                                            pineda pasig city
                                        </div>
                                    </div>
                                </li>
                            </ul>
                            <button type="button" className="btn btn-sm btn-outline-light mr-2">
                                <i className="fab fa-twitter"></i>
                            </button>
                            <button type="button" className="btn btn-sm btn-outline-light mr-2">
                                <i className="fab fa-instagram"></i>
                            </button>
                            <button type="button" className="btn btn-sm btn-outline-light mr-2">
                                <i className="fab fa-facebook-f"></i>
                            </button>
                        </div>
                        <div className="col-12 col-sm-2">
                            <span className="text-primary font-size-sm">Policies</span>
                            <div className="dropdown-divider"></div>
                            <ul className="footer-ul list-unstyled font-size-xs">
                                <li className="text-light">Terms of Use</li>
                                <li className="text-light"><Link to={"/policy"}>Privacy Policy</Link></li>
                                <li className="text-light">Refund Policy</li>
                                <li className="text-light">Shipping Policy</li>
                                <li className="text-light">Payment Method</li>
                            </ul>
                        </div>

                        <div className="col-sm-3 col-sm-offset-1">
                            <div className="single-widget">
                                <span className="text-primary font-size-sm">Stay Connected</span>
                                <div className="dropdown-divider"></div>
                                <form className="input-group font-size-xs">
                                    <p className="text-secondary mt-1 all">
                                        Get the most recent updates from our site and be updated your self.</p>
                                    <input className="form-control form-control-sm rounded-0" type="text"
                                           placeholder="Your email address"/>
                                    <span className="input-group-btn">
                                        <button className="btn btn-sm btn-light rounded-0" type="submit">
                                            <i className="fab fa-telegram-plane text-primary"></i>
                                        </button>
                                  </span>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
        );
    }
}

export default Footer;
